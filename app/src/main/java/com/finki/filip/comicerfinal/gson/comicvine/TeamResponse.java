package com.finki.filip.comicerfinal.gson.comicvine;

import java.io.Serializable;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class TeamResponse extends Resource implements Serializable{
	
	/**
	 * 
	 */
	private String aliases;
	private String api_detail_url;
	private CharacterResponse[] characters;
	private String deck;
	private String description;
	private IssueResponse first_appeared_in_issue;
	private int id;
	private Image image;
	private MovieResponse[] movies;
	private String name;
	private String site_detail_url;
	private VolumeResponse[] volume_credits;
	
	public TeamResponse() {
		super();
		this.characters = new CharacterResponse[]{};
		this.movies = new MovieResponse[]{};
		this.volume_credits = new VolumeResponse[]{};
	}

	public TeamResponse(String aliases, String api_detail_url, CharacterResponse[] characters, String deck, String description,
			IssueResponse first_appeared_in_issue, int id, Image image, MovieResponse[] movies, String name,
			String site_detail_url, VolumeResponse[] volume_credits) {
		super();
		this.aliases = aliases;
		this.api_detail_url = api_detail_url;
		this.characters = characters;
		this.deck = deck;
		this.description = description;
		this.first_appeared_in_issue = first_appeared_in_issue;
		this.id = id;
		this.image = image;
		this.movies = movies;
		this.name = name;
		this.site_detail_url = site_detail_url;
		this.volume_credits = volume_credits;
	}

	public String getAliases() {
		return aliases;
	}

	public void setAliases(String aliases) {
		this.aliases = aliases;
	}

	public String getApi_detail_url() {
		return api_detail_url;
	}

	public void setApi_detail_url(String api_detail_url) {
		this.api_detail_url = api_detail_url;
	}

	public CharacterResponse[] getCharacters() {
		return characters;
	}

	public void setCharacters(CharacterResponse[] characters) {
		this.characters = characters;
	}

	public String getDeck() {
		return deck;
	}

	public void setDeck(String deck) {
		this.deck = deck;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public IssueResponse getFirst_appeared_in_issue() {
		return first_appeared_in_issue;
	}

	public void setFirst_appeared_in_issue(IssueResponse first_appeared_in_issue) {
		this.first_appeared_in_issue = first_appeared_in_issue;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public MovieResponse[] getMovies() {
		return movies;
	}

	public void setMovies(MovieResponse[] movies) {
		this.movies = movies;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSite_detail_url() {
		return site_detail_url;
	}

	public void setSite_detail_url(String site_detail_url) {
		this.site_detail_url = site_detail_url;
	}

	public VolumeResponse[] getVolume_credits() {
		return volume_credits;
	}

	public void setVolume_credits(VolumeResponse[] volume_credits) {
		this.volume_credits = volume_credits;
	}
	
	public String toString() {
		
		JsonObject jo = new JsonObject();
		jo.addProperty("aliases", aliases);
		jo.addProperty("api_detail_url", api_detail_url);
		JsonArray ja = new JsonArray();
		if (characters != null)
			for (int i=0; i<characters.length; i++) {
				ja.add(characters[i].toString());
			}
		jo.add("characters", ja);
		jo.addProperty("deck", deck);
		jo.addProperty("description", description);
		if (first_appeared_in_issue != null) 
			jo.addProperty("first_appeared_in_issue", first_appeared_in_issue.toString());
		else
			jo.addProperty("first_appeared_in_issue", (String)null);
		jo.addProperty("id", id);
		if (image != null)
			jo.addProperty("image", image.toString());
		else
			jo.addProperty("image", (String)null);
		ja = new JsonArray();
		if (movies != null)
			for (int i=0; i<movies.length; i++) {
				ja.add(movies[i].toString());
			}
		jo.add("movies", ja);
		jo.addProperty("name", name);
		jo.addProperty("site_detail_url", site_detail_url);
		ja = new JsonArray();
		if (volume_credits != null)
			for (int i=0; i<volume_credits.length; i++) {
				ja.add(volume_credits[i].toString());
			}
		jo.add("volume_credits", ja);
		return new GsonBuilder().serializeNulls().create().toJson(jo);
	}

	@Override
	public boolean equals(Object obj) {
		return this.id == ((TeamResponse)obj).getId();
	}

	public Team getTeam() {
		Team t = new Team();
		t.setAliases(aliases);
		t.setApi_detail_url(api_detail_url);
		Character[] tCharacters = null;
		if (characters != null) {
			tCharacters = new Character[characters.length];
			for (int i=0; i<characters.length; i++) {
				tCharacters[i] = characters[i].getCharacter();
			}
		}
		t.setCharacters(tCharacters);
		t.setDeck(deck);
		t.setDescription(description);
		Issue issue = null;
		if (first_appeared_in_issue != null) {
			issue = first_appeared_in_issue.getIssue();
		}
		t.setFirst_appeared_in_issue(issue);
		t.setId(id);
		t.setImage(image);
		Movie[] tMovies = null;
		if (movies != null) {
			tMovies = new Movie[movies.length];
			for (int i=0; i<movies.length; i++) {
				tMovies[i] = movies[i].getMovie();
			}
		}
		t.setMovies(tMovies);
		t.setName(name);
		t.setSite_detail_url(site_detail_url);
		Volume[] volumes = null;
		if (volume_credits != null) {
			volumes = new Volume[volume_credits.length];
			for (int i=0; i<volume_credits.length; i++) {
				volumes[i] = volume_credits[i].getVolume();
			}
		}
		t.setVolume_credits(volumes);
		return t;
	}
}
