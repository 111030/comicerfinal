package com.finki.filip.comicerfinal.gson.comicvine;

import java.io.Serializable;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class Volume extends Resource implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7661091119512343369L;
	private String aliases;
	private String api_detail_url;
	private Character[] characters;
	private String count_of_issues;
	private String deck;
	private String description;
	private Issue first_issue;
	private int _id;
	private Image image;
	private Issue last_issue;
	private String name;
	private Person[] people;
	private Publisher publisher;
	private String site_detail_url;
	private String start_year;
	
	public Volume() {
		super();
		characters = new Character[]{};
		people = new Person[]{};
	}

	public Volume(String aliases, String api_detail_url, Character[] characters, String count_of_issues, String deck,
				  String description, Issue first_issue, int _id, Image image, Issue last_issue, String name, Person[] people, Publisher publisher,
				  String site_detail_url, String start_year) {
		super();
		this.aliases = aliases;
		this.api_detail_url = api_detail_url;
		this.characters = characters;
		this.count_of_issues = count_of_issues;
		this.deck = deck;
		this.description = description;
		this.first_issue = first_issue;
		this._id = _id;
		this.image = image;
		this.last_issue = last_issue;
		this.name = name;
		this.people = people;
		this.publisher = publisher;
		this.site_detail_url = site_detail_url;
		this.start_year = start_year;
	}

	public String getAliases() {
		return aliases;
	}

	public void setAliases(String aliases) {
		this.aliases = aliases;
	}

	public String getApi_detail_url() {
		return api_detail_url;
	}

	public void setApi_detail_url(String api_detail_url) {
		this.api_detail_url = api_detail_url;
	}

	public Character[] getCharacters() {
		return characters;
	}

	public void setCharacters(Character[] characters) {
		this.characters = characters;
	}

	public String getCount_of_issues() {
		return count_of_issues;
	}

	public void setCount_of_issues(String count_of_issues) {
		this.count_of_issues = count_of_issues;
	}

	public String getDeck() {
		return deck;
	}

	public void setDeck(String deck) {
		this.deck = deck;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Issue getFirst_issue() {
		return first_issue;
	}

	public void setFirst_issue(Issue first_issue) {
		this.first_issue = first_issue;
	}

	public int getId() {
		return _id;
	}

	public void setId(int _id) {
		this._id = _id;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}
	

	public Issue getLast_issue() {
		return last_issue;
	}

	public void setLast_issue(Issue last_issue) {
		this.last_issue = last_issue;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Person[] getPeople() {
		return people;
	}

	public void setPeople(Person[] people) {
		this.people = people;
	}

	public Publisher getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}

	public String getSite_detail_url() {
		return site_detail_url;
	}

	public void setSite_detail_url(String site_detail_url) {
		this.site_detail_url = site_detail_url;
	}

	public String getStart_year() {
		return start_year;
	}

	public void setStart_year(String start_year) {
		this.start_year = start_year;
	}
	
	public String toString() {
		JsonObject jo = new JsonObject();
		jo.addProperty("aliases", aliases);
		jo.addProperty("api_detail_url", api_detail_url);
		JsonArray ja = new JsonArray();
		if (characters != null)
			for (int i=0; i<characters.length; i++) {
				ja.add(characters[i].toString());
			}
		jo.add("characters", ja);
		jo.addProperty("count_of_issues", count_of_issues);
		jo.addProperty("deck", deck);
		jo.addProperty("description", description);
		if (first_issue != null)
			jo.addProperty("first_issue", first_issue.toString());
		else
			jo.addProperty("first_issue", (String)null);
		jo.addProperty("_id", _id);
		if (image != null)
			jo.addProperty("image", image.toString());
		else 
			jo.addProperty("image", (String)null);
		if (last_issue != null)
			jo.addProperty("last_issue", last_issue.toString());
		else
			jo.addProperty("last_issue", (String)null);
		jo.addProperty("name", name);
		ja = new JsonArray();
		if (people != null)
			for (int i = 0; i< people.length; i++) {
				ja.add(people[i].toString());
			}
		jo.add("people", ja);
		if (publisher != null)
			jo.addProperty("publisher", publisher.toString());
		else
			jo.addProperty("publisher", (String)null);
		jo.addProperty("site_detail_url", site_detail_url);
		jo.addProperty("start_year", start_year);
		return new GsonBuilder().serializeNulls().create().toJson(jo);
	}
	
	public boolean equals(Object o) {
		return this._id == ((Volume)o).getId();
	}
	

}
