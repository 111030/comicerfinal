package com.finki.filip.comicerfinal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.finki.filip.comicerfinal.adapters.ReadingListAdapter;
import com.finki.filip.comicerfinal.asyncs.NavDrawerUser;
import com.finki.filip.comicerfinal.asyncs.ReadingListPopulator;
import com.finki.filip.comicerfinal.fragments.NavDrawerFragment;
import com.finki.filip.comicerfinal.fragments.TempEmptyDrawer;
import com.finki.filip.comicerfinal.singletons.UserSingleton;

/**
 * Created by Filip on 22.1.2017.
 */

public class ReadingListScreen extends Activity{

    private TextView spError;
    private ListView items;
    private ReadingListAdapter adapter;
    //
    private ImageButton toggleDrawerButton;
    private ImageButton shoppingCart;
    private TextView shoppingCartCounter;
    private TextView readingListCounter;

    boolean drawerShow = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reading_list);
        //
        toggleDrawerButton = (ImageButton)findViewById(R.id.appbar_drawer_button);
        toggleDrawerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleNavigationDrawer();
            }
        });
        //
        shoppingCartCounter = (TextView)findViewById(R.id.appbar_shopping_cart_counter);
        readingListCounter = (TextView)findViewById(R.id.appbar_reading_list_counter);
        //setCurrentSignedUser();
        shoppingCart = (ImageButton)findViewById(R.id.appbar_shopping_cart);
        shoppingCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ReadingListScreen.this, ShoppingCartScreen.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                i.putExtra("User", UserSingleton.getInstance().getUser() != null ?
                        UserSingleton.getInstance().getUser().getUsername() : "anonymous");
                startActivity(i);
            }
        });
        spError = (TextView)findViewById(R.id.reading_list_error);
        //
        items = (ListView)findViewById(R.id.reading_list_items);
        //
        String username = getIntent().getStringExtra("User");
        if (username.equalsIgnoreCase("anonymous")) {
            spError.setVisibility(View.VISIBLE);
            items.setVisibility(View.GONE);
        }
        else {
            spError.setVisibility(View.GONE);
            items.setVisibility(View.VISIBLE);
            adapter = new ReadingListAdapter(this);
            new ReadingListPopulator(adapter).execute(
                    UserSingleton.getInstance().getUser());
            items.setAdapter(adapter);
            items.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                }
            });
        }
        getFragmentManager().beginTransaction()
                .add(R.id.nav_drawer_fragment_reading_list, TempEmptyDrawer.newInstance())
                .commit();


    }

    @Override
    protected void onResume() {
        super.onResume();
        setCurrentSignedUser();
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
            drawerShow = false;
        }

    }

    private void setCurrentSignedUser() {
            WifiManager wm = (WifiManager)getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            String macAddress = wm.getConnectionInfo().getMacAddress();
            NavDrawerUser getCurrentUser = new NavDrawerUser(shoppingCartCounter, readingListCounter);
            getCurrentUser.execute(macAddress);
    }

    private void toggleNavigationDrawer() {
        if (drawerShow) {
            getFragmentManager().popBackStack();
            drawerShow = false;
            return;
        }
        drawerShow = true;
        getFragmentManager().beginTransaction()
                .setCustomAnimations(R.animator.drawer_to_right, R.animator.drawer_to_left,
                        R.animator.drawer_to_right, R.animator.drawer_to_left)
                .replace(R.id.nav_drawer_fragment_reading_list, NavDrawerFragment.newInstance())
                .addToBackStack(null)
                .commit();
    }

}
