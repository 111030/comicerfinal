package com.finki.filip.comicerfinal.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.finki.filip.comicerfinal.R;
import com.finki.filip.comicerfinal.ShoppingCartScreen;
import com.finki.filip.comicerfinal.asyncs.GetCurrentUser;
import com.finki.filip.comicerfinal.asyncs.IssueRatingsPopulator;
import com.finki.filip.comicerfinal.eventlisteners.RemoveShoppingCartItemListener;
import com.finki.filip.comicerfinal.fragments.dialogs.BuyIssuePromptDialog;
import com.finki.filip.comicerfinal.gson.comicvine.Issue;
import com.finki.filip.comicerfinal.gson.marvel.ComicPrice;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;


/**
 * Created by Filip on 04.11.2016.
 */

public class ShoppingCartAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater layoutInflater;

    private List<Issue> issues;

    public ShoppingCartAdapter(Context context) {
        mContext = context;
        layoutInflater = LayoutInflater.from(context);
        issues = new ArrayList<Issue>();
    }

    static class ShoppingCartHolder {
        ImageView issueImg;
        TextView issueFullName;
        LinearLayout issueRating;
        TextView ratingCount;
        Button buyForPrice;
        ImageButton remove;
    }

    public void addAll(List<Issue> issues) {
        this.issues.addAll(issues);
        notifyDataSetChanged();
    }

    public List<Issue> getIssues() {
        return issues;
    }

    @Override
    public int getCount() {
        return issues.size();
    }

    @Override
    public Object getItem(int i) {
        return issues.get(i);
    }

    @Override
    public long getItemId(int i) {
        return issues.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final ShoppingCartHolder holder;
        final int index = i;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.shopping_cart_item, null);
            holder = new ShoppingCartHolder();
            holder.issueImg = (ImageView)view.findViewById(R.id.shopping_cart_item_image);
            holder.issueFullName = (TextView)view.findViewById(R.id.shopping_cart_item_full_name);
            holder.issueRating = (LinearLayout) view.findViewById(R.id.shopping_cart_item_rating);
            holder.ratingCount = (TextView)view.findViewById(R.id.shopping_cart_item_ratings_count);
            holder.buyForPrice = (Button)view.findViewById(R.id.shopping_cart_item_price);
            holder.remove = (ImageButton)view.findViewById(R.id.shopping_cart_item_remove);
            view.setTag(holder);
        }
        else {
            holder = (ShoppingCartHolder)view.getTag();
        }
        Glide.with(mContext).load(issues.get(i).getImage().getMedium_url()).fitCenter().
                into(holder.issueImg);
        //
        String volumeName = issues.get(i).getVolume() != null ?
                issues.get(i).getVolume().getName() : issues.get(i).getName();
        DateFormat dateFormatFrom = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String lastYear = "";
        try {
            Date d = dateFormatFrom.parse(issues.get(i).getDate_last_updated());
            DateFormat dateFormatTo = new SimpleDateFormat("yyyy-MM-dd");
            lastYear = dateFormatTo.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        final String issueNumber = issues.get(i).getIssue_number();
        holder.issueFullName.setText(volumeName + " (" + lastYear + ") - #" + issueNumber);
        //
        int rating = issues.get(i).getUser_review() != 0 ?
                (int)issues.get(i).getUser_review() : 0;
        holder.issueRating.setDividerPadding(16);
        holder.issueRating.removeAllViews();
        for (int j=0; j<rating; j++) {
            ImageButton ratingStar = new ImageButton(mContext);
            ratingStar.setScaleType(ImageView.ScaleType.FIT_XY);
            ratingStar.setImageResource(R.drawable.ic_star_white_24dp);
            ratingStar.setBackgroundColor(Color.TRANSPARENT);
            holder.issueRating.addView(ratingStar, j);
        }
        for (int j=rating; j<5; j++) {
            ImageButton ratingStar = new ImageButton(mContext);
            ratingStar.setScaleType(ImageView.ScaleType.FIT_XY);
            ratingStar.setImageResource(R.drawable.ic_star_border_white_24dp);
            ratingStar.setBackgroundColor(Color.TRANSPARENT);
            holder.issueRating.addView(ratingStar, j);
        }
        //
        new IssueRatingsPopulator(holder.ratingCount).execute(issues.get(i));
        //
        List<ComicPrice> prices = issues.get(i).getPrices();
        for (int j=0; j<prices.size(); j++) {
            if (prices.get(j).getType().equalsIgnoreCase("digitalPurchasePrice")) {
                holder.buyForPrice.setText(String.valueOf(prices.get(j).getPrice()));
                holder.buyForPrice.setBackgroundResource(prices.get(j).getPrice() > 0 ?
                R.color.colorRed : R.color.infoScreenColor);
                break;
            }
            if (j == prices.size() - 1) {
                holder.buyForPrice.setText(String.valueOf(prices.get(j).getPrice()));
                holder.buyForPrice.setBackgroundResource(prices.get(j).getPrice() > 0 ?
                        R.color.colorRed : R.color.infoScreenColor);
            }
        }
        holder.remove.setOnClickListener(new RemoveShoppingCartItemListener(this, mContext, i, issues));
        holder.buyForPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BuyIssuePromptDialog bipd = null;
                try {
                    bipd = BuyIssuePromptDialog.newInstance(
                            new GetCurrentUser(mContext).execute().get().getAmount_credit_card(),
                            index,
                            issues
                    );
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                bipd.show(((ShoppingCartScreen)mContext).getFragmentManager(), "BUY_ISSUE_PROMPT");
            }
        });

        return view;
    }

    public float getTotal() {
        float sum = 0.0f;
        for (int i=0; i<issues.size(); i++) {
            List<ComicPrice> listPrices = issues.get(i).getPrices();
            for( int j=0; j<listPrices.size(); j++) {
                if (listPrices.get(j).getType().equalsIgnoreCase("digitalPurchasePrice")) {
                    sum += listPrices.get(j).getPrice();
                    break;
                }
                if (j == listPrices.size() - 1) {
                    sum += listPrices.get(j).getPrice();
                }
            }
        }
        return sum;
    }
}
