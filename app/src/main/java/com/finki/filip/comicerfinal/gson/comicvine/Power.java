package com.finki.filip.comicerfinal.gson.comicvine;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.io.Serializable;


/**
 * Created by Filip on 05.2.2017.
 */

public class Power extends Resource implements Serializable {

    private String api_detail_url;
    private String description;
    private int _id;
    private String name;

    public Power() {}

    public Power(String api_detail_url, String description, int _id, String name) {
        this.api_detail_url = api_detail_url;
        this.description = description;
        this._id = _id;
        this.name = name;
    }

    public String getApi_detail_url() {
        return api_detail_url;
    }

    public void setApi_detail_url(String api_detail_url) {
        this.api_detail_url = api_detail_url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        Gson gson = new GsonBuilder().serializeNulls().create();
        JsonObject jo = new JsonObject();
        jo.addProperty("api_detail_url", api_detail_url);
        jo.addProperty("description", description);
        jo.addProperty("_id", _id);
        jo.addProperty("name", name);
        return gson.toJson(jo);
    }
}
