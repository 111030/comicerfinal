package com.finki.filip.comicerfinal.asyncs;

import android.os.AsyncTask;

import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.gson.comicvine.IssueResponse;
import com.finki.filip.comicerfinal.gson.comicvine.ResponseSingle;
import com.finki.filip.comicerfinal.singletons.MongoSingleton;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Filip on 15.1.2017.
 */

public class IssueInfoScreenPopulator extends AsyncTask<String, Integer, Boolean> {


    @Override
    protected Boolean doInBackground(String... urls) {
        String comicPart = urls[0];
        String fieldPart = urls[1];
        String _id = urls[2];
        boolean result = false;
        if (comicPart != null && !comicPart.isEmpty()) {
            HttpURLConnection http = null;
            InputStream is = null;
            try {
                URL url = new URL(comicPart+"&field_list="+fieldPart);
                http = (HttpURLConnection)url.openConnection();
                if (http.getContentLength() != -1) {
                    is = http.getInputStream();
                    BufferedReader br = new BufferedReader(new InputStreamReader(is, "utf-8"));
                    Gson gson = new GsonBuilder().serializeNulls().create();
                    Type t = new TypeToken<ResponseSingle<IssueResponse>>(){}.getType();
                    ResponseSingle<IssueResponse> responseIssue = gson.fromJson(br, t);
                    IssueResponse issue = responseIssue.getResults();
                    result = MongoDBHelper.updateIssuesField(Integer.parseInt(_id),
                            fieldPart,
                            issue,
                            MongoSingleton.getInstance().getDatabase());
                    br.close();
                    is.close();
                }

                http.disconnect();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }
}
