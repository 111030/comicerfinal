package com.finki.filip.comicerfinal.adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.finki.filip.comicerfinal.R;
import com.finki.filip.comicerfinal.eventlisteners.LikeDislikeListener;
import com.finki.filip.comicerfinal.gson.comicvine.Issue;
import com.finki.filip.comicerfinal.gson.other.Comment;

import java.util.List;

/**
 * Created by Filip on 29.1.2017.
 */

public class CommentsAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    private Issue issue;

    public CommentsAdapter(Context context, Issue issue) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        this.issue = issue;
    }

    public void addAll(List<Comment> comments) {
        issue.getComments().addAll(comments);
        notifyDataSetChanged();
    }

    public void add(Comment comment) {
        issue.getComments().add(comment);
        Toast.makeText(context,"Comment successfully added", Toast.LENGTH_SHORT).show();
        notifyDataSetChanged();
    }


    @Override
    public int getCount() {
        return issue.getComments().size();
    }

    @Override
    public Object getItem(int i) {
        return issue.getComments().get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    static class CommentsHolder {
        TextView profileAvatar;
        TextView message;
        ImageButton like;
        TextView likesCount;
        TextView dateSent;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        CommentsHolder holder;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.issue_info_comment, null);
            holder = new CommentsHolder();
            holder.profileAvatar = (TextView)view.findViewById(R.id.issue_info_comment_profile);
            holder.message = (TextView)view.findViewById(R.id.issue_info_comment_comment);
            holder.like = (ImageButton)view.findViewById(R.id.issue_info_comment_btn_tup);
            holder.likesCount = (TextView)view.findViewById(R.id.issue_info_comment_likes_count);
            holder.dateSent = (TextView)view.findViewById(R.id.issue_info_comment_date_send);
            view.setTag(holder);
        }
        else {
            holder = (CommentsHolder)view.getTag();
        }
        holder.profileAvatar.setText(((Comment)issue.getComments().get(i)).getUser().getName()
                .substring(0, 1).toUpperCase()
         + ((Comment)issue.getComments().get(i)).getUser().getSurname().substring(0, 1).toUpperCase());
        holder.message.setText(((Comment)issue.getComments().get(i)).getMessage());
        int likesCount = ((Comment)issue.getComments().get(i)).getLikes() != null ?
                ((Comment)issue.getComments().get(i)).getLikes().size() : 0;
        int diffCount = likesCount;
        if (diffCount != 0) {
            holder.likesCount.setText(String.valueOf(diffCount));
        }
        holder.dateSent.setText(((Comment)issue.getComments().get(i)).getDateSent());
        holder.like.setOnClickListener(new LikeDislikeListener(issue, holder.likesCount
        , i));
        return view;
    }
}
