package com.finki.filip.comicerfinal.asyncs;

import android.os.AsyncTask;
import android.view.View;
import android.widget.TextView;

import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.gson.other.User;
import com.finki.filip.comicerfinal.singletons.MongoSingleton;

/**
 * Created by Filip on 19.1.2017.
 */

public class ShoppingCartSubscriber extends AsyncTask<User, Void, Integer> {

    private TextView shoppingCartCounter;

    public ShoppingCartSubscriber(TextView shoppingCartCounter) {
        this.shoppingCartCounter = shoppingCartCounter;
    }


    @Override
    protected Integer doInBackground(User... users) {
        return MongoDBHelper.getShoopingCartItems(
                users[0], MongoSingleton.getInstance().getDatabase());

    }

    @Override
    protected void onPostExecute(Integer integer) {
        shoppingCartCounter.setText(String.valueOf(integer));
        if (integer > 0)
            shoppingCartCounter.setVisibility(View.VISIBLE);
        else
            shoppingCartCounter.setVisibility(View.INVISIBLE);
    }
}
