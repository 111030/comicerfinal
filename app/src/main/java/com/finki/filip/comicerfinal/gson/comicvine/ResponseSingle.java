package com.finki.filip.comicerfinal.gson.comicvine;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

/**
 * Created by Filip on 20.8.2016.
 */
public class ResponseSingle<T extends Resource> extends Response{

    private T results;

    public ResponseSingle()
    {
        super();
    }

    public ResponseSingle(int status_code, String error, long number_of_total_results, int number_of_page_results,
                          int limit, int offset, T results)
    {
        super(status_code, error, number_of_total_results, number_of_page_results, limit, offset);
        this.results = results;
    }

    public T getResults() {
        return results;
    }

    public void setResults(T results) {
        this.results = results;
    }

    public String toString()
    {
        JsonObject jo = new JsonObject();
        if (results != null)
            jo.addProperty("results", results.toString());
        else
            jo.addProperty("results", (String)null);
        return super.toString()+ new Gson().toJson(jo);
    }
}
