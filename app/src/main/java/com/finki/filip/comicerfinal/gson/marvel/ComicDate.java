package com.finki.filip.comicerfinal.gson.marvel;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;

public class ComicDate {

	private String type;
	private String date;
	
	public ComicDate() {
		super();
	}

	public ComicDate(String type, String date) {
		super();
		this.type = type;
		this.date = date;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	@Override
	public String toString() {
		JsonObject jo = new JsonObject();
		jo.addProperty("type", type);
		jo.addProperty("date", date);
		return new GsonBuilder().serializeNulls().create().toJson(jo);

	}
	
	
}
