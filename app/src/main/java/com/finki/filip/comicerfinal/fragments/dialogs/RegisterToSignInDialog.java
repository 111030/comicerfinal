package com.finki.filip.comicerfinal.fragments.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.finki.filip.comicerfinal.SignInScreen;

/**
 * Created by Filip on 24.12.2016.
 */

public class RegisterToSignInDialog extends DialogFragment implements DialogInterface.OnClickListener {

    public static RegisterToSignInDialog newInstance() {
        RegisterToSignInDialog rtsid = new RegisterToSignInDialog();
        return rtsid;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder b = new AlertDialog.Builder(getActivity())
                .setTitle("Continue Sign In")
                .setNegativeButton("Cancel", this)
                .setPositiveButton("Ok", this)
                .setMessage("It seems that a user with this e-mail already exists. Would you like to sign in?");
        return b.create();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        if (i == AlertDialog.BUTTON_POSITIVE) {
            //
            Intent intent = new Intent(getActivity(), SignInScreen.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(intent);
        }
        else {
            dismiss();
        }
    }
}
