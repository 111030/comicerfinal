package com.finki.filip.comicerfinal.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.finki.filip.comicerfinal.CharactersScreen;
import com.finki.filip.comicerfinal.FavoritesScreen;
import com.finki.filip.comicerfinal.MainScreen;
import com.finki.filip.comicerfinal.MoviesScreen;
import com.finki.filip.comicerfinal.R;
import com.finki.filip.comicerfinal.RegisterScreen;
import com.finki.filip.comicerfinal.SignInScreen;
import com.finki.filip.comicerfinal.VolumesScreen;
import com.finki.filip.comicerfinal.fragments.dialogs.SignOutDialog;
import com.finki.filip.comicerfinal.gson.other.User;
import com.finki.filip.comicerfinal.singletons.UserSingleton;


/**
 * Created by Filip on 23.10.2016.
 */

public class NavDrawerFragment extends Fragment {


    public static NavDrawerFragment newInstance() {
        NavDrawerFragment ndf = new NavDrawerFragment();
        return ndf;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.navigation_drawer, container, false);

        TextView avatar = (TextView)v.findViewById(R.id.navigation_drawer_profile_picture);
        TextView name = (TextView)v.findViewById(R.id.navigation_drawer_profile_name);
        TextView email = (TextView)v.findViewById(R.id.navigation_drawer_profile_email);
        TextView home = (TextView)v.findViewById(R.id.navigation_drawer__home);
        TextView signIn = (TextView)v.findViewById(R.id.navigation_drawer_sign_in);
        TextView register = (TextView)v.findViewById(R.id.navigation_drawer_register);
        TextView signOut = (TextView)v.findViewById(R.id.navigation_drawer_sign_out);
        TextView favorites = (TextView)v.findViewById(R.id.navigation_drawer_favorites);
        TextView characters = (TextView)v.findViewById(R.id.navigation_drawer_characters);
        TextView volumes = (TextView)v.findViewById(R.id.navigation_drawer_volumes);
        TextView movies = (TextView)v.findViewById(R.id.navigation_drawer_movies);
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MainScreen.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        });
        favorites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), FavoritesScreen.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        });
        //
        characters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CharactersScreen.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        });
        volumes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), VolumesScreen.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        });
        movies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MoviesScreen.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        });

        final User user = UserSingleton.getInstance().getUser();

        if (user == null) {
            avatar.setVisibility(View.INVISIBLE);
            name.setText("Anonymous");
            email.setText("");
            signIn.setVisibility(View.VISIBLE);
            register.setVisibility(View.VISIBLE);
            signOut.setVisibility(View.GONE);
            //
            signIn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(getActivity(), SignInScreen.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(i);
                }
            });
            //
            register.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(getActivity(), RegisterScreen.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(i);

                }
            });
        }
        else {
            avatar.setVisibility(View.VISIBLE);
            avatar.setText(user.getName().substring(0, 1).toUpperCase()+
            user.getSurname().substring(0,1).toUpperCase());
            name.setText(user.getName() + " " + user.getSurname());
            email.setText(user.getEmail());
            signIn.setVisibility(View.GONE);
            register.setVisibility(View.GONE);
            signOut.setVisibility(View.VISIBLE);
            //
            signOut.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new SignOutDialog().newInstance(user).show(getFragmentManager(),"SIGN_OUT");
                }
            });
        }

        v.setPivotX(0);
        return v;

    }
}
