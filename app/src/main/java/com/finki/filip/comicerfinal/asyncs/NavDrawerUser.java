package com.finki.filip.comicerfinal.asyncs;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.TextView;

import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.gson.other.User;
import com.finki.filip.comicerfinal.singletons.UserSingleton;

/**
 * Created by Filip on 22.12.2016.
 */

public class NavDrawerUser extends AsyncTask<String, Integer, User> {

    private TextView shoppingCartCounter;
    private TextView readingListCounter;

    public NavDrawerUser(TextView shoppingCartCounter, TextView readingListCounter) {
        this.shoppingCartCounter = shoppingCartCounter;
        this.readingListCounter = readingListCounter;
    }

    @Override
    protected User doInBackground(String... strings) {
        String macAddr = strings[0];
        return MongoDBHelper.getSignedIn(macAddr);
    }

    @Override
    protected void onPostExecute(User user) {
        super.onPostExecute(user);
        UserSingleton.setUser(user);
        //if (user != null) {
            new ShoppingCartSubscriber(shoppingCartCounter).execute(user);
            new ReadingListSubscriber(readingListCounter).execute(user);
        //}

    }

}
