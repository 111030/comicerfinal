package com.finki.filip.comicerfinal.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.finki.filip.comicerfinal.R;
import com.finki.filip.comicerfinal.gson.comicvine.Issue;
import com.finki.filip.comicerfinal.gson.marvel.ComicPrice;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Filip on 24.10.2016.
 */

public class MainScreenIssuesAdapter extends BaseAdapter implements Serializable{

    private Context mContext;
    private LayoutInflater layoutInflater;
    //
    private List<Issue> issues;
    //
    static class MainScreenIssueHolder {
        ImageView image;
        TextView issue_volume;
        TextView issue_number;
        TextView issue_title;
        TextView issue_price;

    }

    public MainScreenIssuesAdapter(Context mContext, int itemsCount) {
        this.mContext = mContext;
        layoutInflater = LayoutInflater.from(mContext);
        issues = new ArrayList<>();
    }

    public void addAll(List<Issue> issues) {
        this.issues.addAll(issues);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return issues.size();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        MainScreenIssueHolder holder;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.issue_grid_item, null);
            holder = new MainScreenIssueHolder();
            holder.image = (ImageView)view.findViewById(R.id.issue_grid_img);
            holder.issue_volume = (TextView)view.findViewById(R.id.issue_grid_volume);
            holder.issue_number = (TextView)view.findViewById(R.id.issue_grid_number);
            holder.issue_title = (TextView)view.findViewById(R.id.issue_grid_title);
            holder.issue_price = (TextView)view.findViewById(R.id.issue_grid_price);
            view.setTag(holder);
        }
        else {
            holder = (MainScreenIssueHolder)view.getTag();
        }
        if (isValidIssue(issues.get(i))) {
            Glide.with(mContext).load(issues.get(i).getImage().getThumb_url()).fitCenter()
                    .into(holder.image);
            holder.issue_volume.setText(issues.get(i).getVolume().getName());
            holder.issue_number.setText(issues.get(i).getIssue_number());
            holder.issue_title.setText(issues.get(i).getName());
            if (issues.get(i).getPrices() != null && issues.get(i).getPrices().size() > 0) {
                for (int j=0; j < issues.get(i).getPrices().size(); j++) {
                    if (((ComicPrice)(issues.get(i).getPrices().get(j))).getType()
                            .equalsIgnoreCase("digitalPurchasePrice")) {
                        holder.issue_price.setText(
                                String.valueOf(((ComicPrice)issues.get(i).getPrices().get(j)).getPrice()));
                        break;
                    }
                    if (j == issues.get(i).getPrices().size() - 1) {
                        holder.issue_price.setText(
                                String.valueOf(((ComicPrice)issues.get(i).getPrices().get(j)).getPrice()));
                    }
                }
            }

        }

        return view;
    }

    private boolean isValidIssue(Issue issue) {
        boolean valid = issue.getImage() != null && issue.getImage().getThumb_url() != null;
        valid = issue.getVolume() != null && issue.getVolume().getName() != null && valid;
        valid = issue.getIssue_number() != null && valid;
        valid = issue.getName() != null && valid;
        return valid;

    }

    @Override
    public Object getItem(int i) {
        return issues.get(i);
    }

    @Override
    public long getItemId(int i) {
        return issues.get(i).getId();
    }

}
