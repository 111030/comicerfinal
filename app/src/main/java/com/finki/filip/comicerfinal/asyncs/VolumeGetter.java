package com.finki.filip.comicerfinal.asyncs;

import android.content.Context;
import android.os.AsyncTask;

import com.finki.filip.comicerfinal.VolumeInfoScreen;
import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.gson.comicvine.Volume;

/**
 * Created by Filip on 06.2.2017.
 */

public class VolumeGetter extends AsyncTask<Void, Void, Volume> {

    private Context context;
    private int id;
    private String apiDetailUrl;

    public VolumeGetter(Context context, int id, String apiDetailUrl) {
        this.context = context;
        this.id = id;
        this.apiDetailUrl = apiDetailUrl;
    }

    @Override
    protected Volume doInBackground(Void... voids) {
        return MongoDBHelper.getVolume(id, apiDetailUrl);
    }

    @Override
    protected void onPostExecute(Volume volume) {
        if (volume != null) {
            ((VolumeInfoScreen)context).updateTheUI(volume);
        }
    }
}
