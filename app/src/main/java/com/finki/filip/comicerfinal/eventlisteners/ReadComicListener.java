package com.finki.filip.comicerfinal.eventlisteners;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import com.finki.filip.comicerfinal.ComicReader;

/**
 * Created by Filip on 13.2.2017.
 */

public class ReadComicListener implements View.OnClickListener {

    private Context context;
    private int id;

    public ReadComicListener(Context context, int id) {
        this.context = context;
        this.id = id;
    }
    @Override
    public void onClick(View view) {
        Intent intent = new Intent(context, ComicReader.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra("comicId", id);
        context.startActivity(intent);
    }
}
