package com.finki.filip.comicerfinal.asyncs;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.gson.comicvine.Issue;

/**
 * Created by Filip on 23.1.2017.
 */

public class IssueToReadingListPusher extends AsyncTask<Issue, Void, Integer> {

    private Context context;
    private TextView counter;

    public IssueToReadingListPusher(Context context, TextView counter) {

        this.context = context;
        this.counter = counter;
    }

    @Override
    protected Integer doInBackground(Issue... issues) {
        return MongoDBHelper.insertIntoReadingList(issues);
    }

    @Override
    protected void onPostExecute(Integer numUpdated) {
        if (numUpdated > 0) {
            Toast.makeText(context, "The comic issue is succesfully added into Reading List",
                    Toast.LENGTH_LONG).show();
            int count = Integer.parseInt(counter.getText().toString());
            counter.setText(String.valueOf(count + numUpdated));
            counter.setVisibility(View.VISIBLE);
        }
        else {
            Toast.makeText(context, "Issues already exist", Toast.LENGTH_LONG).show();
        }
    }
}