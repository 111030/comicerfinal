package com.finki.filip.comicerfinal.asyncs;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.finki.filip.comicerfinal.adapters.FavoritesAdapter;
import com.finki.filip.comicerfinal.factories.MongoDBHelper;

/**
 * Created by Filip on 22.1.2017.
 */

public class FavoritesUpdater extends AsyncTask<Integer, Void, Boolean> {

    private Context context;
    private FavoritesAdapter adapter;
    //

    public FavoritesUpdater(Context context, FavoritesAdapter adapter) {
        this.context = context;
        this.adapter = adapter;
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected Boolean doInBackground(Integer... indexes) {
        return MongoDBHelper.updateFavorites(adapter.getIssues());
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        if (aBoolean) {
            Toast.makeText(context, "Comics issue removed from favorites", Toast.LENGTH_LONG).show();
        }
        else {
            Toast.makeText(context, "Error occurred! Try again", Toast.LENGTH_LONG).show();
        }
    }
}
