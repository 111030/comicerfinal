package com.finki.filip.comicerfinal.gson.marvel;

import com.finki.filip.comicerfinal.gson.comicvine.Resource;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.io.Serializable;

public class ComicPrice extends Resource implements Serializable{
	
	private String type;
	private float price;
	
	public ComicPrice() {
		super();
	}

	public ComicPrice(String type, float price) {
		super();
		this.type = type;
		this.price = price;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	@Override
	public String toString() {
		JsonObject jo = new JsonObject();
		jo.addProperty("type", type);
		jo.addProperty("price", price);
		return new GsonBuilder().serializeNulls().create().toJson(jo);
	}
	
	

}
