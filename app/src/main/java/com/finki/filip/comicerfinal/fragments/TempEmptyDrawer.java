package com.finki.filip.comicerfinal.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.finki.filip.comicerfinal.R;

/**
 * Created by Filip on 17.4.2017.
 */

public class TempEmptyDrawer extends Fragment {

    public static TempEmptyDrawer newInstance() {
        TempEmptyDrawer ted = new TempEmptyDrawer();
        return ted;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.temp_empty_drawer, container, false);
    }
}
