package com.finki.filip.comicerfinal.fragments.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

/**
 * Created by Filip on 25.12.2016.
 */

public class SuccesfulRegistrationDialog extends DialogFragment implements DialogInterface.OnClickListener {

    public static SuccesfulRegistrationDialog newInstance() {
        SuccesfulRegistrationDialog srd = new SuccesfulRegistrationDialog();
        return srd;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder b = new AlertDialog.Builder(getActivity())
                .setTitle("Success")
                .setPositiveButton("Ok", this)
                .setMessage("Your registration was successful. Click on to button to return to the home screen");
        return b.create();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        if (i == AlertDialog.BUTTON_POSITIVE) {
            dismiss();
            getActivity().finish();
        }
    }
}
