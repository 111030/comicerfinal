package com.finki.filip.comicerfinal.eventlisteners;

import android.content.Context;
import android.view.View;
import android.widget.EditText;

import com.finki.filip.comicerfinal.adapters.CommentsAdapter;
import com.finki.filip.comicerfinal.asyncs.CommentSubmitter;
import com.finki.filip.comicerfinal.gson.comicvine.Issue;

import java.util.Date;

/**
 * Created by Filip on 30.1.2017.
 */

public class PostCommentListener implements View.OnClickListener {

    private Context context;
    private CommentsAdapter adapter;
    private EditText messageView;
    private Issue issue;

    public PostCommentListener(Context context, CommentsAdapter adapter, EditText messageView,
                               Issue issue) {
        this.context = context;
        this.adapter = adapter;
        this.messageView = messageView;
        this.issue = issue;
    }

    @Override
    public void onClick(View view) {
        String message = messageView.getText().toString();
        if (message != null && !message.isEmpty()) {
            String currDate = new Date().toString();
            new CommentSubmitter(context, adapter, issue).execute(message, currDate);
        }
    }
}
