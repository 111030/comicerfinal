package com.finki.filip.comicerfinal.asyncs;

import android.os.AsyncTask;

import com.finki.filip.comicerfinal.adapters.VolumesAdapter;
import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.gson.comicvine.Volume;

import java.util.List;

/**
 * Created by Filip on 06.2.2017.
 */

public class VolumesGridPopulator extends AsyncTask<Void, Void, List<Volume>> {

    private VolumesAdapter adapter;

    public VolumesGridPopulator(VolumesAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    protected List<Volume> doInBackground(Void... voids) {
        return MongoDBHelper.getVolumes();
    }

    @Override
    protected void onPostExecute(List<Volume> volumes) {
        adapter.addAll(volumes);
    }
}
