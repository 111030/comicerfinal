package com.finki.filip.comicerfinal.gson.comicvine;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class Response implements Serializable{
	
	private int status_code;
	private String error;
	private long number_of_total_results;
	private int number_of_page_results;
	private int limit;
	private int offset;
	
	public Response() {
	}

	public Response(int status_code, String error, long number_of_total_results, int number_of_page_results, int limit,
			int offset) {
		super();
		this.status_code = status_code;
		this.error = error;
		this.number_of_total_results = number_of_total_results;
		this.number_of_page_results = number_of_page_results;
		this.limit = limit;
		this.offset = offset;
	}

	public int getStatus_code() {
		return status_code;
	}

	public void setStatus_code(int status_code) {
		this.status_code = status_code;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public long getNumber_of_total_results() {
		return number_of_total_results;
	}

	public void setNumber_of_total_results(long number_of_total_results) {
		this.number_of_total_results = number_of_total_results;
	}

	public int getNumber_of_page_results() {
		return number_of_page_results;
	}

	public void setNumber_of_page_results(int number_of_page_results) {
		this.number_of_page_results = number_of_page_results;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}
	
	public String toString() {
		JsonObject jo = new JsonObject();
		jo.addProperty("status_code", status_code);
		jo.addProperty("error", error);
		jo.addProperty("number_of_total_results", number_of_total_results);
		jo.addProperty("number_of_page_results", number_of_page_results);
		jo.addProperty("limit", limit);
		jo.addProperty("offset", offset);
		return new GsonBuilder().serializeNulls().setPrettyPrinting().create().toJson(jo);
	}

}
