package com.finki.filip.comicerfinal.singletons;

import com.finki.filip.comicerfinal.gson.other.User;

/**
 * Created by Filip on 20.12.2016.
 */

public class UserSingleton {

    private User user;
    private static UserSingleton userSingleton;

    private UserSingleton(User user) {
        this.user = user;
    }

    public static synchronized UserSingleton getInstance() {
        if (userSingleton == null) {
            userSingleton = new UserSingleton(null);
        }
        return userSingleton;
    }

    public static void setUser(User user) {
        //if (user != null) {
            userSingleton = new UserSingleton(user);
        //}
    }

    public User getUser() {
        return user;
    }
}
