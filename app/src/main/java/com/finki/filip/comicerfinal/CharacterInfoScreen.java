package com.finki.filip.comicerfinal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.finki.filip.comicerfinal.asyncs.CharacterGetter;
import com.finki.filip.comicerfinal.asyncs.NavDrawerUser;
import com.finki.filip.comicerfinal.fragments.NavDrawerFragment;
import com.finki.filip.comicerfinal.fragments.TempEmptyDrawer;
import com.finki.filip.comicerfinal.gson.comicvine.Character;
import com.finki.filip.comicerfinal.gson.comicvine.Issue;
import com.finki.filip.comicerfinal.gson.comicvine.Movie;
import com.finki.filip.comicerfinal.gson.comicvine.Person;
import com.finki.filip.comicerfinal.gson.comicvine.Power;
import com.finki.filip.comicerfinal.gson.comicvine.Team;
import com.finki.filip.comicerfinal.singletons.UserSingleton;


/**
 * Created by Filip on 05.2.2017.
 */

public class CharacterInfoScreen extends Activity {

    private ImageView charImg;
    private TextView charBirth;
    private TextView charAppearances;
    private TextView charCreators;
    private TextView charFirstApperance;
    private TextView charPowers;
    private TextView charName;
    private LinearLayout charTeams;
    private LinearLayout charFriends;
    private LinearLayout charEnemies;
    private LinearLayout charIssuesDiedIn;
    private LinearLayout charMovies;
    //
    private ImageButton toggleDrawerButton;
    private TextView shoppingCartCounter;
    private TextView readingListCounter;
    private ImageButton shoppingCart;
    private ImageButton readingList;

    boolean drawerShow = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.character_info);
        //
        String apiDetailUrl = getIntent().getStringExtra("api_detail_url");
        int id = getIntent().getIntExtra("_id", -1);
        //
        CharacterGetter characterGetter = new CharacterGetter(this, id, apiDetailUrl);
        characterGetter.execute();
        //
        toggleDrawerButton = (ImageButton)findViewById(R.id.appbar_drawer_button);
        toggleDrawerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleNavigationDrawer();
            }
        });
        //
        shoppingCartCounter = (TextView)findViewById(R.id.appbar_shopping_cart_counter);
        readingListCounter = (TextView)findViewById(R.id.appbar_reading_list_counter);
        shoppingCart = (ImageButton)findViewById(R.id.appbar_shopping_cart);
        shoppingCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(CharacterInfoScreen.this, ShoppingCartScreen.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                i.putExtra("User", UserSingleton.getInstance().getUser() != null ?
                        UserSingleton.getInstance().getUser().getUsername() : "anonymous");
                startActivity(i);
            }
        });
        readingList = (ImageButton)findViewById(R.id.appbar_reading_list);
        readingList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(CharacterInfoScreen.this, ReadingListScreen.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                i.putExtra("User", UserSingleton.getInstance().getUser() != null ?
                        UserSingleton.getInstance().getUser().getUsername() : "anonymous");
                startActivity(i);
            }
        });
        getFragmentManager().beginTransaction()
                .add(R.id.nav_drawer_fragment, TempEmptyDrawer.newInstance())
                .commit();

    }

    @Override
    protected void onResume() {
        super.onResume();
        setCurrentSignedUser();
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
            drawerShow = false;
        }

    }

    private void setCurrentSignedUser() {
            WifiManager wm = (WifiManager)getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            String macAddress = wm.getConnectionInfo().getMacAddress();
            NavDrawerUser getCurrentUser = new NavDrawerUser(shoppingCartCounter, readingListCounter);
            getCurrentUser.execute(macAddress);
    }

    private void toggleNavigationDrawer() {
        if (drawerShow) {
            getFragmentManager().popBackStack();
            drawerShow = false;
            return;
        }
        drawerShow = true;
        getFragmentManager().beginTransaction()
                .setCustomAnimations(R.animator.drawer_to_right, R.animator.drawer_to_left,
                        R.animator.drawer_to_right, R.animator.drawer_to_left)
                .replace(R.id.nav_drawer_fragment, NavDrawerFragment.newInstance())
                .addToBackStack(null)
                .commit();
    }

    public void updateTheUI(Character character) {
        charImg = (ImageView)findViewById(R.id.character_screen_image);
        charBirth = (TextView)findViewById(R.id.character_screen_birth);
        charAppearances = (TextView)findViewById(R.id.character_screen_appearances);
        charCreators = (TextView)findViewById(R.id.character_screen_creators);
        charFirstApperance = (TextView)findViewById(R.id.character_screen_first_issue);
        charPowers = (TextView)findViewById(R.id.character_screen_powers);
        charName = (TextView)findViewById(R.id.character_screen_title);
        charTeams = (LinearLayout)findViewById(R.id.character_screen_teams);
        charFriends = (LinearLayout)findViewById(R.id.character_screen_friends);
        charEnemies = (LinearLayout)findViewById(R.id.character_screen_enemies);
        charIssuesDiedIn = (LinearLayout)findViewById(R.id.character_screen_died_in);
        charMovies = (LinearLayout)findViewById(R.id.character_screen_movies);
        //
            Glide.with(this).load(character.getImage().getMedium_url()).fitCenter().into(charImg);
            charBirth.setText(character.getBirth());
            charAppearances.setText(String.valueOf(character.getCount_of_issue_appearances()));
            Person[] creators = character.getCreators();
            StringBuilder sb = new StringBuilder();
            if (creators != null) {
                for (int i=0; i<creators.length; i++) {
                    sb.append(creators[i].getName());
                    if (i < creators.length - 1)
                        sb.append(",");
                }
            }
            charCreators.setText(sb.toString());
            charFirstApperance.setText(character.getFirst_appeared_in_issue().getName());
            Power[] powers = character.getPowers();
            sb = new StringBuilder();
            if (powers != null) {
                for (int i=0; i<powers.length; i++) {
                    sb.append(powers[i].getName());
                    if (i < powers.length - 1)
                        sb.append(",");
                }
            }
            charPowers.setText(sb.toString());
            charName.setText(character.getName() + " - " + character.getPublisher().getName());
            Team[] teams = character.getTeams();
            if (teams != null) {
                for (int i=0; i<teams.length; i++) {
                    if (teams[i].getImage() == null || teams[i].getImage().getThumb_url() == null)
                        continue;
                    View team = getLayoutInflater().inflate(R.layout.holder_horizontal, null);
                    ImageView imgHolder = (ImageView)team.findViewById(R.id.horizontal_holder_image);
                    TextView textHolder = (TextView)team.findViewById(R.id.horizontal_holder_text);
                    Glide.with(this).load(teams[i].getImage().getThumb_url())
                            .into(imgHolder);
                    textHolder.setText(teams[i].getName());
                    charTeams.addView(team);
                }
            }
            final Character[] friends = character.getCharacter_friends();
            if (friends != null) {
                for (int i=0; i<friends.length; i++) {
                    if (friends[i].getImage() == null || friends[i].getImage().getThumb_url() == null)
                        continue;
                    View friend = getLayoutInflater().inflate(R.layout.holder_horizontal, null);
                    ImageView imgHolder = (ImageView)friend.findViewById(R.id.horizontal_holder_image);
                    TextView textHolder = (TextView)friend.findViewById(R.id.horizontal_holder_text);
                    Glide.with(this).load(friends[i].getImage().getThumb_url())
                            .into(imgHolder);
                    textHolder.setText(friends[i].getName());
                    final int index = i;
                    charFriends.addView(friend);
                    friend.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(CharacterInfoScreen.this, CharacterInfoScreen.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            intent.putExtra("_id", friends[index].getId());
                            intent.putExtra("api_detail_url", friends[index].getApi_detail_url());
                            startActivity(intent);
                        }
                    });
                }
            }
            final Character[] enemies = character.getCharacter_enemies();
            if (enemies != null) {
                for (int i=0; i<enemies.length; i++) {
                    if (enemies[i].getImage() == null || enemies[i].getImage().getThumb_url() == null)
                        continue;
                    View enemy = getLayoutInflater().inflate(R.layout.holder_horizontal, null);
                    ImageView imgHolder = (ImageView)enemy.findViewById(R.id.horizontal_holder_image);
                    TextView textHolder = (TextView)enemy.findViewById(R.id.horizontal_holder_text);
                    Glide.with(this).load(enemies[i].getImage().getThumb_url())
                            .into(imgHolder);
                    textHolder.setText(enemies[i].getName());
                    final int index = i;
                    charEnemies.addView(enemy);
                    enemy.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(CharacterInfoScreen.this, CharacterInfoScreen.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            intent.putExtra("_id", enemies[index].getId());
                            intent.putExtra("api_detail_url", enemies[index].getApi_detail_url());
                            startActivity(intent);
                        }
                    });
                }
            }
            Issue[] deaths = character.getIssues_died_in();
            if (deaths != null) {
                for (int i=0; i<deaths.length; i++) {
                    if (deaths[i].getImage() == null || deaths[i].getImage().getThumb_url() == null)
                        continue;
                    View issue = getLayoutInflater().inflate(R.layout.holder_horizontal, null);
                    ImageView imgHolder = (ImageView)issue.findViewById(R.id.horizontal_holder_image);
                    TextView textHolder = (TextView)issue.findViewById(R.id.horizontal_holder_text);
                    Glide.with(this).load(deaths[i].getImage().getThumb_url())
                            .into(imgHolder);
                    textHolder.setText(deaths[i].getName());
                    charIssuesDiedIn.addView(issue);
                }
            }
            Movie[] movies = character.getMovies();
            if (movies != null) {
                for (int i=0; i<movies.length; i++) {
                    if (movies[i].getImage() == null || movies[i].getImage().getThumb_url() == null)
                        continue;
                    View movie = getLayoutInflater().inflate(R.layout.holder_horizontal, null);
                    ImageView imgHolder = (ImageView)movie.findViewById(R.id.horizontal_holder_image);
                    TextView textHolder = (TextView)movie.findViewById(R.id.horizontal_holder_text);
                    Glide.with(this).load(movies[i].getImage().getThumb_url())
                            .into(imgHolder);
                    textHolder.setText(movies[i].getName());
                    charMovies.addView(movie);
                }
            }



    }
}
