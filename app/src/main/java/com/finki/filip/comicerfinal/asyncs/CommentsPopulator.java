package com.finki.filip.comicerfinal.asyncs;

import android.os.AsyncTask;

import com.finki.filip.comicerfinal.adapters.CommentsAdapter;
import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.gson.comicvine.Issue;
import com.finki.filip.comicerfinal.gson.other.Comment;

import java.util.List;

/**
 * Created by Filip on 30.1.2017.
 */

public class CommentsPopulator extends AsyncTask<Issue, Void, List<Comment>> {

    private CommentsAdapter adapter;

    public CommentsPopulator(CommentsAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    protected List<Comment> doInBackground(Issue... issues) {
        return MongoDBHelper.getCommentsForIssue(issues[0]);
    }

    @Override
    protected void onPostExecute(List<Comment> comments) {
        if (comments != null && !comments.isEmpty()) {
            adapter.addAll(comments);
        }

    }
}
