package com.finki.filip.comicerfinal.gson.other;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

/**
 * Created by Filip on 19.12.2016.
 */

public class Country {

    private String name;
    private String initials;
    private String code;


    public Country() {}

    public Country(String name, String initials, String code) {
        this.name = name;
        this.initials = initials;
        this.code = code;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String toString() {
        JsonObject jo = new JsonObject();
        jo.addProperty("name", name);
        jo.addProperty("initials", initials);
        jo.addProperty("code", code);
        return new GsonBuilder().serializeNulls().create().toJson(jo);
    }


}
