package com.finki.filip.comicerfinal.asyncs;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.TextView;

import com.finki.filip.comicerfinal.RegisterScreen;
import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.fragments.dialogs.RegisterToSignInDialog;
import com.finki.filip.comicerfinal.gson.other.Country;
import com.finki.filip.comicerfinal.gson.other.User;


/**
 * Created by Filip on 24.12.2016.
 */

public class CheckBeforeRegister extends AsyncTask<String, Integer, Integer> {

    private RegisterToSignInDialog dialog;
    private Context context;
    private TextView usernameError;

    private static final int ALL_VALID = 0;
    private static final int USERNAME_EXISTS = 1;
    private static final int EMAIL_EXISTS = 2;

    public CheckBeforeRegister(Context context, TextView usernameError) {
        this.context = context;
        this.usernameError = usernameError;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        dialog = RegisterToSignInDialog.newInstance();

    }

    @Override
    protected Integer doInBackground(String... strings) {
        String username = strings[0];
        if (MongoDBHelper.checkAlreadyRegisteredUsername(username)) {
            return USERNAME_EXISTS;
        }

        String email = strings[1];
        if (MongoDBHelper.checkAlreadyRegisteredEmail(email)) {
            return EMAIL_EXISTS;
        }

        return ALL_VALID;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        //super.onPostExecute(integer);
        if (integer.intValue() == USERNAME_EXISTS) {
            usernameError.setVisibility(View.VISIBLE);
        }
        else if (integer.intValue() == EMAIL_EXISTS) {
            usernameError.setVisibility(View.GONE);
            dialog.show(((RegisterScreen)context).getFragmentManager().beginTransaction(), "REGISTER_TO_SIGN_IN_DIALOG");
        }
        else {
            //
            usernameError.setVisibility(View.GONE);
            RegisterScreen callingActivity = (RegisterScreen)context;
            User u = new User();
            u.setName(callingActivity.firstName.getText().toString());
            u.setSurname(callingActivity.surname.getText().toString());
            u.setEmail(callingActivity.email.getText().toString());
            u.setUsername(callingActivity.username.getText().toString());
            u.setPassword(callingActivity.password.getText().toString());
            u.setBorn(callingActivity.dateOfBirth.getText().toString());
            u.setGender((String)callingActivity.gender.getSelectedItem());
            u.setCountry(new Country((String)callingActivity.country.getSelectedItem(),"",""));
            u.setAddress(callingActivity.address.getText().toString());
            u.setCode(callingActivity.postalCode.getText().toString());
            u.setPhoneNumber(callingActivity.phoneNumber.getText().toString());
            u.setCredditCard(callingActivity.creditCard.getText().toString());
            new RegisterUser(context).execute(u);
        }

    }
}
