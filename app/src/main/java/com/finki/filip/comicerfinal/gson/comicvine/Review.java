package com.finki.filip.comicerfinal.gson.comicvine;

import java.io.Serializable;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class Review extends Resource implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5444964176736914814L;
	private String api_detail_url;
	private int _id;
	private String name;
	private String site_detail_url;
	
	public Review() {
		super();
	}

	public Review(String api_detail_url, int _id, String name, String site_detail_url) {
		super();
		this.api_detail_url = api_detail_url;
		this._id = _id;
		this.name = name;
		this.site_detail_url = site_detail_url;
	}

	public String getApi_detail_url() {
		return api_detail_url;
	}

	public void setApi_detail_url(String api_detail_url) {
		this.api_detail_url = api_detail_url;
	}

	public int getId() {
		return _id;
	}

	public void setId(int _id) {
		this._id = _id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSite_detail_url() {
		return site_detail_url;
	}

	public void setSite_detail_url(String site_detail_url) {
		this.site_detail_url = site_detail_url;
	}
	
	public String toString() {
		
		JsonObject jo = new JsonObject();
		jo.addProperty("api_detail_url", api_detail_url);
		jo.addProperty("_id", _id);
		jo.addProperty("name", name);
		jo.addProperty("site_detail_url", site_detail_url);
		return new GsonBuilder().serializeNulls().create().toJson(jo);
	}

	@Override
	public boolean equals(Object obj) {
		return this._id == ((Review)obj).getId();
	}
}
