package com.finki.filip.comicerfinal.asyncs;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.AsyncTask;

import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.fragments.SearchResult;
import com.finki.filip.comicerfinal.gson.comicvine.Resource;

import java.util.List;

/**
 * Created by Filip on 11.2.2017.
 */

public class SearchResultsGetter extends AsyncTask<String, Void, List<? extends Resource>> {

    private FragmentManager fm;
    private String collection;
    int fragmentId;

    public SearchResultsGetter(FragmentManager fm, String collection, int fragmentId) {

        this.fm = fm;
        this.collection = collection;
        this.fragmentId = fragmentId;
    }
    @Override
    protected List<? extends Resource> doInBackground(String... strings) {
        String query = strings[0];
        if (collection.equalsIgnoreCase("issues"))
            return MongoDBHelper.getIssueSearchResults(query);
        else if (collection.equalsIgnoreCase("volumes"))
            return MongoDBHelper.getVolumeSearchResults(query);
        else if (collection.equalsIgnoreCase("characters"))
            return MongoDBHelper.getCharacterSearchResults(query);
        else if (collection.equalsIgnoreCase("movies"))
            return MongoDBHelper.getMovieSearchResults(query);
        return null;
    }

    @Override
    protected void onPostExecute(List<? extends Resource> resources) {
        if (resources != null) {
            SearchResult searchFragment = (SearchResult)fm.findFragmentById(fragmentId);
            FragmentTransaction ft = fm.beginTransaction();
            if (searchFragment == null) {
                searchFragment = SearchResult.newInstance(collection, resources);
                ft.replace(fragmentId, searchFragment);
                ft.commit();
            }
            else {
                ft.remove(searchFragment);
                ft.commit();
            }
        }
    }
}
