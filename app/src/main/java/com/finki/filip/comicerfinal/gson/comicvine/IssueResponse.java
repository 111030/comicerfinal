package com.finki.filip.comicerfinal.gson.comicvine;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.finki.filip.comicerfinal.gson.marvel.ComicDate;
import com.finki.filip.comicerfinal.gson.marvel.ComicPrice;
import com.finki.filip.comicerfinal.gson.marvel.ComicSummary;
import com.finki.filip.comicerfinal.gson.marvel.Url;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class IssueResponse<T extends Serializable> extends Resource implements Serializable{

	/**
	 *
	 */
	private String aliases;
	private String api_detail_url;
	private CharacterResponse[] character_credits;
	private String cover_date;
	private List<ComicDate> dates;
	private String date_last_updated;
	private String deck;
	private String description;
	private int digitalId;
	private T has_staff_review;
	private int id;
	private Image image;
	private String issue_number;
	private int marvelId;
	private String name;
	private int numClicks;
	private PersonResponse[] person_credits;
	private float popularity;
	private List<ComicPrice> prices;
	private String site_detail_url;
	private TeamResponse[] team_credits;
	private List<Url> urls;
	private float user_review;
	private List<ComicSummary> variants;
	private VolumeResponse volume;

	public IssueResponse() {
		super();
		character_credits = new CharacterResponse[]{};
		person_credits = new PersonResponse[]{};
		team_credits = new TeamResponse[]{};
		dates = new ArrayList<>();
		prices = new ArrayList<>();
		urls = new ArrayList<>();
		variants = new ArrayList<>();
	}

	public IssueResponse(String aliases, String api_detail_url, CharacterResponse[] character_credits, String cover_date, List<ComicDate> dates, String date_last_updated, String deck,
				 String description, int digitalId, T has_staff_review, int id, Image image, String issue_number, int marvelId, String name,
				 int numClicks, PersonResponse[] person_credits, float popularity, List<ComicPrice> prices, String site_detail_url, TeamResponse[] team_credits, List<Url> urls, float user_review, List<ComicSummary> variants, VolumeResponse volume) {
		super();
		this.aliases = aliases;
		this.api_detail_url = api_detail_url;
		this.character_credits = character_credits;
		this.cover_date = cover_date;
		this.dates = dates;
		this.date_last_updated = date_last_updated;
		this.deck = deck;
		this.description = description;
		this.digitalId = digitalId;
		this.has_staff_review = has_staff_review;
		this.id = id;
		this.image = image;
		this.issue_number = issue_number;
		this.marvelId = marvelId;
		this.name = name;
		this.numClicks = numClicks;
		this.person_credits = person_credits;
		this.popularity = popularity;
		this.prices = prices;
		this.site_detail_url = site_detail_url;
		this.team_credits = team_credits;
		this.urls = urls;
		this.user_review = user_review;
		this.variants = variants;
		this.volume = volume;
	}


	public float getPopularity() {
		return popularity;
	}

	public void setPopularity(float popularity) {
		this.popularity = popularity;
	}

	public List<ComicDate> getDates() {
		return dates;
	}

	public void setDates(List<ComicDate> dates) {
		this.dates = dates;
	}

	public int getDigitalId() {
		return digitalId;
	}

	public void setDigitalId(int digitalId) {
		this.digitalId = digitalId;
	}

	public int getMarvelId() {
		return marvelId;
	}

	public void setMarvelId(int marvelId) {
		this.marvelId = marvelId;
	}

	public List<ComicPrice> getPrices() {
		return prices;
	}

	public void setPrices(List<ComicPrice> prices) {
		this.prices = prices;
	}

	public List<Url> getUrls() {
		return urls;
	}

	public void setUrls(List<Url> urls) {
		this.urls = urls;
	}

	public List<ComicSummary> getVariants() {
		return variants;
	}

	public void setVariants(List<ComicSummary> variants) {
		this.variants = variants;
	}

	public String getAliases() {
		return aliases;
	}

	public void setAliases(String aliases) {
		this.aliases = aliases;
	}

	public String getApi_detail_url() {
		return api_detail_url;
	}

	public void setApi_detail_url(String api_detail_url) {
		this.api_detail_url = api_detail_url;
	}

	public CharacterResponse[] getCharacter_credits() {
		return character_credits;
	}

	public void setCharacter_credits(CharacterResponse[] character_credits) {
		this.character_credits = character_credits;
	}

	public String getCover_date() {
		return cover_date;
	}

	public void setCover_date(String cover_date) {
		this.cover_date = cover_date;
	}

	public String getDate_last_updated() {
		return date_last_updated;
	}

	public void setDate_last_updated(String date_last_updated) {
		this.date_last_updated = date_last_updated;
	}

	public String getDeck() {
		return deck;
	}

	public void setDeck(String deck) {
		this.deck = deck;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public T getHas_staff_review() {
		return has_staff_review;
	}

	public void setHas_staff_review(T has_staff_review) {
		this.has_staff_review = has_staff_review;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public String getIssue_number() {
		return issue_number;
	}

	public void setIssue_number(String issue_number) {
		this.issue_number = issue_number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumClicks() {
		return numClicks;
	}

	public void setNumClicks(int numClicks) {
		this.numClicks = numClicks;
	}

	public PersonResponse[] getPerson_credits() {
		return person_credits;
	}

	public void setPerson_credits(PersonResponse[] person_credits) {
		this.person_credits = person_credits;
	}

	public String getSite_detail_url() {
		return site_detail_url;
	}

	public void setSite_detail_url(String site_detail_url) {
		this.site_detail_url = site_detail_url;
	}

	public TeamResponse[] getTeam_credits() {
		return team_credits;
	}

	public void setTeam_credits(TeamResponse[] team_credits) {
		this.team_credits = team_credits;
	}

	public float getUser_review() {
		return user_review;
	}

	public void setUser_review(float user_review) {
		this.user_review = user_review;
	}

	public VolumeResponse getVolume() {
		return volume;
	}

	public void setVolume(VolumeResponse volume) {
		this.volume = volume;
	}

	public String toString() {
		JsonObject jo = new JsonObject();
		jo.addProperty("aliases", aliases);
		jo.addProperty("api_detail_url", api_detail_url);
		JsonArray ja = new JsonArray();
		if (character_credits != null)
			for (int i=0; i<character_credits.length; i++) {
				ja.add(character_credits[i].toString());
			}
		jo.add("character_credits", ja);
		jo.addProperty("cover_date", cover_date);
		ja = new JsonArray();
		if (dates != null) {
			for (int i=0; i<dates.size(); i++) {
				ja.add(dates.get(i).toString());
			}
		}
		jo.add("dates", ja);
		jo.addProperty("date_last_updated", date_last_updated);
		jo.addProperty("deck", deck);
		jo.addProperty("description", description);
		jo.addProperty("digitalId", digitalId);
		if (has_staff_review != null)
			jo.addProperty("has_staff_review", has_staff_review.toString());
		else
			jo.addProperty("has_staff_review", (String)null);
		jo.addProperty("id", id);
		if (image != null)
			jo.addProperty("image", image.toString());
		else {
			jo.addProperty("image", (String)null);
		}
		jo.addProperty("issue_number", issue_number);
		jo.addProperty("marvelId", marvelId);
		jo.addProperty("name", name);
		jo.addProperty("numClicks", numClicks);
		ja = new JsonArray();
		if (prices != null) {
			for (int i=0; i<prices.size(); i++) {
				ja.add(prices.get(i).toString());
			}
		}
		ja = new JsonArray();
		if (person_credits != null) {
			for (int i=0; i<person_credits.length; i++) {
				ja.add(person_credits[i].toString());
			}
		}
		jo.add("person_credits", ja);
		jo.addProperty("popularity", popularity);
		jo.add("prices", ja);
		jo.addProperty("site_detail_url", site_detail_url);
		ja = new JsonArray();
		if (team_credits != null)
			for (int i=0; i<team_credits.length; i++) {
				ja.add(team_credits[i].toString());
			}
		jo.add("team_credits", ja);
		ja = new JsonArray();
		if (urls != null) {
			for (int i=0; i<urls.size(); i++) {
				ja.add(urls.get(i).toString());
			}
		}
		jo.add("urls", ja);
		jo.addProperty("user_review", user_review);
		ja = new JsonArray();
		if (variants != null) {
			for (int i=0; i<variants.size(); i++) {
				ja.add(variants.get(i).toString());
			}
		}
		jo.add("variants", ja);
		if (volume != null)
			jo.addProperty("volume", volume.toString());
		else {
			jo.addProperty("volume", (String)null);
		}
		return new GsonBuilder().serializeNulls().create().toJson(jo);
	}

	public boolean equals(Object o) {
		return this.id == ((IssueResponse)o).getId();
	}

	public Issue getIssue() {
		Issue ssue = new Issue();
		ssue.setAliases(aliases);
		ssue.setApi_detail_url(api_detail_url);
		Character[] characters = null;
		if (character_credits != null) {
			characters = new Character[character_credits.length];
			for (int i=0; i<character_credits.length; i++) {
				characters[i] = character_credits[i].getCharacter();
			}
		}
		ssue.setCharacter_credits(characters);
		ssue.setCover_date(cover_date);
		ssue.setDates(dates);
		ssue.setDate_last_updated(date_last_updated);
		ssue.setDeck(deck);
		ssue.setDescription(description);
		ssue.setDigitalId(digitalId);
		ssue.setHas_staff_review(has_staff_review);
		ssue.setId(id);
		ssue.setImage(image);
		ssue.setIssue_number(issue_number);
		ssue.setMarvelId(marvelId);
		ssue.setName(name);
		ssue.setNumClicks(numClicks);
		Person[] people = null;
		if (person_credits != null) {
			people = new Person[person_credits.length];
			for (int i=0; i<person_credits.length; i++) {
				people[i] = person_credits[i].getPerson();
			}
		}
		ssue.setPerson_credits(people);
		ssue.setPopularity(popularity);
		ssue.setPrices(prices);
		ssue.setSite_detail_url(site_detail_url);
		Team[] teams = null;
		if (team_credits != null) {
			teams = new Team[team_credits.length];
			for (int i=0; i<team_credits.length; i++) {
				teams[i] = team_credits[i].getTeam();
			}
		}
		ssue.setTeam_credits(teams);
		ssue.setUrls(urls);
		ssue.setUser_review(user_review);
		ssue.setVariants(variants);
		Volume ssueVolume = null;
		if (volume != null) {
			ssueVolume = volume.getVolume();
		}
		ssue.setVolume(ssueVolume);
		return ssue;
	}

}