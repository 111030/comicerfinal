package com.finki.filip.comicerfinal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.finki.filip.comicerfinal.adapters.CharactersAdapter;
import com.finki.filip.comicerfinal.asyncs.CharactersGridPopulator;
import com.finki.filip.comicerfinal.asyncs.NavDrawerUser;
import com.finki.filip.comicerfinal.fragments.NavDrawerFragment;
import com.finki.filip.comicerfinal.fragments.TempEmptyDrawer;
import com.finki.filip.comicerfinal.gson.comicvine.Character;
import com.finki.filip.comicerfinal.singletons.UserSingleton;

/**
 * Created by Filip on 04.2.2017.
 */

public class CharactersScreen extends Activity {

    private ImageButton toggleDrawerButton;
    private TextView shoppingCartCounter;
    private TextView readingListCounter;
    //
    private CharactersAdapter adapter;
    private GridView characters;
    private ImageButton shoppingCart;
    private ImageButton readingList;

    boolean drawerShow = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_characters);
        //
        toggleDrawerButton = (ImageButton)findViewById(R.id.appbar_drawer_button);
        toggleDrawerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleNavigationDrawer();
            }
        });
        //
        shoppingCartCounter = (TextView)findViewById(R.id.appbar_shopping_cart_counter);
        readingListCounter = (TextView)findViewById(R.id.appbar_reading_list_counter);
        //setCurrentSignedUser();
        //
        shoppingCart = (ImageButton)findViewById(R.id.appbar_shopping_cart);
        shoppingCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(CharactersScreen.this, ShoppingCartScreen.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                i.putExtra("User", UserSingleton.getInstance().getUser() != null ?
                        UserSingleton.getInstance().getUser().getUsername() : "anonymous");
                startActivity(i);
            }
        });
        readingList = (ImageButton)findViewById(R.id.appbar_reading_list);
        readingList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(CharactersScreen.this, ReadingListScreen.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                i.putExtra("User", UserSingleton.getInstance().getUser() != null ?
                        UserSingleton.getInstance().getUser().getUsername() : "anonymous");
                startActivity(i);
            }
        });
        //
        adapter = new CharactersAdapter(this);
        characters = (GridView)findViewById(R.id.characters_screen_characters);
        new CharactersGridPopulator(adapter).execute();
        characters.setAdapter(adapter);
        characters.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Character c = (Character)adapterView.getItemAtPosition(i);
                Intent intent = new Intent(CharactersScreen.this, CharacterInfoScreen.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.putExtra("_id", c.getId());
                intent.putExtra("api_detail_url", c.getApi_detail_url());
                startActivity(intent);
            }
        });
        getFragmentManager().beginTransaction()
                .add(R.id.nav_drawer_fragment, TempEmptyDrawer.newInstance())
                .commit();


    }

    @Override
    protected void onResume() {
        super.onResume();
        setCurrentSignedUser();
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
            drawerShow = false;
        }

    }

    private void setCurrentSignedUser() {
            WifiManager wm = (WifiManager)getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            String macAddress = wm.getConnectionInfo().getMacAddress();
            NavDrawerUser getCurrentUser = new NavDrawerUser(shoppingCartCounter, readingListCounter);
            getCurrentUser.execute(macAddress);
    }

    private void toggleNavigationDrawer() {
        if (drawerShow) {
            getFragmentManager().popBackStack();
            drawerShow = false;
            return;
        }
        drawerShow = true;
        getFragmentManager().beginTransaction()
                .setCustomAnimations(R.animator.drawer_to_right, R.animator.drawer_to_left,
                        R.animator.drawer_to_right, R.animator.drawer_to_left)
                .replace(R.id.nav_drawer_fragment, NavDrawerFragment.newInstance())
                .addToBackStack(null)
                .commit();
    }
}
