package com.finki.filip.comicerfinal.gson.other;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.util.Date;

/**
 * Created by Filip on 20.12.2016.
 */

public class SignedIn {

    private User user;
    private String macAddress;
    private int ipAddress;
    private String dateTime;

    public SignedIn() {}

    public SignedIn(User user, String macAddress, int ipAddress, String dateTime) {
        this.user = user;
        this.macAddress = macAddress;
        this.ipAddress = ipAddress;
        this.dateTime = dateTime;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public int getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(int ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String toString() {
        JsonObject jo = new JsonObject();
        if (user != null)
            jo.addProperty("user", user.toString());
        else
            jo.addProperty("user", (String)null);
        jo.addProperty("macAddress", macAddress);
        jo.addProperty("ipAddress", ipAddress);
        jo.addProperty("dateTime", dateTime);
        return new GsonBuilder().serializeNulls().create().toJson(jo);
    }
}
