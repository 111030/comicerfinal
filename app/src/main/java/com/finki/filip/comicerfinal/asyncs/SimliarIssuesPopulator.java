package com.finki.filip.comicerfinal.asyncs;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.finki.filip.comicerfinal.IssueInfoScreen;
import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.gson.comicvine.Issue;

import java.util.List;

/**
 * Created by Filip on 09.2.2017.
 */

public class SimliarIssuesPopulator extends AsyncTask<Issue, Void, List<Issue>> {

    private Context context;
    private LinearLayout similar;

    public SimliarIssuesPopulator(Context context, LinearLayout similar) {
        this.context = context;
        this.similar = similar;
    }

    @Override
    protected List<Issue> doInBackground(Issue... issues) {
        return MongoDBHelper.getSimilarIssues(issues[0]);
    }

    @Override
    protected void onPostExecute(final List<Issue> issues) {
        if (issues != null) {
            ImageView charImgs[] = new ImageView[issues.size()];
            for (int i=0; i<issues.size(); i++) {
                if (issues.get(i).getImage() == null)
                    continue;
                charImgs[i] = new ImageView(context);
                charImgs[i].setId(issues.get(i).getId());
                //charImgs[i].setMaxWidth(100);
                //charImgs[i].setMaxHeight(180);
                charImgs[i].setLayoutParams(new ViewGroup.LayoutParams(196, 256));
                charImgs[i].setScaleType(ImageView.ScaleType.FIT_XY);
                charImgs[i].setPadding(16, 16, 16, 16);
                Glide.with(context).load(issues.get(i).
                        getImage().getIcon_url()).into(charImgs[i]);
                final int index = i;
                similar.addView(charImgs[i]);
                charImgs[i].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, IssueInfoScreen.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        intent.putExtra("_id", issues.get(index).getId());
                        intent.putExtra("api_detail_url", issues.get(index).getApi_detail_url());
                        context.startActivity(intent);
                    }
                });
            }
        }
    }
}
