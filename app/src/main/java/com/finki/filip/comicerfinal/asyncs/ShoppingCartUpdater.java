package com.finki.filip.comicerfinal.asyncs;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.TextView;

import com.finki.filip.comicerfinal.R;
import com.finki.filip.comicerfinal.ShoppingCartScreen;
import com.finki.filip.comicerfinal.adapters.ShoppingCartAdapter;
import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.gson.comicvine.Issue;
import com.finki.filip.comicerfinal.singletons.MongoSingleton;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by Filip on 21.1.2017.
 */

public class ShoppingCartUpdater extends AsyncTask<Void, Void, Integer> {

    private ShoppingCartAdapter adapter;
    private Context context;
    private int index;
    private List<Issue> issues;

    public ShoppingCartUpdater(
            ShoppingCartAdapter adapter, Context context, int index, List<Issue> issues) {
        this.adapter = adapter;
        this.context = context;
        this.index = index;
        this.issues = issues;
    }

    @Override
    protected Integer doInBackground(Void... voids) {
        return MongoDBHelper.updateShoppingCartForUser(issues, index,
                MongoSingleton.getInstance().getDatabase());
    }

    @Override
    protected void onPostExecute(Integer totalModified) {

        ShoppingCartScreen scs = (ShoppingCartScreen)context;
        TextView shoppingCart = (TextView)scs.findViewById(R.id.appbar_shopping_cart_counter);
        int value = Integer.valueOf(shoppingCart.getText().toString());
        shoppingCart.setText(String.valueOf(value - totalModified));
        if (value - totalModified <= 0) {
            shoppingCart.setVisibility(View.INVISIBLE);
        }
        adapter.notifyDataSetChanged();
        TextView total = (TextView)scs.findViewById(R.id.shopping_cart_total);
        total.setText(new DecimalFormat("#####.00").format(adapter.getTotal())+"$");
    }
}
