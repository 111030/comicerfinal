package com.finki.filip.comicerfinal.factories;

/**
 * Created by Filip on 05.2.2017.
 */

public class ComicVineLinks {

    public static final String keyFormat = "?api_key=d7ff6b517060c4395d14ade98b76479f7bea88e0" +
            "&format=json";
    public static final String characterResourceFields = "&field_list=birth," +
            "count_of_issue_appearances,creators,first_appeared_in_issue,powers," +
            "teams,character_friends,character_enemies,issues_died_in,movies";
    public static final String volumeResourceFields = "&field_list=people,characters";
}
