package com.finki.filip.comicerfinal;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.finki.filip.comicerfinal.asyncs.SearchResultsGetter;

/**
 * Created by Filip on 11.2.2017.
 */

public class SearchResultsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results);
        //
        Intent intent = getIntent();
        //if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra("query");
            new SearchResultsGetter(getFragmentManager(), "issues", R.id.search_screen_issues)
                    .execute(query);
            new SearchResultsGetter(getFragmentManager(), "volumes", R.id.search_screen_volumes)
                    .execute(query);
            new SearchResultsGetter(getFragmentManager(), "characters", R.id.search_screen_characters)
                    .execute(query);
            new SearchResultsGetter(getFragmentManager(), "movies", R.id.search_screen_movies)
                    .execute(query);
        //}
    }

}
