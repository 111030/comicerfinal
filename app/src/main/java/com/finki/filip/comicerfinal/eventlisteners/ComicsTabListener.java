package com.finki.filip.comicerfinal.eventlisteners;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.support.annotation.Dimension;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.finki.filip.comicerfinal.R;
import com.finki.filip.comicerfinal.asyncs.MainScreenAdaptersPopulator;
import com.finki.filip.comicerfinal.fragments.MainScreenPublisherComics;
import com.finki.filip.comicerfinal.holders.MainScreenIssuesAdapters;

import java.util.Map;


/**
 * Created by Filip on 08.1.2017.
 */

public class ComicsTabListener implements View.OnClickListener {

    private Context context;
    private LinearLayout tabsLayout;
    private Map<Integer, Integer> tabsScreens;
    private FragmentManager fragmentManager;
    private MainScreenIssuesAdapters[] adapters;

    public ComicsTabListener(Context context,
                             LinearLayout tabsLayout,
                             Map<Integer, Integer> tabsScreens,
                             FragmentManager fragmentManager,
                             MainScreenIssuesAdapters[] adapters ) {
        this.context = context;
        this.tabsLayout = tabsLayout;
        this.tabsScreens = tabsScreens;
        this.fragmentManager = fragmentManager;
        this.adapters = adapters;
    }
    @Override
    public void onClick(View view) {
        for (int i=0; i<tabsLayout.getChildCount(); i++) {
            Button tab = (Button)tabsLayout.getChildAt(i);
            tab.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            tab.setTextSize(Dimension.SP, 18.0f);
        }
        Button current = (Button)view;
        current.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.tab_focuser);
        current.setTextSize(Dimension.SP, current.getTextSize()*1.2f);
        showMainScreenFragment(tabsScreens.get(current.getId()));
    }

    public void showMainScreenFragment(int index) {
        MainScreenPublisherComics currFragment = (MainScreenPublisherComics)fragmentManager.
                findFragmentById(R.id.main_screen_publisher_comics);
        if (currFragment == null || currFragment.getShownIndex() != index) {
            if (adapters[index] == null) {
                adapters[index] = new MainScreenIssuesAdapters(context, 4);
                new MainScreenAdaptersPopulator(context, adapters[index]).execute(index);
            }
            currFragment = MainScreenPublisherComics.newInstance(index);
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.replace(R.id.main_screen_publisher_comics, currFragment);
            ft.commit();
        }
    }
}
