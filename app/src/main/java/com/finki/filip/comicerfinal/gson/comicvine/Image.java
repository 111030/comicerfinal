package com.finki.filip.comicerfinal.gson.comicvine;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.io.Serializable;

public class Image implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3004204367526342502L;
	private String icon_url;
	private String medium_url;
	private String screen_url;
	private String small_url;
	private String super_url;
	private String thumb_url;
	private String tiny_url;
	
	public Image() {
	
	}

	public Image(String icon_url, String medium_url, String screen_url, String small_url, String super_url,
			String thumb_url, String tiny_url) {
		super();
		this.icon_url = icon_url;
		this.medium_url = medium_url;
		this.screen_url = screen_url;
		this.small_url = small_url;
		this.super_url = super_url;
		this.thumb_url = thumb_url;
		this.tiny_url = tiny_url;
	}

	public String getIcon_url() {
		return icon_url;
	}

	public void setIcon_url(String icon_url) {
		this.icon_url = icon_url;
	}

	public String getMedium_url() {
		return medium_url;
	}

	public void setMedium_url(String medium_url) {
		this.medium_url = medium_url;
	}

	public String getScreen_url() {
		return screen_url;
	}

	public void setScreen_url(String screen_url) {
		this.screen_url = screen_url;
	}

	public String getSmall_url() {
		return small_url;
	}

	public void setSmall_url(String small_url) {
		this.small_url = small_url;
	}

	public String getSuper_url() {
		return super_url;
	}

	public void setSuper_url(String super_url) {
		this.super_url = super_url;
	}

	public String getThumb_url() {
		return thumb_url;
	}

	public void setThumb_url(String thumb_url) {
		this.thumb_url = thumb_url;
	}

	public String getTiny_url() {
		return tiny_url;
	}

	public void setTiny_url(String tiny_url) {
		this.tiny_url = tiny_url;
	}
	
	public String toString() {
		JsonObject jo = new JsonObject();
		jo.addProperty("icon_url", icon_url);
		jo.addProperty("medium_url", medium_url);
		jo.addProperty("screen_url", screen_url);
		jo.addProperty("small_url", small_url);
		jo.addProperty("super_url", super_url);
		jo.addProperty("thumb_url", thumb_url);
		jo.addProperty("tiny_url", tiny_url);
		return new GsonBuilder().serializeNulls().create().toJson(jo);
	}

}
