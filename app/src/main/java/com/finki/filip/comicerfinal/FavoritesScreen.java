package com.finki.filip.comicerfinal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.TextView;

import com.finki.filip.comicerfinal.adapters.FavoritesAdapter;
import com.finki.filip.comicerfinal.asyncs.FavoritesPopulator;
import com.finki.filip.comicerfinal.asyncs.NavDrawerUser;
import com.finki.filip.comicerfinal.fragments.NavDrawerFragment;
import com.finki.filip.comicerfinal.fragments.TempEmptyDrawer;
import com.finki.filip.comicerfinal.singletons.UserSingleton;

/**
 * Created by Filip on 21.1.2017.
 */

public class FavoritesScreen extends Activity {

    private GridView favorites;
    private TextView error;
    private TextView shoppingCartCounter;
    private TextView readingListCounter;
    private ImageButton toggleDrawerButton;
    private ImageButton shoppingCart;
    private ImageButton readingList;

    boolean drawerShow = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);
        //
        error = (TextView)findViewById(R.id.favorites_error);
        favorites = (GridView)findViewById(R.id.favorites_grid);
        shoppingCartCounter = (TextView)findViewById(R.id.appbar_shopping_cart_counter);
        readingListCounter = (TextView)findViewById(R.id.appbar_reading_list_counter);
        //setCurrentSignedUser();
        toggleDrawerButton = (ImageButton)findViewById(R.id.appbar_drawer_button);
        toggleDrawerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleNavigationDrawer();
            }
        });
        shoppingCart = (ImageButton)findViewById(R.id.appbar_shopping_cart);
        shoppingCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(FavoritesScreen.this, ShoppingCartScreen.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                i.putExtra("User", UserSingleton.getInstance().getUser() != null ?
                        UserSingleton.getInstance().getUser().getUsername() : "anonymous");
                startActivity(i);
            }
        });
        readingList = (ImageButton)findViewById(R.id.appbar_reading_list);
        readingList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(FavoritesScreen.this, ReadingListScreen.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                i.putExtra("User", UserSingleton.getInstance().getUser() != null ?
                        UserSingleton.getInstance().getUser().getUsername() : "anonymous");
                startActivity(i);
            }
        });
        //
        if (UserSingleton.getInstance().getUser() == null) {
            error.setVisibility(View.VISIBLE);
            favorites.setVisibility(View.GONE);
        }
        else {
            error.setVisibility(View.GONE);
            favorites.setVisibility(View.VISIBLE);
            FavoritesAdapter adapter = new FavoritesAdapter(this, shoppingCartCounter);
            new FavoritesPopulator(adapter).execute();
            favorites.setAdapter(adapter);
            favorites.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Intent intent = new Intent(FavoritesScreen.this, IssueInfoScreen.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.putExtra("Issue", adapterView.getSelectedItemId());
                    startActivity(intent);
                }
            });
        }
        getFragmentManager().beginTransaction()
                .add(R.id.favorites_drawer, TempEmptyDrawer.newInstance())
                .commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setCurrentSignedUser();
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
            drawerShow = false;
        }

    }

    private void setCurrentSignedUser() {
            WifiManager wm = (WifiManager)getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            String macAddress = wm.getConnectionInfo().getMacAddress();
            NavDrawerUser getCurrentUser = new NavDrawerUser(shoppingCartCounter, readingListCounter);
            getCurrentUser.execute(macAddress);
    }

    private void toggleNavigationDrawer() {
        if (drawerShow) {
            getFragmentManager().popBackStack();
            drawerShow = false;
            return;
        }
        drawerShow = true;
        getFragmentManager().beginTransaction()
                .setCustomAnimations(R.animator.drawer_to_right, R.animator.drawer_to_left,
                        R.animator.drawer_to_right, R.animator.drawer_to_left)
                .replace(R.id.favorites_drawer, NavDrawerFragment.newInstance())
                .addToBackStack(null)
                .commit();
    }
}
