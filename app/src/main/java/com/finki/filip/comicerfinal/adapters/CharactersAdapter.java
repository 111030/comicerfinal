package com.finki.filip.comicerfinal.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.finki.filip.comicerfinal.R;
import com.finki.filip.comicerfinal.gson.comicvine.Character;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Filip on 04.2.2017.
 */

public class CharactersAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    private List<Character> characters;

    public CharactersAdapter(Context context) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        characters = new ArrayList<>();
    }

    public void addAll(List<Character> characters) {
        this.characters.addAll(characters);
        notifyDataSetChanged();
    }

    public void add(Character character) {
        characters.add(character);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return characters.size();
    }

    @Override
    public Object getItem(int i) {
        return characters.get(i);
    }

    @Override
    public long getItemId(int i) {
        return characters.get(i).getId();
    }

    static class CharacterHolder {
        ImageView img;
        TextView name;
        TextView firstAppearance;
        TextView volume;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        CharacterHolder holder;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.character_grid_item, null);
            holder = new CharacterHolder();
            holder.img = (ImageView)view.findViewById(R.id.character_grid_img);
            holder.name = (TextView)view.findViewById(R.id.character_grid_name);
            holder.firstAppearance = (TextView)view.findViewById(R.id.character_grid_first_issue);
            holder.volume = (TextView)view.findViewById(R.id.character_grid_volume_title);
            view.setTag(holder);
        }
        else {
            holder = (CharacterHolder)view.getTag();
        }
        Character current = characters.get(i);
        if (current.getImage() != null && current.getImage().getThumb_url() != null) {
            Glide.with(context).load(current.getImage().getThumb_url()).fitCenter()
                    .into(holder.img);
        }

        holder.name.setText(current.getName());
        holder.firstAppearance.setText(current.getFirst_appeared_in_issue().getName());
        holder.volume.setText(current.getPublisher().getName());
        return view;
    }
}
