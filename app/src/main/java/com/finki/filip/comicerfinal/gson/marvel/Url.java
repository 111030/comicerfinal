package com.finki.filip.comicerfinal.gson.marvel;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class Url {
	
	private String type;
	private String url;
	
	public Url() {
		super();
	}

	public Url(String type, String url) {
		super();
		this.type = type;
		this.url = url;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		JsonObject jo = new JsonObject();
		jo.addProperty("type", type);
		jo.addProperty("url", url);
		return new GsonBuilder().serializeNulls().create().toJson(jo);
	}
	
	

}
