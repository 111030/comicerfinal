package com.finki.filip.comicerfinal.eventlisteners;

import android.view.View;
import android.widget.PopupMenu;

/**
 * Created by Filip on 22.1.2017.
 */

public class FavoritesOptionsListener implements View.OnClickListener {

    private PopupMenu popupMenu;

    public FavoritesOptionsListener(PopupMenu popupMenu) {
        this.popupMenu = popupMenu;
    }

    @Override
    public void onClick(View view) {
        popupMenu.show();
    }
}
