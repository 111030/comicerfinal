package com.finki.filip.comicerfinal.asyncs;

import android.os.AsyncTask;

import com.dropbox.core.DbxDownloader;
import com.dropbox.core.DbxException;
import com.dropbox.core.v2.DbxClientV2;
import com.dropbox.core.v2.files.DownloadErrorException;
import com.dropbox.core.v2.files.FileMetadata;
import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.singletons.DropboxClientFactory;
import com.github.barteksc.pdfviewer.PDFView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Filip on 12.2.2017.
 */

public class DropboxAsync extends AsyncTask<Integer, Object, byte[]> {

    private DbxClientV2 dbxClient;
    final int BUFFER = 2048;
    PDFView pdfView;

    public DropboxAsync(PDFView pdfView) {

        this.pdfView = pdfView;
    }

    @Override
    protected byte[] doInBackground(Integer... ints) {
        String readingLink = MongoDBHelper.getDropboxLink(ints[0]);
        dbxClient = DropboxClientFactory.getClient();
        try {
            DbxDownloader<FileMetadata> downloader = dbxClient.files().download(readingLink);
            InputStream is = downloader.getInputStream();
            byte[] data = new byte[BUFFER];
            int count;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            while ((count = is.read(data, 0, BUFFER)) != -1) {
                baos.write(data, 0, count);
            }
            //
            byte[] result = baos.toByteArray();
            baos.close();
            is.close();
            downloader.close();
            return result;
        } catch (DownloadErrorException e) {
            e.printStackTrace();
        } catch (DbxException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void onPostExecute(byte[] data) {
        if (data != null) {
            pdfView.fromBytes(data).load();
        }
        else {
        }
    }

}
