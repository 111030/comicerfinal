package com.finki.filip.comicerfinal.eventlisteners;

import android.view.View;
import android.widget.TextView;

import com.finki.filip.comicerfinal.R;
import com.finki.filip.comicerfinal.asyncs.LikeAComment;
import com.finki.filip.comicerfinal.gson.comicvine.Issue;
import com.finki.filip.comicerfinal.gson.other.Comment;
import com.finki.filip.comicerfinal.gson.other.User;
import com.finki.filip.comicerfinal.singletons.UserSingleton;

/**
 * Created by Filip on 29.1.2017.
 */

public class LikeDislikeListener implements View.OnClickListener {

    private Issue issue;
    private TextView likesCounter;
    private int commentIndex;

    public LikeDislikeListener(Issue issue, TextView likesCounter, int commentIndex) {
        this.issue = issue;
        this.likesCounter = likesCounter;
        this.commentIndex = commentIndex;
    }

    @Override
    public void onClick(View view) {
        Comment comment = (Comment)issue.getComments().get(commentIndex);
        User current = UserSingleton.getInstance().getUser();
        if (view.getId() == R.id.issue_info_comment_btn_tup) {
            boolean alreadyExists = false;
            for (int i=0; i<comment.getLikes().size(); i++) {
                if (current.getUsername().equalsIgnoreCase(comment.getLikes().get(i).getUsername())) {
                    alreadyExists = true;
                    break;
                }
            }
            if (comment.getUser().getUsername().equals(current.getUsername())) {
                alreadyExists = true;
            }
            if (!alreadyExists) {
                new LikeAComment(issue, likesCounter).execute(commentIndex);
                //
            }
        }
    }
}
