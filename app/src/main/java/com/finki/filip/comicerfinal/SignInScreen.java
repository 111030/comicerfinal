package com.finki.filip.comicerfinal;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.finki.filip.comicerfinal.asyncs.SignIn;

/**
 * Created by Filip on 25.12.2016.
 */

public class SignInScreen extends Activity {

    public EditText username;
    public EditText password;
    //
    public TextView usernameError;
    public TextView passwordError;
    //

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signin_modal);
        //
        username = (EditText)findViewById(R.id.dialog_signin_username);
        password = (EditText)findViewById(R.id.dialog_signin_password);
        //
        usernameError = (TextView)findViewById(R.id.dialog_signin_username_error);
        passwordError = (TextView)findViewById(R.id.dialog_signin_password_error);

        //
        Bundle registerBundle = getIntent().getExtras();
        if (registerBundle != null) {
            String emailBundle = registerBundle.getString("EMAIL");
            username.setText(emailBundle);
        }

        //
        Button signIn = (Button)findViewById(R.id.dialog_signin_signin);
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new SignIn(SignInScreen.this).execute(username.getText().toString(),
                        password.getText().toString());
            }
        });
        //
        ImageView close = (ImageView) findViewById(R.id.dialog_close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
