package com.finki.filip.comicerfinal.asyncs;

import android.os.AsyncTask;
import android.widget.TextView;

import com.finki.filip.comicerfinal.adapters.ShoppingCartAdapter;
import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.gson.comicvine.Issue;
import com.finki.filip.comicerfinal.gson.other.User;
import com.finki.filip.comicerfinal.singletons.MongoSingleton;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by Filip on 20.1.2017.
 */

public class ShoppingCartPopulator extends AsyncTask<User, Void, List<Issue>> {

    private ShoppingCartAdapter shoppingCartAdapter;
    private TextView total;

    public ShoppingCartPopulator(ShoppingCartAdapter shoppingCartAdapter
    ,TextView total) {
        this.shoppingCartAdapter = shoppingCartAdapter;
        this.total = total;
    }

    @Override
    protected List<Issue> doInBackground(User... users) {
        return MongoDBHelper.getShoppingCartItemsList(users[0], MongoSingleton.getInstance().getDatabase());
    }

    @Override
    protected void onPostExecute(List<Issue> issues) {
        if (issues != null) {
            shoppingCartAdapter.addAll(issues);
            total.setText(new DecimalFormat("#####.00").format(shoppingCartAdapter.getTotal()) + "$");
        }
    }
}
