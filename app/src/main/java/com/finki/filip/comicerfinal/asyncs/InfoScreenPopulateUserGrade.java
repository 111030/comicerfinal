package com.finki.filip.comicerfinal.asyncs;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.ImageView;
import android.widget.TextView;

import com.finki.filip.comicerfinal.R;
import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.gson.comicvine.Issue;

import java.util.List;

/**
 * Created by Filip on 28.1.2017.
 */

public class InfoScreenPopulateUserGrade extends AsyncTask<Issue, Void, Integer> {

    private Context context;
    private List<ImageView> gradeStars;
    private TextView gradesCounts;
    private Issue currentIssue;

    public InfoScreenPopulateUserGrade(Context context, List<ImageView> gradeStars
    ,TextView gradesCounts) {
        this.context = context;
        this.gradeStars = gradeStars;
        this.gradesCounts = gradesCounts;
    }

    @Override
    protected Integer doInBackground(Issue... issues) {
        currentIssue = issues[0];
        return MongoDBHelper.getRatingByUserForIssue(currentIssue);
    }

    @Override
    protected void onPostExecute(Integer integer) {
        if (integer > -1) {
            for (int i=0; i<integer; i++) {
                gradeStars.get(i).setImageResource(R.drawable.ic_star_black_36dp);
            }
        }
        new GetRatingsCount(gradesCounts).execute(currentIssue);

    }
}
