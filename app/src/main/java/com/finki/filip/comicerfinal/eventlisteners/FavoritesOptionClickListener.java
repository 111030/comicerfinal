package com.finki.filip.comicerfinal.eventlisteners;

import android.content.Context;
import android.content.Intent;
import android.view.MenuItem;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.finki.filip.comicerfinal.R;
import com.finki.filip.comicerfinal.SignInScreen;
import com.finki.filip.comicerfinal.adapters.FavoritesAdapter;
import com.finki.filip.comicerfinal.asyncs.FavoritesUpdater;
import com.finki.filip.comicerfinal.asyncs.IssueToCartPusher;
import com.finki.filip.comicerfinal.gson.comicvine.Issue;
import com.finki.filip.comicerfinal.gson.marvel.ComicPrice;
import com.finki.filip.comicerfinal.gson.other.User;
import com.finki.filip.comicerfinal.singletons.UserSingleton;

import java.util.List;

/**
 * Created by Filip on 22.1.2017.
 */

public class FavoritesOptionClickListener implements PopupMenu.OnMenuItemClickListener {

    private Context context;
    private FavoritesAdapter adapter;
    private int itemIndex;
    private TextView shoppingCartCounter;

    public FavoritesOptionClickListener(Context context, FavoritesAdapter adapter, int itemIndex,
                                        TextView shoppingCartCounter) {
        this.context = context;
        this.adapter = adapter;
        this.itemIndex = itemIndex;
        this.shoppingCartCounter = shoppingCartCounter;
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        if (menuItem.getItemId() == R.id.menu_item_buy) {
            User current = UserSingleton.getInstance().getUser();
            if (current == null) {
                Intent intent = new Intent(context, SignInScreen.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                context.startActivity(intent);
            }
            else {
                Issue issue = (Issue)adapter.getItem(itemIndex);
                List<ComicPrice> prices = issue.getPrices();
                if (prices != null && !prices.isEmpty())
                    new IssueToCartPusher(context, shoppingCartCounter).
                            execute(current,issue);
                else
                    Toast.makeText(context, "This comic issue can't be bought", Toast.LENGTH_LONG).
                            show();
            }
        }
        else if (menuItem.getItemId() == R.id.menu_item_remove) {
            adapter.removeIssue(itemIndex);
            new FavoritesUpdater(context, adapter).execute();
        }
        else {
            //
        }
        return true;
    }
}
