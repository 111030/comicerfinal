package com.finki.filip.comicerfinal.gson.comicvine;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.Serializable;

public class Person extends Resource implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -708266045137134440L;
	private String aliases;
	private String api_detail_url;
	private String country;
	private Character[] created_characters;
	private String deck;
	private String description;
	private int _id;
	private String name;
	private Volume[] volume_credits;
	
	public Person() {
		super();
		created_characters = new Character[]{};
		volume_credits = new Volume[]{};
	}

	public Person(String aliases, String api_detail_url, String country, Character[] created_characters, 
			String deck, String description, int _id, String name, Volume[] volume_credits) {
		super();
		this.aliases = aliases;
		this.api_detail_url = api_detail_url;
		this.country = country;
		this.created_characters = created_characters;
		this.deck = deck;
		this.description = description;
		this._id = _id;
		this.name = name;
		this.volume_credits = volume_credits;
	}

	public String getAliases() {
		return aliases;
	}

	public void setAliases(String aliases) {
		this.aliases = aliases;
	}

	public String getApi_detail_url() {
		return api_detail_url;
	}

	public void setApi_detail_url(String api_detail_url) {
		this.api_detail_url = api_detail_url;
	}
	
	

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Character[] getCreated_characters() {
		return created_characters;
	}

	public void setCreated_characters(Character[] created_characters) {
		this.created_characters = created_characters;
	}

	public String getDeck() {
		return deck;
	}

	public void setDeck(String deck) {
		this.deck = deck;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getId() {
		return _id;
	}

	public void setId(int _id) {
		this._id = _id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	public Volume[] getVolume_credits() {
		return volume_credits;
	}

	public void setVolume_credits(Volume[] volume_credits) {
		this.volume_credits = volume_credits;
	}

	public String toString() {
		JsonObject jo = new JsonObject();
		jo.addProperty("aliases", aliases);
		jo.addProperty("api_detail_url", api_detail_url);
		jo.addProperty("country", country);
		JsonArray ja = new JsonArray();
		if (created_characters != null)
			for (int i=0; i<created_characters.length; i++) {
				ja.add(created_characters[i].toString());
			}
		jo.add("created_characters", ja);
		jo.addProperty("deck", deck);
		jo.addProperty("description", description);
		jo.addProperty("_id", _id);
		jo.addProperty("name", name);
		ja = new JsonArray();
		if (volume_credits != null)
			for (int i=0; i<volume_credits.length; i++) {
				ja.add(volume_credits[i].toString());
			}
		jo.add("volume_credits", ja);
		return new GsonBuilder().serializeNulls().create().toJson(jo);
	}
	
	public boolean equals(Object o) {
		return this._id == ((Person)o).getId();
	}
}
