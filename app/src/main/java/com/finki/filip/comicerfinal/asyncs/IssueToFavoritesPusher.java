package com.finki.filip.comicerfinal.asyncs;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.gson.comicvine.Issue;

/**
 * Created by Filip on 22.1.2017.
 */

public class IssueToFavoritesPusher extends AsyncTask<Issue, Void, Boolean> {

    private Context context;

    public IssueToFavoritesPusher(Context context) {
        this.context = context;
    }

    @Override
    protected Boolean doInBackground(Issue... issues) {
        return MongoDBHelper.insertInfoFavorites(issues[0]);
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        if (aBoolean) {
            Toast.makeText(context, "The comic issue is succesfully added into Favorites",
                    Toast.LENGTH_LONG).show();
        }
        else {
            Toast.makeText(context, "Issue already exists", Toast.LENGTH_LONG).show();
        }
    }
}
