package com.finki.filip.comicerfinal.gson.comicvine;

import java.io.Serializable;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class Movie<T extends Serializable> extends Resource implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7607466577612371194L;
	private String api_detail_url;
	private String box_office_revenue;
	private String budget;
	private Character[] characters;
	private String deck;
	private String description;
	private T has_staff_review;
	private int _id;
	private Image image;
	private String name;
	private String rating;
	private String release_date;
	private String runtime;
	private String site_detail_url;
	
	public Movie() {
		super();
		characters = new Character[]{};
	}

	public Movie(String api_detail_url, String box_office_revenue, String budget, Character[] characters, String deck, String description, T has_staff_review, int _id, Image image, String name, String rating,
				 String release_date, String runtime, String site_detail_url) {
		super();
		this.api_detail_url = api_detail_url;
		this.box_office_revenue = box_office_revenue;
		this.budget = budget;
		this.characters = characters;
		this.deck = deck;
		this.description = description;
		this.has_staff_review = has_staff_review;
		this._id = _id;
		this.image = image;
		this.name = name;
		this.rating = rating;
		this.release_date = release_date;
		this.runtime = runtime;
		this.site_detail_url = site_detail_url;
	}

	public String getApi_detail_url() {
		return api_detail_url;
	}

	public void setApi_detail_url(String api_detail_url) {
		this.api_detail_url = api_detail_url;
	}


	public String getBox_office_revenue() {
		return box_office_revenue;
	}

	public void setBox_office_revenue(String box_office_revenue) {
		this.box_office_revenue = box_office_revenue;
	}

	public String getBudget() {
		return budget;
	}

	public void setBudget(String budget) {
		this.budget = budget;
	}

	public Character[] getCharacters() {
		return characters;
	}

	public void setCharacters(Character[] characters) {
		this.characters = characters;
	}


	public String getDeck() {
		return deck;
	}

	public void setDeck(String deck) {
		this.deck = deck;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public T getHas_staff_review() {
		return has_staff_review;
	}

	public void setHas_staff_review(T has_staff_review) {
		this.has_staff_review = has_staff_review;
	}

	public int getId() {
		return _id;
	}

	public void setId(int _id) {
		this._id = _id;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getRelease_date() {
		return release_date;
	}

	public void setRelease_date(String release_date) {
		this.release_date = release_date;
	}

	public String getRuntime() {
		return runtime;
	}

	public void setRuntime(String runtime) {
		this.runtime = runtime;
	}
	
	
	public String getSite_detail_url() {
		return site_detail_url;
	}

	public void setSite_detail_url(String site_detail_url) {
		this.site_detail_url = site_detail_url;
	}

	public String toString() {
		JsonObject jo = new JsonObject();
		jo.addProperty("api_detail_url", api_detail_url);
		jo.addProperty("box_office_revenue", box_office_revenue);
		jo.addProperty("budget", budget);
		JsonArray ja = new JsonArray();
		if (characters != null)
			for (int i=0; i<characters.length; i++) {
				ja.add(characters[i].toString());
			}
		jo.add("characters", ja);
		jo.addProperty("deck", deck);
		jo.addProperty("description", description);
		if (has_staff_review != null)
			jo.addProperty("has_staff_review", has_staff_review.toString());
		else
			jo.addProperty("has_staff_review", (String)null);
		jo.addProperty("_id", _id);
		if (image != null)
			jo.addProperty("image", image.toString());
		else
			jo.addProperty("image", (String)null);
		jo.addProperty("name", name);
		jo.addProperty("rating", rating);
		jo.addProperty("release_date", release_date);
		jo.addProperty("runtime", runtime);
		jo.addProperty("site_detail_url", site_detail_url);
		return new GsonBuilder().serializeNulls().create().toJson(jo);
	}

	@Override
	public boolean equals(Object obj) {
		Movie m = (Movie)obj;
		return this._id == m._id;
	}
}
