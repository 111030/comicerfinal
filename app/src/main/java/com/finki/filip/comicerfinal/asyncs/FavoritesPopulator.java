package com.finki.filip.comicerfinal.asyncs;

import android.os.AsyncTask;

import com.finki.filip.comicerfinal.adapters.FavoritesAdapter;
import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.gson.comicvine.Issue;

import java.util.List;

/**
 * Created by Filip on 22.1.2017.
 */

public class FavoritesPopulator extends AsyncTask<Void, Void, List<Issue>> {

    private FavoritesAdapter adapter;

    public FavoritesPopulator(FavoritesAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    protected List<Issue> doInBackground(Void... voids) {
        return MongoDBHelper.getFavorites();
    }

    @Override
    protected void onPostExecute(List<Issue> issues) {
        if (issues != null) {
            adapter.addAll(issues);
        }
    }
}
