package com.finki.filip.comicerfinal;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.finki.filip.comicerfinal.asyncs.DropboxAsync;
import com.github.barteksc.pdfviewer.PDFView;

/**
 * Created by Filip on 11.2.2017.
 */

public class ComicReader extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comic_reader);
        //
        PDFView pdfView = (PDFView)findViewById(R.id.comic_reader_pdfView);
        //
        Intent intent = getIntent();
        int comicId = intent.getIntExtra("comicId", -1);
        new DropboxAsync(pdfView).execute(comicId);
    }
}
