package com.finki.filip.comicerfinal.gson.comicvine;

import java.io.Serializable;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class Team extends Resource implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5054563256744995835L;
	private String aliases;
	private String api_detail_url;
	private Character[] characters;
	private String deck;
	private String description;
	private Issue first_appeared_in_issue;
	private int _id;
	private Image image;
	private Movie[] movies;
	private String name;
	private String site_detail_url;
	private Volume[] volume_credits;
	
	public Team() {
		super();
		this.characters = new Character[]{};
		this.movies = new Movie[]{};
		this.volume_credits = new Volume[]{};
	}

	public Team(String aliases, String api_detail_url, Character[] characters, String deck, String description,
			Issue first_appeared_in_issue, int _id, Image image, Movie[] movies, String name,
			String site_detail_url, Volume[] volume_credits) {
		super();
		this.aliases = aliases;
		this.api_detail_url = api_detail_url;
		this.characters = characters;
		this.deck = deck;
		this.description = description;
		this.first_appeared_in_issue = first_appeared_in_issue;
		this._id = _id;
		this.image = image;
		this.movies = movies;
		this.name = name;
		this.site_detail_url = site_detail_url;
		this.volume_credits = volume_credits;
	}

	public String getAliases() {
		return aliases;
	}

	public void setAliases(String aliases) {
		this.aliases = aliases;
	}

	public String getApi_detail_url() {
		return api_detail_url;
	}

	public void setApi_detail_url(String api_detail_url) {
		this.api_detail_url = api_detail_url;
	}

	public Character[] getCharacters() {
		return characters;
	}

	public void setCharacters(Character[] characters) {
		this.characters = characters;
	}

	public String getDeck() {
		return deck;
	}

	public void setDeck(String deck) {
		this.deck = deck;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Issue getFirst_appeared_in_issue() {
		return first_appeared_in_issue;
	}

	public void setFirst_appeared_in_issue(Issue first_appeared_in_issue) {
		this.first_appeared_in_issue = first_appeared_in_issue;
	}

	public int getId() {
		return _id;
	}

	public void setId(int _id) {
		this._id = _id;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public Movie[] getMovies() {
		return movies;
	}

	public void setMovies(Movie[] movies) {
		this.movies = movies;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSite_detail_url() {
		return site_detail_url;
	}

	public void setSite_detail_url(String site_detail_url) {
		this.site_detail_url = site_detail_url;
	}

	public Volume[] getVolume_credits() {
		return volume_credits;
	}

	public void setVolume_credits(Volume[] volume_credits) {
		this.volume_credits = volume_credits;
	}
	
	public String toString() {
		
		JsonObject jo = new JsonObject();
		jo.addProperty("aliases", aliases);
		jo.addProperty("api_detail_url", api_detail_url);
		JsonArray ja = new JsonArray();
		if (characters != null)
			for (int i=0; i<characters.length; i++) {
				ja.add(characters[i].toString());
			}
		jo.add("characters", ja);
		jo.addProperty("deck", deck);
		jo.addProperty("description", description);
		if (first_appeared_in_issue != null) 
			jo.addProperty("first_appeared_in_issue", first_appeared_in_issue.toString());
		else
			jo.addProperty("first_appeared_in_issue", (String)null);
		jo.addProperty("_id", _id);
		if (image != null)
			jo.addProperty("image", image.toString());
		else
			jo.addProperty("image", (String)null);
		ja = new JsonArray();
		if (movies != null)
			for (int i=0; i<movies.length; i++) {
				ja.add(movies[i].toString());
			}
		jo.add("movies", ja);
		jo.addProperty("name", name);
		jo.addProperty("site_detail_url", site_detail_url);
		ja = new JsonArray();
		if (volume_credits != null)
			for (int i=0; i<volume_credits.length; i++) {
				ja.add(volume_credits[i].toString());
			}
		jo.add("volume_credits", ja);
		return new GsonBuilder().serializeNulls().create().toJson(jo);
	}

	@Override
	public boolean equals(Object obj) {
		return this._id == ((Team)obj).getId();
	}
}
