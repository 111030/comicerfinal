package com.finki.filip.comicerfinal.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.finki.filip.comicerfinal.R;
import com.finki.filip.comicerfinal.gson.comicvine.Movie;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Filip on 07.2.2017.
 */

public class MoviesAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private List<Movie> movies;

    public MoviesAdapter(Context context) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        movies = new ArrayList<>();
    }

    public void addAll(List<Movie> movies) {
        this.movies.addAll(movies);
        notifyDataSetChanged();
    }

    public void add(Movie movie) {
        movies.add(movie);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return movies.size();
    }

    @Override
    public Object getItem(int i) {
        return movies.get(i);
    }

    @Override
    public long getItemId(int i) {
        return movies.get(i).getId();
    }

    static class MovieHolder {
        ImageView img;
        TextView name;
        TextView releaseDate;
        TextView boxOffice;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        MovieHolder holder;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.movie_grid_item, null);
            holder = new MovieHolder();
            holder.img = (ImageView)view.findViewById(R.id.movie_grid_img);
            holder.name = (TextView)view.findViewById(R.id.movie_grid_name);
            holder.releaseDate = (TextView)view.findViewById(R.id.movie_grid_release_date);
            holder.boxOffice = (TextView)view.findViewById(R.id.movie_grid_box_office);
            view.setTag(holder);
        }
        else {
            holder = (MovieHolder) view.getTag();
        }
        Movie current = movies.get(i);
        Glide.with(context).load(current.getImage().getThumb_url()).fitCenter()
                .into(holder.img);
        holder.name.setText(current.getName());
        holder.releaseDate.setText(current.getRelease_date());
        holder.boxOffice.setText(current.getBox_office_revenue()+"$");
        return view;
    }
}
