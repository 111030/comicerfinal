package com.finki.filip.comicerfinal.singletons;

import android.os.Bundle;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;

/**
 * Created by Filip on 19.12.2016.
 */

public class MongoSingleton {

    private MongoClient client;
    private MongoDatabase database;

    private static MongoSingleton mongoInstance;

    private MongoSingleton(String host, int port, String db) {
        client = new MongoClient(host, port);
        database = client.getDatabase(db);
    }

    public static synchronized MongoSingleton getInstance() {
        if (mongoInstance == null) {
            mongoInstance = new MongoSingleton("111030.ddns.net", 27017, "comicer");
        }
        return mongoInstance;
    }

    public MongoClient getClient() {
        return client;
    }
    public MongoDatabase getDatabase() {
        return database;
    }
}
