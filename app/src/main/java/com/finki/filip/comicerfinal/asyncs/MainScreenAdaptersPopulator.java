package com.finki.filip.comicerfinal.asyncs;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.gson.comicvine.Issue;
import com.finki.filip.comicerfinal.holders.MainScreenIssuesAdapters;
import com.finki.filip.comicerfinal.singletons.MongoSingleton;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Filip on 11.1.2017.
 */

public class MainScreenAdaptersPopulator extends AsyncTask<Integer, List<Issue>, List<List<Issue>>> {

    private Context context;
    private MainScreenIssuesAdapters issuesAdapters;
    private ProgressDialog pd;

    public MainScreenAdaptersPopulator(Context context, MainScreenIssuesAdapters issuesAdapters) {
        this.context = context;
        this.issuesAdapters = issuesAdapters;
    }

    @Override
    protected void onPreExecute() {
        pd = new ProgressDialog(context, ProgressDialog.STYLE_SPINNER);
        pd.setMessage("Loading comics...");
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected List<List<Issue>> doInBackground(Integer... integers) {
        List<List<Issue>> result = new ArrayList<>();
        List<Issue> newest = MongoDBHelper.getNewIssues(integers[0],
                MongoSingleton.getInstance().getDatabase(),
                4);
        List<Issue> free = MongoDBHelper.getFreeIssues(integers[0],
                MongoSingleton.getInstance().getDatabase(),
                4);
        List<Issue> popular = MongoDBHelper.getMostPopularIssues(integers[0],
                MongoSingleton.getInstance().getDatabase(),
                4);
        List<Issue> bestRated = MongoDBHelper.getBestRatedIssues(integers[0],
                MongoSingleton.getInstance().getDatabase(),
                4);
        result.add(newest);
        result.add(free);
        result.add(popular);
        result.add(bestRated);
        return result;

    }

    @Override
    protected void onPostExecute(List<List<Issue>> lists) {
        pd.dismiss();
        issuesAdapters.getNewIssuesAdapter().addAll(lists.get(0));
        issuesAdapters.getFreeIssuesAdapter().addAll(lists.get(1));
        issuesAdapters.getPopularIssuesAdapter().addAll(lists.get(2));
        issuesAdapters.getBestRatedIssuesAdapter().addAll(lists.get(3));
    }
}
