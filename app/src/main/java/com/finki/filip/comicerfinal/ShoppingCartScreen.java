package com.finki.filip.comicerfinal;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.finki.filip.comicerfinal.adapters.ShoppingCartAdapter;
import com.finki.filip.comicerfinal.asyncs.GetCurrentUser;
import com.finki.filip.comicerfinal.asyncs.NavDrawerUser;
import com.finki.filip.comicerfinal.asyncs.ShoppingCartPopulator;
import com.finki.filip.comicerfinal.fragments.NavDrawerFragment;
import com.finki.filip.comicerfinal.fragments.TempEmptyDrawer;
import com.finki.filip.comicerfinal.fragments.dialogs.BuyIssuePromptDialog;
import com.finki.filip.comicerfinal.singletons.UserSingleton;

import java.util.concurrent.ExecutionException;


/**
 * Created by Filip on 05.11.2016.
 */

public class ShoppingCartScreen extends AppCompatActivity {

    private TextView spError;
    private LinearLayout spScreen;
    private ListView items;
    private TextView total;
    //
    private ShoppingCartAdapter shoppingCartAdapter;
    //
    private ImageButton toggleDrawerButton;
    private TextView shoppingCartCounter;
    private ImageButton readingList;
    private TextView readingListCounter;
    //
    private Button checkoutAll;

    boolean drawerShow = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_cart);
        //
        toggleDrawerButton = (ImageButton)findViewById(R.id.appbar_drawer_button);
        toggleDrawerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleNavigationDrawer();
            }
        });
        //
        shoppingCartCounter = (TextView)findViewById(R.id.appbar_shopping_cart_counter);
        readingListCounter = (TextView)findViewById(R.id.appbar_reading_list_counter);
        //
        readingList = (ImageButton)findViewById(R.id.appbar_reading_list);
        readingList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ShoppingCartScreen.this, ReadingListScreen.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                i.putExtra("User", UserSingleton.getInstance().getUser() != null ?
                        UserSingleton.getInstance().getUser().getUsername() : "anonymous");
                startActivity(i);
            }
        });
        spError = (TextView)findViewById(R.id.shopping_cart_error);
        spScreen = (LinearLayout)findViewById(R.id.shopping_cart_info);
        //
        items = (ListView)findViewById(R.id.shopping_cart_items);
        total = (TextView)findViewById(R.id.shopping_cart_total);
        //
        String username = getIntent().getStringExtra("User");
        if (username.equalsIgnoreCase("anonymous")) {
            spError.setVisibility(View.VISIBLE);
            spScreen.setVisibility(View.GONE);
        }
        else {
            spError.setVisibility(View.GONE);
            shoppingCartAdapter = new ShoppingCartAdapter(this);
            new ShoppingCartPopulator(shoppingCartAdapter, total).execute(
                    UserSingleton.getInstance().getUser());
            items.setAdapter(shoppingCartAdapter);
            spScreen.setVisibility(View.VISIBLE);
        }
        checkoutAll = (Button)findViewById(R.id.shopping_cart_proceed);
        checkoutAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BuyIssuePromptDialog bipd = null;
                try {
                    bipd = BuyIssuePromptDialog.newInstance(
                            new GetCurrentUser(ShoppingCartScreen.this).execute().get().getAmount_credit_card(),
                            -1,
                            shoppingCartAdapter.getIssues()
                    );
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
                bipd.show(getFragmentManager(), "BUY_ISSUE_PROMPT");
            }
        });
        getFragmentManager().beginTransaction()
                .add(R.id.nav_drawer_fragment_shopping_cart, TempEmptyDrawer.newInstance())
                .commit();


    }

    @Override
    protected void onResume() {
        super.onResume();
        setCurrentSignedUser();
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
            drawerShow = false;
        }

    }

    private void setCurrentSignedUser() {
            WifiManager wm = (WifiManager)getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            String macAddress = wm.getConnectionInfo().getMacAddress();
            NavDrawerUser getCurrentUser = new NavDrawerUser(shoppingCartCounter, readingListCounter);
            getCurrentUser.execute(macAddress);
    }

    private void toggleNavigationDrawer() {
        if (drawerShow) {
            getFragmentManager().popBackStack();
            drawerShow = false;
            return;
        }
        drawerShow = true;
        getFragmentManager().beginTransaction()
                .setCustomAnimations(R.animator.drawer_to_right, R.animator.drawer_to_left,
                        R.animator.drawer_to_right, R.animator.drawer_to_left)
                .replace(R.id.nav_drawer_fragment_shopping_cart, NavDrawerFragment.newInstance())
                .addToBackStack(null)
                .commit();
    }

    public ShoppingCartAdapter getAdapter() {
        return shoppingCartAdapter;
    }
}
