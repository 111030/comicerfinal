package com.finki.filip.comicerfinal.gson.comicvine;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.Serializable;

public class Publisher extends Resource implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2679941862078183787L;
	private String aliases;
	private String api_detail_url;
	private String deck;
	private String description;
	private int _id;
	private Image image;
	private String location_city;
	private String location_state;
	private String name;
	private String site_detail_url;
	private Volume[] volumes;
	
	public Publisher() {
		super();
		volumes = new Volume[]{};
	}

	public Publisher(String aliases, String api_detail_url, String deck, String description, 
			int _id, Image image, String location_city, String location_state,
			String name, String site_detail_url, Volume[] volumes) {
		super();
		this.aliases = aliases;
		this.api_detail_url = api_detail_url;
		this.deck = deck;
		this.description = description;
		this._id = _id;
		this.location_city = location_city;
		this.location_state = location_state;
		this.name = name;
		this.site_detail_url = site_detail_url;
		this.volumes = volumes;
	}

	public String getAliases() {
		return aliases;
	}

	public void setAliases(String aliases) {
		this.aliases = aliases;
	}

	public String getApi_detail_url() {
		return api_detail_url;
	}

	public void setApi_detail_url(String api_detail_url) {
		this.api_detail_url = api_detail_url;
	}
	
	public String getDeck() {
		return deck;
	}

	public void setDeck(String deck) {
		this.deck = deck;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getId() {
		return _id;
	}

	public void setId(int _id) {
		this._id = _id;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public String getLocation_city() {
		return location_city;
	}

	public void setLocation_city(String location_city) {
		this.location_city = location_city;
	}

	public String getLocation_state() {
		return location_state;
	}

	public void setLocation_state(String location_state) {
		this.location_state = location_state;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSite_detail_url() {
		return site_detail_url;
	}

	public void setSite_detail_url(String site_detail_url) {
		this.site_detail_url = site_detail_url;
	}

	public Volume[] getVolumes() {
		return volumes;
	}

	public void setVolumes(Volume[] volumes) {
		this.volumes = volumes;
	}

	public String toString() {
		JsonObject jo = new JsonObject();
		jo.addProperty("aliases", aliases);
		jo.addProperty("api_detail_url", api_detail_url);
		jo.addProperty("deck", deck);
		jo.addProperty("description", description);
		jo.addProperty("_id", _id);
		if (image != null) 
			jo.addProperty("image", image.toString());
		else
			jo.addProperty("image", (String)null);
		jo.addProperty("location_city", location_city);
		jo.addProperty("location_state", location_state);
		jo.addProperty("name", name);
		jo.addProperty("site_detail_url", site_detail_url);
		JsonArray ja = new JsonArray();
		if (volumes != null)
			for (int i=0; i<volumes.length; i++) {
				ja.add(volumes[i].toString());
			}
		jo.add("volumes", ja);
		return new GsonBuilder().serializeNulls().create().toJson(jo);	
	}
	
	public boolean equals(Object o) {
		return this._id == ((Publisher)o).getId();
	}
	
}
