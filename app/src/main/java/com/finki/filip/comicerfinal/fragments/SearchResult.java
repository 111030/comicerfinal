package com.finki.filip.comicerfinal.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import com.finki.filip.comicerfinal.R;
import com.finki.filip.comicerfinal.adapters.CharactersAdapter;
import com.finki.filip.comicerfinal.adapters.MainScreenIssuesAdapter;
import com.finki.filip.comicerfinal.adapters.MoviesAdapter;
import com.finki.filip.comicerfinal.adapters.VolumesAdapter;
import com.finki.filip.comicerfinal.gson.comicvine.Resource;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Filip on 10.2.2017.
 */

public class SearchResult extends Fragment {

    private String collection;
    private List results;

    public static SearchResult newInstance(String collection, List<? extends Resource> results) {
        SearchResult searchResult = new SearchResult();
        Bundle b = new Bundle();
        b.putString("collection", collection);
        b.putSerializable("results", results.toArray());
        searchResult.setArguments(b);
        return searchResult;
    }

    public static SearchResult newInstance(Bundle bundle) {
        String collection = bundle.getString("collection");
        Object[] oResults = (Object[])bundle.getSerializable("results");
        List results = new ArrayList<>();
        for (int i=0; i<oResults.length; i++) {
            results.add(oResults[i]);
        }
        return newInstance(collection, results);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle temp = getArguments();
        collection = temp.getString("collection");
        Object[] oResults = (Object[])temp.getSerializable("results");
        results = new ArrayList<>();
        for (int i=0; i<oResults.length; i++) {
            results.add(oResults[i]);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.search_result, container, false);
        TextView title = (TextView)v.findViewById(R.id.search_result_title);
        GridView items = (GridView)v.findViewById(R.id.search_result_items);
        if (collection.equalsIgnoreCase("issues")) {
            title.setText("Issues");
            MainScreenIssuesAdapter issuesAdapter = new MainScreenIssuesAdapter(getActivity(), 8);
            issuesAdapter.addAll(results);
            items.setAdapter(issuesAdapter);
        }
        else if (collection.equalsIgnoreCase("volumes")) {
            title.setText("Volumes");
            VolumesAdapter volumesAdapter = new VolumesAdapter(getActivity());
            volumesAdapter.addAll(results);
            items.setAdapter(volumesAdapter);
        }
        else if (collection.equalsIgnoreCase("characters")) {
            title.setText("Characters");
            CharactersAdapter charactersAdapter = new CharactersAdapter(getActivity());
            charactersAdapter.addAll(results);
            items.setAdapter(charactersAdapter);
        }
        else if (collection.equalsIgnoreCase("movies")) {
            title.setText("Movies");
            MoviesAdapter moviesAdapter = new MoviesAdapter(getActivity());
            moviesAdapter.addAll(results);
            items.setAdapter(moviesAdapter);
        }
        else if (collection.equalsIgnoreCase("publishers")) {
            title.setText("Publishers");
            //
        }
        else {
            //
        }
        return v;
    }
}
