package com.finki.filip.comicerfinal.gson.comicvine;

import java.io.Serializable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class CharacterResponse extends Resource implements Serializable {
	
	/**
	 * 
	 */
	private String aliases;
	private String api_detail_url;
	private String birth;
	private int count_of_issue_appearances;
	private CharacterResponse[] character_enemies;
	private CharacterResponse[] character_friends;
	private PersonResponse[] creators;
	private String deck;
	private String description;
	private IssueResponse first_appeared_in_issue;
	private int id;
	private Image image;
	private IssueResponse[] issues_died_in;
	private MovieResponse[] movies;
	private String name;
	private PowerResponse[] powers;
	private PublisherResponse publisher;
	private String real_name;
	private String site_detail_url;
	private TeamResponse[] teams;
	private VolumeResponse[] volume_credits;
	
	public CharacterResponse() {
		super();
		character_enemies = new CharacterResponse[]{};
		character_friends = new CharacterResponse[]{};
		creators = new PersonResponse[]{};
		powers = new PowerResponse[]{};
		issues_died_in = new IssueResponse[]{};
		movies = new MovieResponse[]{};
		teams = new TeamResponse[]{};
		volume_credits = new VolumeResponse[]{};
	}

	public CharacterResponse(String aliases, String api_detail_url, String birth, int count_of_issue_appearances, CharacterResponse[] character_enemies,
			CharacterResponse[] character_friends, PersonResponse[] creators, String deck, String description,
			IssueResponse first_appeared_in_issue, int id, Image image, IssueResponse[] issues_died_in, MovieResponse[] movies, String name,
			PowerResponse[] powers, PublisherResponse publisher, String real_name, String site_detail_url, TeamResponse[] teams, VolumeResponse[] volume_credits) {
		super();
		this.aliases = aliases;
		this.api_detail_url = api_detail_url;
		this.birth = birth;
		this.count_of_issue_appearances = count_of_issue_appearances;
		this.character_enemies = character_enemies;
		this.character_friends = character_friends;
		this.creators = creators;
		this.deck = deck;
		this.description = description;
		this.first_appeared_in_issue = first_appeared_in_issue;
		this.id = id;
		this.image = image;
		this.issues_died_in = issues_died_in;
		this.movies = movies;
		this.name = name;
		this.powers = powers;
		this.publisher = publisher;
		this.real_name = real_name;
		this.site_detail_url = site_detail_url;
		this.teams = teams;
		this.volume_credits = volume_credits;
	}

	public String getAliases() {
		return aliases;
	}

	public void setAliases(String aliases) {
		this.aliases = aliases;
	}

	public String getApi_detail_url() {
		return api_detail_url;
	}

	public void setApi_detail_url(String api_detail_url) {
		this.api_detail_url = api_detail_url;
	}

	public String getBirth() {
		return birth;
	}

	public void setBirth(String birth) {
		this.birth = birth;
	}

	public int getCount_of_issue_appearances() {
		return count_of_issue_appearances;
	}

	public void setCount_of_issue_appearances(int count_of_issue_appearances) {
		this.count_of_issue_appearances = count_of_issue_appearances;
	}

	public CharacterResponse[] getCharacter_enemies() {
		return character_enemies;
	}

	public void setCharacter_enemies(CharacterResponse[] character_enemies) {
		this.character_enemies = character_enemies;
	}

	public CharacterResponse[] getCharacter_friends() {
		return character_friends;
	}

	public void setCharacter_friends(CharacterResponse[] character_friends) {
		this.character_friends = character_friends;
	}

	public PersonResponse[] getCreators() {
		return creators;
	}

	public void setCreators(PersonResponse[] creators) {
		this.creators = creators;
	}
	
	

	public String getDeck() {
		return deck;
	}

	public void setDeck(String deck) {
		this.deck = deck;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public IssueResponse getFirst_appeared_in_issue() {
		return first_appeared_in_issue;
	}

	public void setFirst_appeared_in_issue(IssueResponse first_appeared_in_issue) {
		this.first_appeared_in_issue = first_appeared_in_issue;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}
	
	
	
	public IssueResponse[] getIssues_died_in() {
		return issues_died_in;
	}

	public void setIssues_died_in(IssueResponse[] issues_died_in) {
		this.issues_died_in = issues_died_in;
	}

	public MovieResponse[] getMovies() {
		return movies;
	}

	public void setMovies(MovieResponse[] movies) {
		this.movies = movies;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public PowerResponse[] getPowers() {
		return powers;
	}

	public void setPowers(PowerResponse[] powers) {
		this.powers = powers;
	}

	public PublisherResponse getPublisher() {
		return publisher;
	}

	public void setPublisher(PublisherResponse publisher) {
		this.publisher = publisher;
	}

	public String getReal_name() {
		return real_name;
	}

	public void setReal_name(String real_name) {
		this.real_name = real_name;
	}
	
	
	
	public String getSite_detail_url() {
		return site_detail_url;
	}

	public void setSite_detail_url(String site_detail_url) {
		this.site_detail_url = site_detail_url;
	}

	public TeamResponse[] getTeams() {
		return teams;
	}

	public void setTeams(TeamResponse[] teams) {
		this.teams = teams;
	}

	public VolumeResponse[] getVolume_credits() {
		return volume_credits;
	}

	public void setVolume_credits(VolumeResponse[] volume_credits) {
		this.volume_credits = volume_credits;
	}

	public String toString() {
		Gson gson = new GsonBuilder().serializeNulls().create();
		JsonObject jo = new JsonObject();
		jo.addProperty("aliases", aliases);
		jo.addProperty("api_detail_url", api_detail_url);
		jo.addProperty("birth", birth);
		jo.addProperty("count_of_issue_appearances", count_of_issue_appearances);
		JsonArray ja = new JsonArray();
		if (character_enemies != null)
			for (int i=0; i<character_enemies.length; i++) {
				ja.add(character_enemies[i].toString());
			}
		jo.add("character_enemies", ja);
		ja = new JsonArray();
		if (character_friends != null)
			for (int i=0; i<character_friends.length; i++) {
				ja.add(character_friends[i].toString());
			}
		jo.add("character_friends", ja);
		ja = new JsonArray();
		if (creators != null)
			for (int i=0; i<creators.length; i++) {
				ja.add(creators[i].toString());
			}
		jo.add("creators", ja);
		jo.addProperty("deck", deck);
		jo.addProperty("description", description);
		if (first_appeared_in_issue != null)
			jo.addProperty("first_appeared_in_issue", first_appeared_in_issue.toString());
		else
			jo.addProperty("first_appeared_in_issue", (String)null);
		jo.addProperty("id", id);
		if (image != null)
			jo.addProperty("image", image.toString());
		else
			jo.addProperty("image", (String)null);
		ja = new JsonArray();
		if (issues_died_in != null)
			for (int i=0; i<issues_died_in.length; i++) {
				ja.add(issues_died_in[i].toString());
			}
		jo.add("issues_died_in", ja);
		ja = new JsonArray();
		if (movies != null)
			for (int i=0; i<movies.length; i++) {
				ja.add(movies[i].toString());
			}
		jo.add("movies", ja);
		jo.addProperty("name", name);
		ja = new JsonArray();
		if (powers != null) {
			for (int i=0; i<powers.length; i++) {
				ja.add(gson.toJson(powers[i], Power.class));
			}
		}
		jo.add("powers", ja);
		if (publisher != null)
			jo.addProperty("publisher", publisher.toString());
		else
			jo.addProperty("publisher", (String)null);
		jo.addProperty("real_name", real_name);
		jo.addProperty("site_detail_url", site_detail_url);
		ja = new JsonArray();
		if (teams != null)
			for (int i=0; i<teams.length; i++) {
				ja.add(teams[i].toString());
			}
		jo.add("teams", ja);
		ja = new JsonArray();
		if (volume_credits != null)
			for (int i=0; i<volume_credits.length; i++) {
				ja.add(volume_credits[i].toString());
			}
		jo.add("volume_credits", ja);
		return new GsonBuilder().serializeNulls().create().toJson(jo);
	}
	
	public boolean equals(Object o) {
		return this.id == ((CharacterResponse)o).getId();
	}

	public Character getCharacter() {
		Character c = new Character();
		c.setAliases(aliases);
		c.setApi_detail_url(api_detail_url);
		c.setBirth(birth);
		Character[] cFriends = null;
		if (character_friends != null) {
			cFriends = new Character[character_friends.length];
			for (int i=0; i<character_friends.length; i++) {
				cFriends[i] = character_friends[i].getCharacter();
			}
		}
		c.setCharacter_friends(cFriends);
		Character[] cEnemies = null;
		if (character_enemies != null) {
			cEnemies = new Character[character_enemies.length];
			for (int i=0; i<character_enemies.length; i++) {
				cEnemies[i] = character_enemies[i].getCharacter();
			}
		}
		c.setCharacter_enemies(cEnemies);
		c.setCount_of_issue_appearances(count_of_issue_appearances);
		Person[] people = null;
		if (creators != null) {
			people = new Person[creators.length];
			for (int i=0; i<creators.length; i++) {
				people[i] = creators[i].getPerson();
			}
		}
		c.setCreators(people);
		c.setDeck(deck);
		c.setDescription(description);
		Issue cIssue = null;
		if (first_appeared_in_issue != null) {
			cIssue = first_appeared_in_issue.getIssue();
		}
		c.setFirst_appeared_in_issue(cIssue);
		c.setId(id);
		c.setImage(image);
		Issue[] diedIn = null;
		if (issues_died_in != null) {
			diedIn = new Issue[issues_died_in.length];
			for (int i=0; i<issues_died_in.length; i++) {
				diedIn[i] = issues_died_in[i].getIssue();
			}
		}
		c.setIssues_died_in(diedIn);
		Movie[] cMovies = null;
		if (movies != null) {
			cMovies = new Movie[movies.length];
			for (int i=0; i<movies.length; i++) {
				cMovies[i] = movies[i].getMovie();
			}
		}
		c.setMovies(cMovies);
		c.setName(name);
		Power[] cPowers = null;
		if (powers != null) {
			cPowers = new Power[powers.length];
			for (int i=0; i<powers.length; i++) {
				cPowers[i] = powers[i].getPower();
			}
		}
		c.setPowers(cPowers);
		Publisher cPublisher = null;
		if (publisher != null) {
			cPublisher = publisher.getPublisher();
		}
		c.setPublisher(cPublisher);
		c.setReal_name(real_name);
		c.setSite_detail_url(site_detail_url);
		Team[] cTeams = null;
		if (teams != null) {
			cTeams = new Team[teams.length];
			for (int i=0; i<teams.length; i++) {
				cTeams[i] = teams[i].getTeam();
			}
		}
		c.setTeams(cTeams);
		Volume[] volumes = null;
		if (volume_credits != null) {
			volumes = new Volume[volume_credits.length];
			for (int i=0; i<volume_credits.length; i++) {
				volumes[i] = volume_credits[i].getVolume();
			}
		}
		c.setVolume_credits(volumes);
		return c;
	}

}
