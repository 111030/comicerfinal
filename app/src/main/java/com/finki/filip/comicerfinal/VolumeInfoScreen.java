package com.finki.filip.comicerfinal;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.finki.filip.comicerfinal.asyncs.GetTopIssuesForVolume;
import com.finki.filip.comicerfinal.asyncs.NavDrawerUser;
import com.finki.filip.comicerfinal.asyncs.VolumeGetter;
import com.finki.filip.comicerfinal.fragments.NavDrawerFragment;
import com.finki.filip.comicerfinal.fragments.TempEmptyDrawer;
import com.finki.filip.comicerfinal.gson.comicvine.Character;
import com.finki.filip.comicerfinal.gson.comicvine.Person;
import com.finki.filip.comicerfinal.gson.comicvine.Volume;
import com.finki.filip.comicerfinal.singletons.UserSingleton;

/**
 * Created by Filip on 06.2.2017.
 */

public class VolumeInfoScreen extends Activity {
    private ImageView volumeImg;
    private TextView volumeIssuesCount;
    private TextView volumeStartYear;
    private TextView volumeCreators;
    private TextView volumeName;
    private TextView volumeDescription;
    private LinearLayout volumeTopIssues;
    private LinearLayout volumeCharacters;
    //
    private ImageButton toggleDrawerButton;
    private TextView shoppingCartCounter;
    private TextView readingListCounter;
    private ImageButton shoppingCart;
    private ImageButton readingList;

    boolean drawerShow = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.volume_info);
        //
        String apiDetailUrl = getIntent().getStringExtra("api_detail_url");
        int id = getIntent().getIntExtra("_id", -1);
        //
        VolumeGetter volumeGetter = new VolumeGetter(this, id, apiDetailUrl);
        volumeGetter.execute();
        //
        toggleDrawerButton = (ImageButton)findViewById(R.id.appbar_drawer_button);
        toggleDrawerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleNavigationDrawer();
            }
        });
        //
        shoppingCartCounter = (TextView)findViewById(R.id.appbar_shopping_cart_counter);
        readingListCounter = (TextView)findViewById(R.id.appbar_reading_list_counter);
        //
        shoppingCart = (ImageButton)findViewById(R.id.appbar_shopping_cart);
        shoppingCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(VolumeInfoScreen.this, ShoppingCartScreen.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                i.putExtra("User", UserSingleton.getInstance().getUser() != null ?
                        UserSingleton.getInstance().getUser().getUsername() : "anonymous");
                startActivity(i);
            }
        });
        readingList = (ImageButton)findViewById(R.id.appbar_reading_list);
        readingList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(VolumeInfoScreen.this, ReadingListScreen.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                i.putExtra("User", UserSingleton.getInstance().getUser() != null ?
                        UserSingleton.getInstance().getUser().getUsername() : "anonymous");
                startActivity(i);
            }
        });
        getFragmentManager().beginTransaction()
                .add(R.id.nav_drawer_fragment, TempEmptyDrawer.newInstance())
                .commit();

    }

    @Override
    protected void onResume() {
        super.onResume();
        setCurrentSignedUser();
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
            drawerShow = false;
        }

    }

    private void setCurrentSignedUser() {
            WifiManager wm = (WifiManager)getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            String macAddress = wm.getConnectionInfo().getMacAddress();
            NavDrawerUser getCurrentUser = new NavDrawerUser(shoppingCartCounter, readingListCounter);
            getCurrentUser.execute(macAddress);
    }

    private void toggleNavigationDrawer() {
        if (drawerShow) {
            getFragmentManager().popBackStack();
            drawerShow = false;
            return;
        }
        drawerShow = true;
        getFragmentManager().beginTransaction()
                .setCustomAnimations(R.animator.drawer_to_right, R.animator.drawer_to_left,
                        R.animator.drawer_to_right, R.animator.drawer_to_left)
                .replace(R.id.nav_drawer_fragment, NavDrawerFragment.newInstance())
                .addToBackStack(null)
                .commit();
    }

    public void updateTheUI(Volume volume) {
        volumeImg = (ImageView)findViewById(R.id.volume_screen_image);
        volumeIssuesCount = (TextView)findViewById(R.id.volume_screen_issues_count);
        volumeStartYear = (TextView)findViewById(R.id.volume_screen_start_year);
        volumeCreators = (TextView)findViewById(R.id.volume_screen_creators);
        volumeName = (TextView)findViewById(R.id.volume_screen_title);
        volumeDescription = (TextView)findViewById(R.id.volume_screen_description);
        volumeTopIssues = (LinearLayout) findViewById(R.id.volume_screen_issues);
        volumeCharacters = (LinearLayout)findViewById(R.id.volume_screen_characters);
        //
        if (volume.getImage() != null && volume.getImage().getMedium_url() != null)
            Glide.with(this).load(volume.getImage().getMedium_url()).fitCenter().into(volumeImg);
        //
        volumeIssuesCount.setText(volume.getCount_of_issues());
        //
        volumeStartYear.setText(volume.getStart_year());
        //
        Person[] creators = volume.getPeople();
        StringBuilder sb = new StringBuilder();
        if (creators != null) {
            for (int i=0; i<creators.length; i++) {
                sb.append(creators[i].getName());
                if (i < creators.length - 1)
                    sb.append(",");
            }
        }
        volumeCreators.setText(sb.toString());
        //
        volumeName.setText(volume.getName() + " - " + volume.getPublisher() != null
        ? volume.getPublisher().getName() : "");
        //
        volumeDescription.setText(Html.fromHtml(volume.getDescription()));
        //
        new GetTopIssuesForVolume(this, volumeTopIssues).execute(volume.getId());
        //
        Character[] characters = volume.getCharacters();
        if (characters != null) {
            for (int i=0; i<characters.length; i++) {
                if (characters[i].getImage() == null || characters[i].getImage().getThumb_url() == null)
                    continue;
                View friend = getLayoutInflater().inflate(R.layout.holder_horizontal, null);
                ImageView imgHolder = (ImageView)friend.findViewById(R.id.horizontal_holder_image);
                TextView textHolder = (TextView)friend.findViewById(R.id.horizontal_holder_text);
                Glide.with(this).load(characters[i].getImage().getThumb_url())
                        .into(imgHolder);
                textHolder.setText(characters[i].getName());
                volumeCharacters.addView(friend);
                /*friend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(CharacterInfoScreen.this, CharacterInfoScreen.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        intent.putExtra("_id", friends[index].getId());
                        intent.putExtra("api_detail_url", friends[index].getApi_detail_url());
                        startActivity(intent);
                    }
                });*/
            }
        }
    }
}
