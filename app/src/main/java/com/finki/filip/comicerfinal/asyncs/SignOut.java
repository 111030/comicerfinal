package com.finki.filip.comicerfinal.asyncs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.widget.Toast;

import com.finki.filip.comicerfinal.SignOutTemp;
import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.gson.other.User;
import com.finki.filip.comicerfinal.singletons.UserSingleton;

/**
 * Created by Filip on 18.2.2017.
 */

public class SignOut extends AsyncTask<Void, Void, Boolean> {

    private Activity activity;
    private User user;

    public SignOut(Activity activity, User user) {
        this.activity = activity;
        this.user = user;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        WifiManager wm = (WifiManager)activity.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        String macAddress = wm.getConnectionInfo().getMacAddress();
        return MongoDBHelper.signOutUser(user, macAddress);
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        if (aBoolean) {
            UserSingleton.setUser(null);
            Intent i = new Intent(activity, SignOutTemp.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            activity.startActivity(i);
        }
        else {
            Toast.makeText(activity, "Error signing out", Toast.LENGTH_SHORT).show();
        }
    }
}
