package com.finki.filip.comicerfinal.asyncs;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.finki.filip.comicerfinal.SignInScreen;
import com.finki.filip.comicerfinal.factories.MongoDBHelper;



/**
 * Created by Filip on 25.12.2016.
 */

public class SignIn extends AsyncTask<String, Integer, Integer> {

    private Context context;
    //
    private static final int ALL_VALID = 0;
    private static final int NO_USERNAME = 1;
    private static final int NO_PASSWORD = 2;

    public SignIn(Context context) {
        this.context = context;
    }

    @Override
    protected Integer doInBackground(String... strings) {
        String username = strings[0];
        String password = strings[1];
        //
        if (!MongoDBHelper.checkAlreadyRegisteredUsername(username) &&
                !MongoDBHelper.checkAlreadyRegisteredEmail(username)) {
            return NO_USERNAME;
        }
        //
        WifiManager wm = (WifiManager)context.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        String macAddr = wm.getConnectionInfo().getMacAddress();
        int ipAddr = wm.getConnectionInfo().getIpAddress();
        if (!MongoDBHelper.signInUser(username, password, ipAddr, macAddr)) {
            return NO_PASSWORD;
        }
        return ALL_VALID;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        super.onPostExecute(integer);
        TextView usernameError = ((SignInScreen)context).usernameError;
        TextView passwordError = ((SignInScreen)context).passwordError;
        //
        if (integer.intValue() == NO_USERNAME) {
            usernameError.setVisibility(View.VISIBLE);
            passwordError.setVisibility(View.INVISIBLE);
        }
        else if (integer.intValue() == NO_PASSWORD) {
            usernameError.setVisibility(View.INVISIBLE);
            passwordError.setVisibility(View.VISIBLE);
        }
        else {
            usernameError.setVisibility(View.INVISIBLE);
            passwordError.setVisibility(View.INVISIBLE);
            Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
            ((SignInScreen) context).finish();
        }
    }
}
