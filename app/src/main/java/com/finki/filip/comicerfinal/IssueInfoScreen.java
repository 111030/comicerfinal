package com.finki.filip.comicerfinal;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.finki.filip.comicerfinal.adapters.CommentsAdapter;
import com.finki.filip.comicerfinal.asyncs.CommentsPopulator;
import com.finki.filip.comicerfinal.asyncs.InfoScreenPopulateUserGrade;
import com.finki.filip.comicerfinal.asyncs.IssueInfoScreenPopulator;
import com.finki.filip.comicerfinal.asyncs.IssueToCartPusher;
import com.finki.filip.comicerfinal.asyncs.IssueToFavoritesPusher;
import com.finki.filip.comicerfinal.asyncs.NavDrawerUser;
import com.finki.filip.comicerfinal.asyncs.SimliarIssuesPopulator;
import com.finki.filip.comicerfinal.eventlisteners.PostCommentListener;
import com.finki.filip.comicerfinal.eventlisteners.RateIssueListener;
import com.finki.filip.comicerfinal.fragments.NavDrawerFragment;
import com.finki.filip.comicerfinal.fragments.TempEmptyDrawer;
import com.finki.filip.comicerfinal.gson.comicvine.Issue;
import com.finki.filip.comicerfinal.gson.marvel.ComicPrice;
import com.finki.filip.comicerfinal.gson.other.User;
import com.finki.filip.comicerfinal.singletons.UserSingleton;
import com.google.gson.GsonBuilder;

import org.xmlpull.v1.XmlPullParser;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.MeasureSpec.UNSPECIFIED;

/**
 * Created by Filip on 29.10.2016.
 */

public class IssueInfoScreen extends AppCompatActivity {

    private ImageView issueImage;
    private TextView volumeName;
    private TextView issueNumber;
    private TextView coverDate;
    private TextView createdBy;
    private ImageView ratingStars[];
    private TextView title;
    private TextView price;
    private TextView description;
    private LinearLayout characters;
    private LinearLayout similar;
    private Button buyButton;
    private Button favoritesButton;
    private TextView shoppingCartCounter;
    private TextView readingListCounter;
    private ImageButton toggleDrawerButton;
    private ImageButton shoppingCart;
    private ImageButton readingList;
    //
    private TextView gradesCount;
    //
    private TextView userInitials;
    private EditText comment;
    private ImageButton postComment;
    //
    private ListView comments;
    private CommentsAdapter commentsAdapter;
    //
    private Issue issue;

    boolean drawerShow = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.issue_info);
        //
        issue = new GsonBuilder().serializeNulls().create()
                .fromJson(getIntent().getStringExtra("Issue"), Issue.class);
        //
        final String issueUrl = issue.getApi_detail_url() != null
                ? issue.getApi_detail_url() + "?api_key=d7ff6b517060c4395d14ade98b76479f7bea88e0" +
                "&format=json"
                : null;
        //
        issueImage = (ImageView)findViewById(R.id.info_screen_image);
        Glide.with(this).load(issue.getImage().getMedium_url()).fitCenter().into(issueImage);
        volumeName = (TextView)findViewById(R.id.info_screen_volume_title);
        volumeName.setText(issue.getVolume().getName());
        issueNumber = (TextView)findViewById(R.id.info_screen_issue_number);
        issueNumber.setText("#"+issue.getIssue_number());
        coverDate = (TextView)findViewById(R.id.info_screen_cover_date);
        coverDate.setText(issue.getCover_date());
        createdBy = (TextView)findViewById(R.id.info_screen_issue_creators);
        if (issue.getPerson_credits() == null || issue.getPerson_credits().length == 0) {
            new IssueInfoScreenPopulator().
                    execute(issueUrl, "person_credits", String.valueOf(issue.getId()));
        }
        else {
            StringBuilder sb = new StringBuilder();
            for (int i=0; i<issue.getPerson_credits().length; i++) {
                sb.append(issue.getPerson_credits()[i].getName());
                if (i < issue.getPerson_credits().length - 1)
                    sb.append(", ");
            }
            createdBy.setText(sb.toString());
        }
        ratingStars = new ImageView[5];
        int userReview = issue.getUser_review() != 0 ?
                Float.valueOf(issue.getUser_review()).intValue() : 0;
        ratingStars[0] = (ImageView)findViewById(R.id.info_screen_ratingstar_1);
        ratingStars[0].setImageResource(userReview >= 1 ?
                R.drawable.ic_star_white_36dp : R.drawable.ic_star_border_white_36dp);
        ratingStars[1] = (ImageView)findViewById(R.id.info_screen_ratingstar_2);
        ratingStars[1].setImageResource(userReview >= 2 ?
                R.drawable.ic_star_white_36dp : R.drawable.ic_star_border_white_36dp);
        ratingStars[2] = (ImageView)findViewById(R.id.info_screen_ratingstar_3);
        ratingStars[2].setImageResource(userReview >= 3 ?
                R.drawable.ic_star_white_36dp : R.drawable.ic_star_border_white_36dp);
        ratingStars[3] = (ImageView)findViewById(R.id.info_screen_ratingstar_4);
        ratingStars[3].setImageResource(userReview >= 4 ?
                R.drawable.ic_star_white_36dp : R.drawable.ic_star_border_white_36dp);
        ratingStars[4] = (ImageView)findViewById(R.id.info_screen_ratingstar_5);
        ratingStars[4].setImageResource(userReview >= 5 ?
                R.drawable.ic_star_white_36dp : R.drawable.ic_star_border_white_36dp);
        title = (TextView)findViewById(R.id.info_screen_title);
        title.setText(issue.getName());
        price = (TextView)findViewById(R.id.info_screen_issue_price);
        final List<ComicPrice> prices = issue.getPrices();
        if (prices == null || prices.size() == 0) {
            price.setText("N/A");
        }
        else {
            for (int i=0; i<prices.size(); i++) {
                if (prices.get(i).getType().equalsIgnoreCase("digitalPurchasePrice")) {
                    price.setText(String.valueOf(prices.get(i).getPrice()));
                    break;
                }
                if (i == prices.size() - 1) {
                    price.setText(String.valueOf(prices.get(i).getPrice()));
                }
            }
        }
        description = (TextView)findViewById(R.id.info_screen_description);
        description.setText(Html.fromHtml(issue.getDescription() != null
        ? issue.getDescription() : ""));
        characters = (LinearLayout)findViewById(R.id.info_screen_characters);
        characters.setDividerPadding(16);
        if (issue.getCharacter_credits() == null || issue.getCharacter_credits().length == 0) {
            new IssueInfoScreenPopulator().
                    execute(issueUrl, "character_credits", String.valueOf(issue.getId()));
        }
        else {
            ImageView charImgs[] = new ImageView[issue.getCharacter_credits().length];
            for (int i=0; i<issue.getCharacter_credits().length; i++) {
                if (issue.getCharacter_credits()[i].getImage() == null ||
                        issue.getCharacter_credits()[i].getImage().getThumb_url() == null)
                    continue;
                charImgs[i] = new ImageView(this);
                charImgs[i].setId(issue.getCharacter_credits()[i].getId());
                charImgs[i].setLayoutParams(new ViewGroup.LayoutParams(196, 256));
                charImgs[i].setScaleType(ImageView.ScaleType.FIT_XY);
                charImgs[i].setPadding(16, 16, 16, 16);
                Glide.with(this).load(issue.getCharacter_credits()[i].
                        getImage().getThumb_url()).into(charImgs[i]);
                final int index = i;
                characters.addView(charImgs[i]);
                charImgs[i].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(IssueInfoScreen.this, CharacterInfoScreen.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        intent.putExtra("_id", issue.getCharacter_credits()[index].getId());
                        intent.putExtra("api_detail_url", issue.getCharacter_credits()[index].getApi_detail_url());
                        startActivity(intent);
                    }
                });
            }

        }
        //
        similar = (LinearLayout)findViewById(R.id.info_screen_similar_issues);
        similar.setDividerPadding(16);
        new SimliarIssuesPopulator(this, similar).execute(issue);

        //
        List<ImageView> gradeStars = new ArrayList<>();
        gradeStars.add((ImageView)findViewById(R.id.info_screen_gradestar_1));
        gradeStars.add((ImageView)findViewById(R.id.info_screen_gradestar_2));
        gradeStars.add((ImageView)findViewById(R.id.info_screen_gradestar_3));
        gradeStars.add((ImageView)findViewById(R.id.info_screen_gradestar_4));
        gradeStars.add((ImageView)findViewById(R.id.info_screen_gradestar_5));
        gradesCount = (TextView)findViewById(R.id.info_screen_grades_count);
        new InfoScreenPopulateUserGrade(this, gradeStars, gradesCount).execute(issue);
        for (int i=0; i<gradeStars.size(); i++) {
            gradeStars.get(i).setOnClickListener(
                    new RateIssueListener(i+1,this,gradeStars,gradesCount,ratingStars, issue)
            );
        }
        //
        toggleDrawerButton = (ImageButton)findViewById(R.id.appbar_drawer_button);
        toggleDrawerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleNavigationDrawer();
            }
        });
        //
        shoppingCartCounter = (TextView)findViewById(R.id.appbar_shopping_cart_counter);
        readingListCounter = (TextView)findViewById(R.id.appbar_reading_list_counter);
        buyButton = (Button)findViewById(R.id.info_screen_btn_buy);
        buyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User current = UserSingleton.getInstance().getUser();
                if (current == null) {
                    Intent intent = new Intent(IssueInfoScreen.this, SignInScreen.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                }
                else {
                    if (!price.getText().toString().equalsIgnoreCase("N/A"))
                        new IssueToCartPusher(IssueInfoScreen.this.getApplicationContext(), shoppingCartCounter).
                                execute(current,issue);
                    else
                        Toast.makeText(IssueInfoScreen.this, "This comic issue can't be bought", Toast.LENGTH_LONG).
                                show();
                }
            }
        });
        favoritesButton = (Button)findViewById(R.id.info_screen_btn_favorites);
        favoritesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User current = UserSingleton.getInstance().getUser();
                if (current == null) {
                    Intent intent = new Intent(IssueInfoScreen.this, SignInScreen.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);
                }
                else {
                    new IssueToFavoritesPusher(IssueInfoScreen.this).execute(issue);
                }
            }
        });
        //
        shoppingCart = (ImageButton)findViewById(R.id.appbar_shopping_cart);
        shoppingCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(IssueInfoScreen.this, ShoppingCartScreen.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                i.putExtra("User", UserSingleton.getInstance().getUser() != null ?
                UserSingleton.getInstance().getUser().getUsername() : "anonymous");
                startActivity(i);
            }
        });
        readingList = (ImageButton)findViewById(R.id.appbar_reading_list);
        readingList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(IssueInfoScreen.this, ReadingListScreen.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                i.putExtra("User", UserSingleton.getInstance().getUser() != null ?
                        UserSingleton.getInstance().getUser().getUsername() : "anonymous");
                startActivity(i);
            }
        });
        //
        comments = (ListView)findViewById(R.id.info_screen_comments);
        commentsAdapter = new CommentsAdapter(this, issue);
        new CommentsPopulator(commentsAdapter).execute(issue);
        comments.setAdapter(commentsAdapter);
        comments.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                view.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
        setListViewHeightBasedOnChildren(comments);
        getFragmentManager().beginTransaction()
                .add(R.id.nav_drawer_fragment_issue_info, TempEmptyDrawer.newInstance())
                .commit();

    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            Log.v("ListAdapter", "Is null");
            return;
        }
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i=0; i<listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0) {
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));
            }
            view.measure(desiredWidth, UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount()-1));
        listView.setLayoutParams(params);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setCurrentSignedUser();
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
            drawerShow = false;
        }
        User user = UserSingleton.getInstance().getUser();
        if (user != null) {
            LinearLayout ll = (LinearLayout)findViewById(R.id.info_screen_comments_section);
            ll.setVisibility(View.VISIBLE);
            userInitials = (TextView)findViewById(R.id.info_screen_profile_avatar);
            userInitials.setText(user.getName().substring(0, 1).toUpperCase()
                    + user.getSurname().substring(0, 1).toUpperCase());
            comment = (EditText)findViewById(R.id.info_screen_post_comment);
            postComment = (ImageButton)findViewById(R.id.info_screen_submit_comment);
            postComment.setOnClickListener(new PostCommentListener(this, commentsAdapter,
                    comment,issue));


        }
        else {
            LinearLayout ll = (LinearLayout)findViewById(R.id.info_screen_comments_section);
            ll.setVisibility(View.GONE);
        }
    }

    private void setCurrentSignedUser() {
            WifiManager wm = (WifiManager)getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            String macAddress = wm.getConnectionInfo().getMacAddress();
            NavDrawerUser getCurrentUser = new NavDrawerUser(shoppingCartCounter, readingListCounter);
            getCurrentUser.execute(macAddress);
    }

    private void toggleNavigationDrawer() {
        if (drawerShow) {
            getFragmentManager().popBackStack();
            drawerShow = false;
            return;
        }
        drawerShow = true;
        getFragmentManager().beginTransaction()
                .setCustomAnimations(R.animator.drawer_to_right, R.animator.drawer_to_left,
                        R.animator.drawer_to_right, R.animator.drawer_to_left)
                .replace(R.id.nav_drawer_fragment_issue_info, NavDrawerFragment.newInstance())
                .addToBackStack(null)
                .commit();
    }
}
