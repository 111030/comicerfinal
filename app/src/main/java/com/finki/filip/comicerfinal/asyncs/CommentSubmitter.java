package com.finki.filip.comicerfinal.asyncs;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.finki.filip.comicerfinal.adapters.CommentsAdapter;
import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.gson.comicvine.Issue;
import com.finki.filip.comicerfinal.gson.other.Comment;

/**
 * Created by Filip on 30.1.2017.
 */

public class CommentSubmitter extends AsyncTask<String, Void, Comment> {

    private Context context;
    private CommentsAdapter adapter;
    private Issue issue;

    public CommentSubmitter(Context context, CommentsAdapter adapter, Issue issue) {
        this.context = context;
        this.adapter = adapter;
        this.issue = issue;
    }

    @Override
    protected Comment doInBackground(String... strings) {
        String message = strings[0];
        String dateSent = strings[1];
        int id = adapter.getCount()+1;
        return MongoDBHelper.updateIssueWihtComment(issue,message,dateSent,id);
    }

    @Override
    protected void onPostExecute(Comment comment) {
        if (comment != null) {
            adapter.add(comment);
        }
        else {
            Toast.makeText(context, "Error posting a comment", Toast.LENGTH_LONG).show();
        }
    }
}
