package com.finki.filip.comicerfinal.asyncs;

import android.content.Context;
import android.os.AsyncTask;

import com.finki.filip.comicerfinal.RegisterScreen;
import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.fragments.dialogs.SuccesfulRegistrationDialog;
import com.finki.filip.comicerfinal.gson.other.User;

/**
 * Created by Filip on 25.12.2016.
 */

public class RegisterUser extends AsyncTask<User, Integer, Boolean> {

    private Context context;
    private SuccesfulRegistrationDialog srd;

    public RegisterUser(Context context) {
        this.context = context;
        srd = SuccesfulRegistrationDialog.newInstance();
    }

    @Override
    protected Boolean doInBackground(User... users) {
        User toInsert = users[0];
        MongoDBHelper.registerUser(toInsert);
        return true;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        srd.show(((RegisterScreen)context).getFragmentManager().beginTransaction(), "SuccesfulRegistrationDialog");
    }
}
