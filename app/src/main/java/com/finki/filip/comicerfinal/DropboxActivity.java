package com.finki.filip.comicerfinal;

import android.support.v7.app.AppCompatActivity;

import com.finki.filip.comicerfinal.factories.ApiKeys;
import com.finki.filip.comicerfinal.singletons.DropboxClientFactory;

/**
 * Created by Filip on 12.2.2017.
 */

public abstract class DropboxActivity extends AppCompatActivity {
    @Override
    protected void onResume() {
        super.onResume();

        initAndLoadData(ApiKeys.DROPBOX_TOKEN);
    }

    private void initAndLoadData(String accessToken) {
        DropboxClientFactory.init(accessToken);
        loadData();
    }

    protected abstract void loadData();

}
