package com.finki.filip.comicerfinal.holders;

import android.content.Context;

import com.finki.filip.comicerfinal.adapters.MainScreenIssuesAdapter;

/**
 * Created by Filip on 11.1.2017.
 */

public class MainScreenIssuesAdapters {

    private MainScreenIssuesAdapter newIssuesAdapter;
    private MainScreenIssuesAdapter freeIssuesAdapter;
    private MainScreenIssuesAdapter popularIssuesAdapter;
    private MainScreenIssuesAdapter bestRatedIssuesAdapter;

    public MainScreenIssuesAdapters(Context context, int size) {
        newIssuesAdapter = new MainScreenIssuesAdapter(context, size);
        freeIssuesAdapter = new MainScreenIssuesAdapter(context, size);
        popularIssuesAdapter = new MainScreenIssuesAdapter(context, size);
        bestRatedIssuesAdapter = new MainScreenIssuesAdapter(context, size);
    }

    public MainScreenIssuesAdapter getNewIssuesAdapter() {
        return newIssuesAdapter;
    }

    public MainScreenIssuesAdapter getFreeIssuesAdapter() {
        return freeIssuesAdapter;
    }

    public MainScreenIssuesAdapter getPopularIssuesAdapter() {
        return popularIssuesAdapter;
    }

    public MainScreenIssuesAdapter getBestRatedIssuesAdapter() {
        return bestRatedIssuesAdapter;
    }
}
