package com.finki.filip.comicerfinal.asyncs;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.finki.filip.comicerfinal.IssueInfoScreen;
import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.gson.comicvine.Issue;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by Filip on 05.4.2017.
 */

public class IssueGetter extends AsyncTask<Integer, Integer, Issue> {

    private Context context;

    public IssueGetter(Context context) {
        this.context = context;
    }

    @Override
    protected Issue doInBackground(Integer... integers) {
        int id = integers[0];
        return MongoDBHelper.getIssueById(id);
    }

    @Override
    protected void onPostExecute(Issue issue) {
        if (issue != null) {
            Gson gson = new GsonBuilder().serializeNulls().create();
            Intent intent = new Intent(context, IssueInfoScreen.class);
            intent.putExtra("Issue", gson.toJson(issue, Issue.class));
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            context.startActivity(intent);
        }
    }
}
