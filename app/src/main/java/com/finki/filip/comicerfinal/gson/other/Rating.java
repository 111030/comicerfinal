package com.finki.filip.comicerfinal.gson.other;

import com.finki.filip.comicerfinal.gson.comicvine.Resource;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.io.Serializable;

/**
 * Created by Filip on 28.1.2017.
 */

public class Rating extends Resource implements Serializable {

    private int rating;
    private User user;

    public Rating() {}

    public Rating(int rating, User user) {
        this.rating = rating;
        this.user = user;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String toString() {
        Gson gson = new GsonBuilder().serializeNulls().create();
        JsonObject jo = new JsonObject();
        jo.addProperty("rating", rating);
        jo.addProperty("user", user != null ? gson.toJson(user, User.class) : null);
        return gson.toJson(jo);
    }
}
