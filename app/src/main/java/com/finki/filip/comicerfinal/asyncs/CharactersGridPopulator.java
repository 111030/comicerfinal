package com.finki.filip.comicerfinal.asyncs;

import android.os.AsyncTask;

import com.finki.filip.comicerfinal.adapters.CharactersAdapter;
import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.gson.comicvine.Character;

import java.util.List;

/**
 * Created by Filip on 04.2.2017.
 */

public class CharactersGridPopulator extends AsyncTask<Void, Void, List<Character>> {

    private CharactersAdapter adapter;

    public CharactersGridPopulator(CharactersAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    protected List<Character> doInBackground(Void... voids) {
        return MongoDBHelper.getCharacters();
    }

    @Override
    protected void onPostExecute(List<Character> characters) {
        adapter.addAll(characters);
    }
}
