package com.finki.filip.comicerfinal.asyncs;

import android.os.AsyncTask;
import android.widget.TextView;

import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.gson.comicvine.Issue;
import com.finki.filip.comicerfinal.gson.other.Comment;
import com.finki.filip.comicerfinal.gson.other.User;
import com.finki.filip.comicerfinal.singletons.UserSingleton;

/**
 * Created by Filip on 04.2.2017.
 */

public class LikeAComment extends AsyncTask<Integer, Void, Integer> {

    private Issue issue;
    private int commentIndex;
    private TextView likesCount;

    public LikeAComment(Issue issue, TextView likesCount) {
        this.issue = issue;
        this.likesCount = likesCount;
    }

    @Override
    protected Integer doInBackground(Integer... integers) {
        commentIndex = integers[0];
        Comment comment = (Comment)issue.getComments().get(commentIndex);
        User current = UserSingleton.getInstance().getUser();
        User newCurrent = new User();
        newCurrent.setEmail(current.getEmail());
        newCurrent.setUsername(current.getUsername());
        comment.getLikes().add(newCurrent);
        return MongoDBHelper.updateLikes(issue, commentIndex);
    }

    @Override
    protected void onPostExecute(Integer integer) {
        if (integer > -1) {
            likesCount.setText(String.valueOf(integer));
        }
        else {
        }
    }
}
