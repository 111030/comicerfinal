package com.finki.filip.comicerfinal.factories;


import com.finki.filip.comicerfinal.MainScreen;
import com.finki.filip.comicerfinal.gson.comicvine.Character;
import com.finki.filip.comicerfinal.gson.comicvine.CharacterResponse;
import com.finki.filip.comicerfinal.gson.comicvine.Image;
import com.finki.filip.comicerfinal.gson.comicvine.Issue;
import com.finki.filip.comicerfinal.gson.comicvine.IssueResponse;
import com.finki.filip.comicerfinal.gson.comicvine.Movie;
import com.finki.filip.comicerfinal.gson.comicvine.MovieResponse;
import com.finki.filip.comicerfinal.gson.comicvine.Person;
import com.finki.filip.comicerfinal.gson.comicvine.PersonResponse;
import com.finki.filip.comicerfinal.gson.comicvine.Power;
import com.finki.filip.comicerfinal.gson.comicvine.PowerResponse;
import com.finki.filip.comicerfinal.gson.comicvine.ResponseSingle;
import com.finki.filip.comicerfinal.gson.comicvine.Team;
import com.finki.filip.comicerfinal.gson.comicvine.TeamResponse;
import com.finki.filip.comicerfinal.gson.comicvine.Volume;
import com.finki.filip.comicerfinal.gson.comicvine.VolumeResponse;
import com.finki.filip.comicerfinal.gson.marvel.ComicPrice;
import com.finki.filip.comicerfinal.gson.other.Comment;
import com.finki.filip.comicerfinal.gson.other.Rating;
import com.finki.filip.comicerfinal.gson.other.SignedIn;
import com.finki.filip.comicerfinal.gson.other.User;
import com.finki.filip.comicerfinal.singletons.MongoSingleton;
import com.finki.filip.comicerfinal.singletons.UserSingleton;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.mongodb.Block;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Sorts;
import com.mongodb.client.model.UpdateOptions;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;

import org.bson.Document;
import org.bson.conversions.Bson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.elemMatch;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.in;
import static com.mongodb.client.model.Filters.ne;
import static com.mongodb.client.model.Filters.nin;
import static com.mongodb.client.model.Filters.or;
import static com.mongodb.client.model.Filters.text;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.push;
import static com.mongodb.client.model.Updates.set;

/**
 * Created by Filip on 12.11.2016.
 */

public class MongoDBHelper {

    public static synchronized User getSignedIn(String macAddress) {
        MongoDatabase db = MongoSingleton.getInstance().getDatabase();
        MongoCollection<Document> signedUsers = db.getCollection("SignedIn");
        Document currentUser = signedUsers.find(eq("macAddress", macAddress)).first();
        if (currentUser != null) {
            Gson gson = new GsonBuilder().create();
            SignedIn sUser = gson.fromJson(currentUser.toJson(), SignedIn.class);
            MongoCollection<Document> users = db.getCollection("User");
            Document currentUserInfo = users.find(eq("username", sUser.getUser().getUsername())).first();
            if (currentUserInfo != null) {
                return gson.fromJson(currentUserInfo.toJson(), User.class);
            }
        }
        return null;
    }

    public static synchronized boolean checkAlreadyRegisteredUsername(String username) {
        MongoDatabase db = MongoSingleton.getInstance().getDatabase();
        MongoCollection<Document> regUsers = db.getCollection("User");
        Document targetUser = regUsers.find(eq("username", username)).first();
        return (targetUser != null && !targetUser.isEmpty());
    }

    public static synchronized boolean checkAlreadyRegisteredEmail(String email) {
        MongoDatabase db = MongoSingleton.getInstance().getDatabase();
        MongoCollection<Document> regUsers = db.getCollection("User");
        Document targetUser = regUsers.find(eq("email", email)).first();
        return (targetUser != null && !targetUser.isEmpty());
    }

    public static synchronized void registerUser(User user) {
        MongoDatabase db = MongoSingleton.getInstance().getDatabase();
        MongoCollection<Document> regUsers = db.getCollection("User");
        Document doc = new Document("username", user.getUsername())
                .append("password", user.getPassword())
                .append("email", user.getEmail())
                .append("name", user.getName())
                .append("surname", user.getSurname())
                .append("born", user.getBorn())
                .append("gender", user.getGender())
                .append("country", new Document("name", user.getCountry().getName())
                .append("initials", user.getCountry().getInitials())
                .append("code", user.getCountry().getCode()))
                .append("code", user.getCode())
                .append("phoneNumber", user.getPhoneNumber())
                .append("address", user.getAddress())
                .append("credditCard", user.getCredditCard())
                .append("amount_credit_card", 29.99);
        regUsers.insertOne(doc);
    }

    public static synchronized boolean signInUser(String username, String password, int ip, String mac) {
        MongoDatabase db = MongoSingleton.getInstance().getDatabase();
        MongoCollection<Document> registeredUsers = db.getCollection("User");
        Document targetUser = registeredUsers.find(and(
                or(eq("username", username), eq("email", username)),
                eq("password", password))).first();
        if (targetUser == null || targetUser.isEmpty())
            return false;
        Date currentDate = new Date();
        MongoCollection<Document> signedUsers = db.getCollection("SignedIn");
        Document doc = new Document("user",targetUser)
                .append("macAddress",mac)
                .append("ipAddress",ip)
                .append("dateTime",currentDate.toString());
        signedUsers.insertOne(doc);
        return true;

    }

    public static synchronized boolean signOutUser(User user, String mac) {
        MongoDatabase db = MongoSingleton.getInstance().getDatabase();
        MongoCollection<Document> signedIn = db.getCollection("SignedIn");
        DeleteResult dr = signedIn.deleteOne(and(eq("user.username", user.getUsername()),
                eq("macAddress", mac)));
        return dr.getDeletedCount() > 0;
    }

    public static synchronized List<Issue> getNewIssues(int which, MongoDatabase db, int size) {
        String tag = "";
        Bson filter;
        if (which == MainScreen.MARVEL_COMICS) {
            tag = "MARVEL";
            filter = and(text(tag),
                    ne("image", null), ne("image.thumb_url", null),
                    ne("volume.name", null), ne("name", null), ne("prices", null));
        }

        else if (which == MainScreen.DC_COMICS) {
            tag = "DC_COMICS";
            filter = and(text(tag),
                    ne("image", null), ne("image.thumb_url", null),
                    ne("volume.name", null), ne("name", null));
        }
        else {
            tag = "OTHER";
            filter = and(eq("TAG", tag),
                    ne("image", null), ne("image.thumb_url", null),
                    ne("volume.name", null), ne("name", null));
        }
        //
        final List<Issue> result = new ArrayList<Issue>();
        for (int i=0; i<4; i++) {
            FindIterable<Document> iterable = db.getCollection("issues"+(i+1)).find(filter).sort(
                    new Document("date_last_updated", -1)).limit(size);
            iterable.forEach(new Block<Document>() {
                @Override
                public void apply(Document document) {
                    Gson gson = new GsonBuilder().create();
                    Issue issue = gson.fromJson(document.toJson(), Issue.class);
                    result.add(issue);
                }
            });
        }
        Issue []tempIssues = new Issue[result.size()];
        for (int i=0; i<result.size(); i++) {
            tempIssues[i] = (Issue)result.get(i);
        }


        Arrays.sort(tempIssues, new Comparator<Issue>() {
            public int compare(Issue i1, Issue i2) {
                if (i1.getDate_last_updated() == null)
                    return -1;
                if (i2.getDate_last_updated() == null)
                    return 1;
                Date dateI1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(i1.getDate_last_updated(), new ParsePosition(0));
                Date dateI2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(i2.getDate_last_updated(), new ParsePosition(0));
                return dateI2.compareTo(dateI1);
            }
        });

        List<Issue> newIssues =
                Arrays.asList(tempIssues).subList(0, tempIssues.length < size ? tempIssues.length : size);
        //Log.v("IssueNotShownError", "New issues - " + newIssues.size());
        return newIssues;
    }

    public static synchronized List<Issue> getFreeIssues(int which, MongoDatabase db, int size) {
        String tag = "";
        Bson filter;
        if (which == MainScreen.MARVEL_COMICS) {
            tag = "MARVEL";
            filter = and(text(tag),
                    elemMatch("prices", and(eq("type", "digitalPurchasePrice"), eq("price", 0))),
                    eq("TAG",tag), ne("image", null), ne("image.thumb_url", null),
                    ne("volume.name", null), ne("name", null));
        }
        else if (which == MainScreen.DC_COMICS) {
            tag = "DC_COMICS";
            filter = and(
                    text(tag), ne("image", null), ne("image.thumb_url", null),
                    ne("volume.name", null), ne("name", null));
        }
        else {
            tag = "OTHER";
            filter = and(
                    eq("TAG", tag), ne("image", null), ne("image.thumb_url", null),
                    ne("volume.name", null), ne("name", null));
        }
        //
        final List<Issue> result = new ArrayList<Issue>();
        for (int i=0; i<4; i++) {
            FindIterable<Document> iterable = db.getCollection("issues"+(i+1)).find(filter).
                    sort(new Document("prices.type", 1)).limit(size);
            iterable.forEach(new Block<Document>() {
                @Override
                public void apply(Document document) {
                    Gson gson = new GsonBuilder().create();
                    Issue issue = gson.fromJson(document.toJson(), Issue.class);
                    result.add(issue);
                }
            });
        }
        Issue []tempIssues = new Issue[result.size()];
        for (int i=0; i<result.size(); i++) {
            tempIssues[i] = (Issue)result.get(i);
        }

        Arrays.sort(tempIssues, new Comparator<Issue>() {
            public int compare(Issue i1, Issue i2) {
                if (i1.getPrices() == null || i1.getPrices().isEmpty())
                    return 1;
                if (i2.getPrices() == null || i2.getPrices().isEmpty())
                    return -1;
                return ((ComicPrice)i1.getPrices().get(0)).getType().compareTo(((ComicPrice)i2.getPrices().get(0)).getType());
            }
        });

        List<Issue> freeIssues =
                result.subList(0, tempIssues.length < size ? tempIssues.length : size);
        //Log.v("IssueNotShownError", "Free issues - " + freeIssues.size());
        return freeIssues;
    }

    public static synchronized List<Issue> getMostPopularIssues(int which, MongoDatabase db, int size) {
        String tag = "";
        Bson filter;
        if (which == MainScreen.MARVEL_COMICS) {
            tag = "MARVEL";
            filter = and(text(tag),
                    ne("image", null), ne("image.thumb_url", null),
                    ne("volume.name", null), ne("name", null), ne("prices", null));
        }
        else if (which == MainScreen.DC_COMICS) {
            tag = "DC_COMICS";
            filter = and(text(tag),
                    ne("image", null), ne("image.thumb_url", null),
                    ne("volume.name", null), ne("name", null));
        }
        else {
            tag = "OTHER";
            filter = and(eq("TAG", tag),
                    ne("image", null), ne("image.thumb_url", null),
                    ne("volume.name", null), ne("name", null));
        }
        //
        final List<Issue> result = new ArrayList<Issue>();
        for (int i=0; i<4; i++) {
            FindIterable<Document> iterable = db.getCollection("issues"+(i+1)).find(filter).
                    sort(new Document("popularity", -1)).limit(size);
            iterable.forEach(new Block<Document>() {
                @Override
                public void apply(Document document) {
                    Gson gson = new GsonBuilder().create();
                    Issue issue = gson.fromJson(document.toJson(), Issue.class);
                    result.add(issue);
                }
            });
        }
        Issue []tempIssues = new Issue[result.size()];
        for (int i=0; i<result.size(); i++) {
            tempIssues[i] = (Issue)result.get(i);
        }

        Arrays.sort(tempIssues, new Comparator<Issue>() {
            public int compare(Issue i1, Issue i2) {
                if (i1 == null)
                    return -1;
                if (i2 == null)
                    return 1;
                return Float.compare(i2.getPopularity(), i1.getPopularity());
            }
        });

        List<Issue> popular =
                result.subList(0, tempIssues.length < size ? tempIssues.length : size);
        //Log.v("IssueNotShownError", "Popular issues - " + popular.size());
        return popular;
    }

    public static synchronized List<Issue> getBestRatedIssues(int which, MongoDatabase db, int size) {
        String tag = "";
        Bson filter;
        if (which == MainScreen.MARVEL_COMICS) {
            tag = "MARVEL";
            filter = and(text(tag),
                    ne("image", null), ne("image.thumb_url", null),
                    ne("volume.name", null), ne("name", null), ne("prices", null));
        }
        else if (which == MainScreen.DC_COMICS) {
            tag = "DC_COMICS";
            filter = and(text(tag),
                    ne("image", null), ne("image.thumb_url", null),
                    ne("volume.name", null), ne("name", null));
        }
        else {
            tag = "OTHER";
            filter = and(eq("TAG", tag),
                    ne("image", null), ne("image.thumb_url", null),
                    ne("volume.name", null), ne("name", null));
        }
        final List<Issue> result = new ArrayList<Issue>();
        for (int i=0; i<4; i++) {
            FindIterable<Document> iterable = db.getCollection("issues"+(i+1)).find(filter).
                    sort(new Document("user_review", -1)).limit(size);
            iterable.forEach(new Block<Document>() {
                @Override
                public void apply(Document document) {
                    Gson gson = new GsonBuilder().create();
                    Issue issue = gson.fromJson(document.toJson(), Issue.class);
                    result.add(issue);
                }
            });
        }
        Issue []tempIssues = new Issue[result.size()];
        for (int i=0; i<result.size(); i++) {
            tempIssues[i] = (Issue)result.get(i);
        }

        Arrays.sort(tempIssues, new Comparator<Issue>() {
            public int compare(Issue i1, Issue i2) {
                if (i1 == null)
                    return -1;
                if (i2 == null)
                    return 1;
                return Float.compare(i2.getUser_review(), i1.getUser_review());
            }
        });

        List<Issue> bestRated =
                result.subList(0, tempIssues.length < size ? tempIssues.length : size);
        //Log.v("IssueNotShownError", "Best rated - " + bestRated.size());
        return bestRated;
    }

    public static synchronized boolean updateIssuesField(int id, String field, IssueResponse issue, MongoDatabase db) {
        for (int i=1; i<=4; i++) {
            MongoCollection<Document> issues = db.getCollection("issues"+i);
            List<Document> docs = new ArrayList<Document>();
            Gson gson = new GsonBuilder().serializeNulls().create();
            if (field.equalsIgnoreCase("person_credits")) {
                if (issue.getPerson_credits() != null) {
                    MongoCollection<Document> people = db.getCollection("people");
                    for (int j=0; j<issue.getPerson_credits().length; j++) {
                        Document temp = people.find(eq("_id",(issue.getPerson_credits()[j]).getId()))
                                .projection(new Document("_id", 1).append("name",1)).first();
                        docs.add(temp);
                    }
                }

            }
            else if (field.equalsIgnoreCase("character_credits")) {
                if (issue.getCharacter_credits() != null) {
                    MongoCollection<Document> characters = db.getCollection("characters");
                    for (int j=0; j<issue.getCharacter_credits().length; j++) {
                        Document temp = characters.find(eq("_id", (issue.getCharacter_credits()[j]).getId()))
                                .projection(new Document("_id",1).append("image",1)).first();
                        docs.add(temp);
                    }
                }
            }
            else {

            }

            UpdateResult result = issues.updateOne(eq("_id", id), set(field, docs));
            if (result.getModifiedCount() > 0)
                return true;
        }
        return false;
    }

    public static synchronized boolean insertIntoShoppingCart(User user, Issue issue, MongoDatabase db) {
        Gson gson = new GsonBuilder().serializeNulls().create();
        Document issueDoc = Document.parse(gson.toJson(issue, Issue.class));
        MongoCollection<Document> allUsers = db.getCollection("User");
        UpdateResult ur = allUsers.updateOne(and(
                eq("username", user.getUsername()),
                ne("shopping_cart._id", issue.getId())),push("shopping_cart", issueDoc));
        return ur.getModifiedCount() > 0;
    }

    public static synchronized int getShoopingCartItems(User user, MongoDatabase db) {
        if (user == null)
            return 0;
        MongoCollection<Document> users = db.getCollection("User");
        Document userDoc = users.find(eq("username", user.getUsername())).first();
        List<Issue> shopping_cart = (List<Issue>)userDoc.get("shopping_cart");
        return shopping_cart != null ? shopping_cart.size() : 0;
    }

    public static synchronized List<Issue> getShoppingCartItemsList(User user, MongoDatabase db) {
        MongoCollection<Document> users = db.getCollection("User");
        Document userDoc = users.find(eq("username", user.getUsername())).first();
        Gson gson = new GsonBuilder().serializeNulls().create();
        User newUser = gson.fromJson(userDoc.toJson(), User.class);
        return newUser.getShopping_cart();
    }

    public static synchronized int getRatingsCountForIssue(Issue issue, MongoDatabase db) {
        MongoCollection<Document> ratedIssues = db.getCollection("ratings");
        Gson gson = new GsonBuilder().serializeNulls().create();
        Document issueDoc = ratedIssues.find(eq("_id", issue.getId())).first();
        //List<User> usersRated = (List<User>)(issueDoc != null ?
        //issueDoc.get("users") : new ArrayList<User>());
        //List<User> usersRated = new ArrayList<>();
        if (issueDoc != null) {
            Issue newIssue = gson.fromJson(issueDoc.toJson(), Issue.class);
            Rating[] ratings = newIssue.getRatings();
            return (ratings == null ? 0 : ratings.length);
            //return (newIssue.getU)
        }
        return 0;
    }

    public static synchronized int updateShoppingCartForUser(List<Issue> issues, int index,
                                                    MongoDatabase db) {
        MongoCollection<Document> users = db.getCollection("User");
        String username = UserSingleton.getInstance().getUser().getUsername();
        Gson gson = new GsonBuilder().serializeNulls().create();
        int totalModified;
        if (index == -1) {
            totalModified = issues.size();
            issues.clear();
        }
        else {
            issues.remove(index);
            totalModified = 1;
        }

        List<Document> documentList = new ArrayList<Document>();
        for (Issue issue : issues) {
            documentList.add(Document.parse(gson.toJson(issue, Issue.class)));
        }
        UpdateResult result = users.updateOne(eq("username", username), set("shopping_cart", documentList));
        return totalModified;
    }

    public static synchronized List<Issue> getFavorites() {
        MongoCollection<Document> favorites = MongoSingleton.getInstance().
                getDatabase().getCollection("User");
        String username = UserSingleton.getInstance().getUser().getUsername();
        Document doc = favorites.find(eq("username", username)).first();
        Gson gson = new GsonBuilder().serializeNulls().create();
        User newUser = gson.fromJson(doc.toJson(), User.class);
        return newUser.getFavorites();
    }

    public static synchronized boolean insertInfoFavorites(Issue issue) {
        Gson gson = new GsonBuilder().serializeNulls().create();
        Document issueDoc = Document.parse(gson.toJson(issue, Issue.class));
        String username = UserSingleton.getInstance().getUser().getUsername();
        MongoCollection<Document> allUsers = MongoSingleton.getInstance().
                getDatabase().getCollection("User");
        UpdateResult ur = allUsers.updateOne(and(
                eq("username", username), ne("favorites._id", issue.getId())),
                push("favorites", issueDoc));
        return ur.getModifiedCount() > 0;
    }

    public static synchronized boolean updateFavorites(List<Issue> newIssues) {
        MongoCollection<Document> users = MongoSingleton.getInstance().getDatabase()
                .getCollection("User");
        String username = UserSingleton.getInstance().getUser().getUsername();
        Gson gson = new GsonBuilder().serializeNulls().create();
        List<Document> documentList = new ArrayList<Document>();
        for (Issue issue : newIssues) {
            documentList.add(Document.parse(gson.toJson(issue, Issue.class)));
        }
        UpdateResult result = users.updateOne(eq("username", username), set("favorites", documentList));
        return result.getModifiedCount() > 0;
    }

    public static synchronized int getReadingListItems(User user, MongoDatabase db) {
        if (user == null)
            return 0;
        MongoCollection<Document> users = db.getCollection("User");
        Document userDoc = users.find(eq("username", user.getUsername())).first();
        List<Issue> shopping_cart = (List<Issue>)userDoc.get("reading_list");
        return shopping_cart != null ? shopping_cart.size() : 0;
    }

    public static synchronized List<Issue> getReadingListItemsList(User user, MongoDatabase db) {
        MongoCollection<Document> users = db.getCollection("User");
        Document userDoc = users.find(eq("username", user.getUsername())).first();
        Gson gson = new GsonBuilder().serializeNulls().create();
        User newUser = gson.fromJson(userDoc.toJson(), User.class);
        return newUser.getReading_list();
    }

    public static synchronized int insertIntoReadingList(Issue[] issues) {
        Gson gson = new GsonBuilder().serializeNulls().create();
        String username = UserSingleton.getInstance().getUser().getUsername();
        MongoCollection<Document> allUsers = MongoSingleton.getInstance().
                getDatabase().getCollection("User");
        int totalModified = 0;
        for (int i=0; i<issues.length; i++) {
            Document issueDoc = Document.parse(gson.toJson(issues[i], Issue.class));
            float newPrice = allUsers.find(eq("username", username))
                    .first()
                    .getDouble("amount_credit_card")
                    .floatValue();
            List<ComicPrice> prices = issues[i].getPrices();
            for (int j=0; j<prices.size(); j++) {
                if (prices.get(j).getType().equalsIgnoreCase("digitalPurchaseSize")) {
                    newPrice -= prices.get(j).getPrice();
                    break;
                }
                if (j == prices.size()-1) {
                    newPrice -= prices.get(j).getPrice();
                }
            }
            UpdateResult ur = allUsers.updateOne(and(
                    eq("username", username), ne("reading_list._id", issues[i].getId())),
                    combine(push("reading_list", issueDoc), set("amount_credit_card", newPrice)));
            totalModified += ur.getModifiedCount();
        }

        return totalModified;
    }

    public static synchronized float getReadingListTotal() {
        String username = UserSingleton.getInstance().getUser().getUsername();
        MongoCollection<Document> users = MongoSingleton.getInstance().getDatabase()
                .getCollection("User");
        Document userDoc = users.find(eq("username", username)).first();
        Gson gson = new GsonBuilder().serializeNulls().create();
        User current = gson.fromJson(userDoc.toJson(), User.class);
        List<Issue> readingList = current.getReading_list();
        float sum = 0;
        for (Issue issue : readingList) {
            List<ComicPrice> prices = issue.getPrices();
            for (int i=0; i<prices.size(); i++) {
                if (prices.get(i).getType().equalsIgnoreCase("digitalPurchasePrice")) {
                    sum += prices.get(i).getPrice();
                    break;
                }
                if (i == prices.size() - 1) {
                    sum += prices.get(i).getPrice();
                }
            }
        }
        return sum;
    }

    public static synchronized boolean issueExistsInReadingList(Issue issue, MongoDatabase db) {
        User current = UserSingleton.getInstance().getUser();
        MongoCollection<Document> users = db.getCollection("User");
        FindIterable<Document> doc = users.find(and(eq("username", current.getUsername()),
                elemMatch("reading_list", eq("_id", issue.getId()))));
        return doc != null && doc.first() != null;
    }

    public static synchronized int getRatingByUserForIssue(Issue issue) {
        MongoCollection<Document> ratings = MongoSingleton.getInstance().getDatabase()
                .getCollection("ratings");
        User current = UserSingleton.getInstance().getUser();
        if (current == null)
            return -1;
        Document userWithRating = ratings.find(and(eq("_id", issue.getId()),
                elemMatch("ratings", eq("user.username", current.getUsername()))))
                .projection(new Document("_id", 1).append("ratings", 1)).first();
        if (userWithRating == null)
            return -1;
        //Log.v("GradeStarsEvent", userWithRating.toJson());
        Gson gson = new GsonBuilder().serializeNulls().create();
        Issue newIssue = gson.fromJson(userWithRating.toJson(), Issue.class);
        for (int i=0; i<newIssue.getRatings().length; i++) {
            Rating r = newIssue.getRatings()[i];
            if (r.getUser().getUsername().equalsIgnoreCase(current.getUsername())) {
                return r.getRating();
            }

        }
        return -1;

    }

    public static synchronized int updateRatingsForIssue(int userRating, Issue issue) {
        if (getRatingByUserForIssue(issue) != -1) {
            return -1;
        }

        MongoCollection<Document> ratings = MongoSingleton.getInstance().getDatabase()
                .getCollection("ratings");
        User current = UserSingleton.getInstance().getUser();
        Rating r = new Rating(userRating, current);
        Gson gson = new GsonBuilder().serializeNulls().create();
        Document rDocument = Document.parse(gson.toJson(r, Rating.class));
        UpdateResult ur = ratings.updateOne(eq("_id", issue.getId()), push("ratings", rDocument)
        , new UpdateOptions().upsert(true).bypassDocumentValidation(true));
        if (ur.getModifiedCount() <= 0 && ur.getUpsertedId() == null) {
            return -1;
        }

        return updateIssueRating(userRating, issue);

    }

    public static synchronized int updateIssueRating(int userRating, Issue issue) {
        int newUserReview = (int)(issue.getUser_review() == 0 ?
                        userRating : (issue.getUser_review() + userRating) / (float)2);
        for (int i=1; i<=4; i++) {
            MongoCollection<Document> issues = MongoSingleton.getInstance().getDatabase()
                    .getCollection("issues"+i);
            UpdateResult ur = issues.updateOne(eq("_id", issue.getId()),
                    set("user_review", newUserReview));
            if (ur.getModifiedCount() > 0)
                return newUserReview;

        }
        return -1;
    }

    public static synchronized int getRatingsCountForIssue(Issue issue) {
        MongoCollection<Document> ratings = MongoSingleton.getInstance().getDatabase()
                .getCollection("ratings");
        Document issueDoc = ratings.find(eq("_id", issue.getId())).first();
        if (issueDoc == null)
            return -1;
        Gson gson = new GsonBuilder().serializeNulls().create();
        Issue ratedIssue = gson.fromJson(issueDoc.toJson(), Issue.class);
        return ratedIssue.getRatings().length;
    }

    public static synchronized int updateLikes(Issue issue, int commentIndex) {
        User current = UserSingleton.getInstance().getUser();
        MongoCollection<Document> commentsDoc = MongoSingleton.getInstance().getDatabase()
                .getCollection("comments");
        Gson gson = new GsonBuilder().serializeNulls().create();
        Document doc = Document.parse(gson.toJson(issue, Issue.class));
        UpdateResult ur = commentsDoc.replaceOne(eq("_id", issue.getId()),
                doc,
                new UpdateOptions().upsert(true).bypassDocumentValidation(true));
        if (ur.getModifiedCount() > 0 || ur.getUpsertedId() != null)
            return ((Comment)issue.getComments().get(commentIndex)).getLikes().size();
        return -1;
    }

    public static synchronized List<Comment> getCommentsForIssue(Issue issue) {
        MongoCollection<Document> comments = MongoSingleton.getInstance().getDatabase()
                .getCollection("comments");
        Document doc = comments.find(eq("_id", issue.getId())).first();
        Gson gson = new GsonBuilder().serializeNulls().create();
        if (doc != null) {
            Issue newIssue = gson.fromJson(doc.toJson(), Issue.class);
            return newIssue.getComments();
        }
        return null;
    }

    public static synchronized Comment updateIssueWihtComment(Issue issue, String message,
                                                              String dateSent, int id) {
        MongoCollection<Document> comments = MongoSingleton.getInstance().getDatabase()
                .getCollection("comments");
        User current = UserSingleton.getInstance().getUser();
        User newCurrent = new User();
        newCurrent.setUsername(current.getUsername());
        newCurrent.setEmail(current.getEmail());
        newCurrent.setName(current.getName());
        newCurrent.setSurname(current.getSurname());
        Comment comment = new Comment(id,newCurrent, message,new ArrayList<User>(),dateSent);
        Gson gson = new GsonBuilder().serializeNulls().create();
        Document document = Document.parse(gson.toJson(comment, Comment.class));
        UpdateResult ur = comments.updateOne(eq("_id", issue.getId()), push("comments", document),
                new UpdateOptions().upsert(true).bypassDocumentValidation(true));
        return ur.getModifiedCount() > 0 || ur.getUpsertedId() != null ? comment : null;
    }

    public static synchronized List<Character> getCharacters() {
        MongoDatabase db = MongoSingleton.getInstance().getDatabase();
        MongoCollection<Document> characters = db.getCollection("characters");
        Gson gson = new GsonBuilder().serializeNulls().create();
        FindIterable<Document> charactersDoc = characters.find(and(
                ne("first_appeared_in_issue", null), ne("image.thumb_url", null), ne("name",null),ne("publisher", null)))
                .projection(
                new Document("first_appeared_in_issue._id", 1)
                        .append("api_detail_url", 1)
                        .append("image", 1)
                        .append("name", 1)
                        .append("publisher.name", 1))
                .limit(100);
        List<Character> charactersArray = new ArrayList<>();
        Iterator<Document> iterator = charactersDoc.iterator();
        while (iterator.hasNext()) {
            Character character = gson.fromJson(iterator.next().toJson(), Character.class);
            charactersArray.add(character);
            for (int i=1; i<=4; i++) {
                MongoCollection<Document> issues = db.getCollection("issues"+i);
                Document d = issues.find(eq("_id", character.getFirst_appeared_in_issue().getId())).first();
                if (d != null) {
                    character.getFirst_appeared_in_issue().setName(d.getString("name"));
                    break;
                }
            }
        }
        return charactersArray;
    }

    public static synchronized Character getCharacter(int id, String apiDetailUrl) {
        MongoCollection<Document> characters = MongoSingleton.getInstance().getDatabase()
                .getCollection("characters");
        Gson gson = new GsonBuilder().serializeNulls().create();
        Document document = characters.find(eq("_id", id)).first();
        Character character = gson.fromJson(document.toJson(), Character.class);
        if (character.getCreators().length == 0 && character.getPowers().length == 0
                && character.getTeams().length == 0 && character.getCharacter_friends().length == 0
                && character.getCharacter_enemies().length == 0
                && character.getIssues_died_in().length == 0 && character.getMovies().length == 0) {
            //
            String urls = apiDetailUrl
                    + ComicVineLinks.keyFormat + ComicVineLinks.characterResourceFields;
            URLConnection http = null;
            BufferedReader br = null;
            try {
                URL url = new URL(urls);
                http = url.openConnection();
                if (http.getContentLength() == -1) {
                    urls = apiDetailUrl.replaceFirst("http", "https")
                            + ComicVineLinks.keyFormat + ComicVineLinks.characterResourceFields;
                    url = new URL(urls);
                    http = url.openConnection();
                }
                    //Log.v("AdditionalInfo", "SUCCESS");
                    br = new BufferedReader(new InputStreamReader(http.getInputStream(), "utf-8"));
                    Type t = new TypeToken<ResponseSingle<CharacterResponse>>(){}.getType();
                    ResponseSingle<CharacterResponse> response = gson.fromJson(br, t);
                    CharacterResponse cResponse = response.getResults();
                    br.close();
                    character.setBirth(cResponse.getBirth());
                    character.setCount_of_issue_appearances(cResponse.getCount_of_issue_appearances());
                    PersonResponse[] creators = cResponse.getCreators();
                    Person[] pCreators = new Person[]{};
                    if (creators != null) {
                        pCreators = new Person[creators.length];
                        for (int i=0; i<creators.length; i++) {
                            pCreators[i] = creators[i].getPerson();
                        }
                    }
                    character.setCreators(pCreators);
                    character.setFirst_appeared_in_issue(cResponse.getFirst_appeared_in_issue().getIssue());
                    PowerResponse[] powers = cResponse.getPowers();
                    Power[] pPowers = new Power[]{};
                    if (powers != null) {
                        pPowers = new Power[powers.length];
                        for (int i=0; i<powers.length; i++) {
                            pPowers[i] = powers[i].getPower();
                        }
                    }
                    character.setPowers(pPowers);
                    TeamResponse[] teams = cResponse.getTeams();
                    Team[] tTeams = new Team[]{};
                    if (teams != null) {
                        tTeams = new Team[teams.length];
                        for (int i=0; i<teams.length; i++) {
                            tTeams[i] = teams[i].getTeam();
                            url = new URL(teams[i].getApi_detail_url()+ComicVineLinks.keyFormat+"&field_list=image");
                            http = url.openConnection();
                            br = new BufferedReader(new InputStreamReader(http.getInputStream(), "utf-8"));
                            Type teamt = new TypeToken<ResponseSingle<TeamResponse>>(){}.getType();
                            ResponseSingle<TeamResponse> teamResponse = gson.fromJson(br, teamt);
                            Image image = teamResponse.getResults().getImage();
                            tTeams[i].setImage(image);
                            br.close();
                        }
                    }
                    character.setTeams(tTeams);
                    CharacterResponse[] friends = cResponse.getCharacter_friends();
                    Character[] fFriends = new Character[]{};
                    if (friends != null) {
                        fFriends = new Character[friends.length];
                        for (int i=0; i<friends.length; i++) {
                            fFriends[i] = friends[i].getCharacter();
                            url = new URL(fFriends[i].getApi_detail_url()+ComicVineLinks.keyFormat+"&field_list=image");
                            http = url.openConnection();
                            br = new BufferedReader(new InputStreamReader(http.getInputStream(), "utf-8"));
                            Type chart = new TypeToken<ResponseSingle<CharacterResponse>>(){}.getType();
                            ResponseSingle<CharacterResponse> teamResponse = gson.fromJson(br, chart);
                            Image image = teamResponse.getResults().getImage();
                            fFriends[i].setImage(image);
                            br.close();
                        }
                    }
                    character.setCharacter_friends(fFriends);
                    CharacterResponse[] enemies = cResponse.getCharacter_enemies();
                    Character[] eEnemies = new Character[]{};
                    if (enemies != null) {
                        eEnemies = new Character[enemies.length];
                        for (int i=0; i<enemies.length; i++) {
                            eEnemies[i] = enemies[i].getCharacter();
                            url = new URL(enemies[i].getApi_detail_url()+ComicVineLinks.keyFormat+"&field_list=image");
                            http = url.openConnection();
                            br = new BufferedReader(new InputStreamReader(http.getInputStream(), "utf-8"));
                            Type chart = new TypeToken<ResponseSingle<CharacterResponse>>(){}.getType();
                            ResponseSingle<CharacterResponse> teamResponse = gson.fromJson(br, chart);
                            Image image = teamResponse.getResults().getImage();
                            eEnemies[i].setImage(image);
                            br.close();
                        }
                    }
                    character.setCharacter_enemies(eEnemies);
                    IssueResponse[] diedIn = cResponse.getIssues_died_in();
                    Issue[] dDienIn = new Issue[]{};
                    if (diedIn != null) {
                        dDienIn = new Issue[diedIn.length];
                        for (int i=0; i<diedIn.length; i++) {
                            dDienIn[i] = diedIn[i].getIssue();
                            url = new URL(dDienIn[i].getApi_detail_url()+ComicVineLinks.keyFormat+"&field_list=image");
                            http = url.openConnection();
                            br = new BufferedReader(new InputStreamReader(http.getInputStream(), "utf-8"));
                            Type issuet = new TypeToken<ResponseSingle<IssueResponse>>(){}.getType();
                            ResponseSingle<IssueResponse> teamResponse = gson.fromJson(br, issuet);
                            Image image = teamResponse.getResults().getImage();
                            dDienIn[i].setImage(image);
                            br.close();
                        }
                    }
                    character.setIssues_died_in(dDienIn);
                    MovieResponse[] movies = cResponse.getMovies();
                    Movie[] mMovies = new Movie[]{};
                    if (movies != null) {
                        mMovies = new Movie[movies.length];
                        for (int i=0; i<movies.length; i++) {
                            mMovies[i] = movies[i].getMovie();
                            url = new URL(mMovies[i].getApi_detail_url()+ComicVineLinks.keyFormat+"&field_list=image");
                            http = url.openConnection();
                            br = new BufferedReader(new InputStreamReader(http.getInputStream(), "utf-8"));
                            Type moviet = new TypeToken<ResponseSingle<MovieResponse>>(){}.getType();
                            ResponseSingle<MovieResponse> teamResponse = gson.fromJson(br, moviet);
                            Image image = teamResponse.getResults().getImage();
                            mMovies[i].setImage(image);
                            br.close();
                        }
                    }
                    character.setMovies(mMovies);
                    //
                    document = Document.parse(gson.toJson(character, Character.class));
                    UpdateResult ur = characters.replaceOne(eq("_id", id), document);



            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return character;
    }

    public static synchronized List<Volume> getVolumes() {
        MongoDatabase db = MongoSingleton.getInstance().getDatabase();
        MongoCollection<Document> volumes = db.getCollection("volumes");
        final Gson gson = new GsonBuilder().serializeNulls().create();
        FindIterable<Document> volumeDocs = volumes.find(and(
                ne("image", null), ne("name", null), ne("start_year", null), ne("first_issue.name", null)
        )).projection(new Document("api_detail_url", 1).append("image", 1)
        .append("name", 1).append("start_year", 1).append("first_issue.name", 1)).limit(100);
        final List<Volume> volumesList = new ArrayList<>();
        volumeDocs.forEach(new Block<Document>() {
            @Override
            public void apply(Document document) {
                volumesList.add(gson.fromJson(document.toJson(), Volume.class));
            }
        });
        return volumesList;
    }

    public static synchronized Volume getVolume(int id, String apiDetailUrl) {
        MongoCollection<Document> volumes = MongoSingleton.getInstance().getDatabase()
                .getCollection("volumes");
        Gson gson = new GsonBuilder().serializeNulls().create();
        Document volumeDoc = volumes.find(eq("_id", id)).first();
        Volume volume = gson.fromJson(volumeDoc.toJson(), Volume.class);
        if (volume.getPeople().length == 0 && volume.getCharacters().length == 0) {
            //
            String urls = apiDetailUrl
                    + ComicVineLinks.keyFormat + ComicVineLinks.volumeResourceFields;
            HttpURLConnection http = null;
            BufferedReader br = null;
            try {
                URL url = new URL(urls);
                http = (HttpURLConnection)url.openConnection();
                if (http.getContentLength() != -1) {
                    br = new BufferedReader(new InputStreamReader(http.getInputStream(), "utf-8"));
                    Type t = new TypeToken<ResponseSingle<VolumeResponse>>(){}.getType();
                    ResponseSingle<VolumeResponse> response = gson.fromJson(br, t);
                    VolumeResponse vResponse = response.getResults();
                    br.close();
                    http.disconnect();
                    //
                    CharacterResponse[] charactersR = vResponse.getCharacters();
                    Character[] characters = new Character[]{};
                    if (charactersR != null) {
                        characters = new Character[charactersR.length];
                        for (int i=0; i<charactersR.length; i++) {
                            characters[i] = charactersR[i].getCharacter();
                        }
                    }
                    volume.setCharacters(characters);
                    //
                    PersonResponse[] peopleR = vResponse.getPeople();
                    Person[] people = new Person[]{};
                    if (peopleR != null) {
                        people = new Person[peopleR.length];
                        for (int i=0; i<peopleR.length; i++) {
                            people[i] = peopleR[i].getPerson();
                        }
                    }
                    volume.setPeople(people);
                    //
                    volumeDoc = Document.parse(gson.toJson(volume, Volume.class));
                    UpdateResult ur = volumes.replaceOne(eq("_id", id), volumeDoc);
                }
                else {
                }


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return  volume;
    }

    public static synchronized List<Issue> getTopVolumeIssues(int volumeId) {
        final Gson gson = new GsonBuilder().serializeNulls().create();
        final List<Issue> topIssues = new ArrayList<>();
        for (int i=1; i<=4; i++) {
            MongoCollection<Document> issues = MongoSingleton.getInstance().getDatabase()
                    .getCollection("issues"+i);
            FindIterable<Document> docs = issues.find(eq("volume._id", volumeId))
                    .sort(Sorts.descending("popularity"))
                    .limit(10);
            docs.forEach(new Block<Document>() {
                @Override
                public void apply(Document document) {
                    topIssues.add(gson.fromJson(document.toJson(), Issue.class));
                }
            });
        }
        Arrays.sort(topIssues.toArray(), new Comparator<Object>() {
            @Override
            public int compare(Object o, Object t1) {
                Issue i1 = (Issue)o;
                Issue i2 = (Issue)t1;
                return Float.compare(i2.getPopularity(), i1.getPopularity());
            }
        });
        return topIssues.subList(0, 10);
    }

    public static synchronized List<Movie> getMovies() {
        MongoDatabase db = MongoSingleton.getInstance().getDatabase();
        MongoCollection<Document> movies = db.getCollection("movies");
        final Gson gson = new GsonBuilder().serializeNulls().create();
        FindIterable<Document> moviesDocs = movies.find(and(
                ne("image.thumb_url", null),ne("name", null),ne("release_date", null),
                ne("box_office_revenue", null)
        )).projection(new Document("api_detail_url", 1).append("image", 1)
        .append("name", 1).append("release_date", 1).append("box_office_revenue", 1)).limit(100);
        final List<Movie> moviesList = new ArrayList<>();
        moviesDocs.forEach(new Block<Document>() {
            @Override
            public void apply(Document document) {
                moviesList.add(gson.fromJson(document.toJson(), Movie.class));
            }
        });
        return moviesList;
    }

    public static synchronized List<Issue> getSimilarIssues(final Issue issue) {
        MongoDatabase db = MongoSingleton.getInstance().getDatabase();
        final Gson gson = new GsonBuilder().serializeNulls().create();
        final List<Issue> similar = new ArrayList<>();
        //
        //cover date * 0.1
        //popularity * 0.6
        //volume.name * 0.3
        //character_credits
        //person_credits
        /*issuesColl.aggregate(Arrays.asList(
                Aggregates.project(Projections.fields(
                        Projections.computed("similarity",
                                new Document("$sqrt", )
                ))
        ));*/
        final float popularity = issue.getPopularity();
        final String volumeName = issue.getVolume() != null ? issue.getVolume().getName() : null;
        final List<Integer> charsIds = new ArrayList<>();
        if (issue.getCharacter_credits() != null) {
            for (int i=0; i<issue.getCharacter_credits().length; i++) {
                charsIds.add(issue.getCharacter_credits()[i].getId());
            }
        }
        final List<Integer> personIds = new ArrayList<>();
        if (issue.getPerson_credits() != null) {
            for (int i=0; i<issue.getPerson_credits().length; i++) {
                personIds.add(issue.getPerson_credits()[i].getId());
            }
        }
        //
        for (int i=1; i<=4; i++) {
            MongoCollection<Document> issuesColl = db.getCollection("issues"+i);
            FindIterable<Document> sDocs = issuesColl.find(or(
                    eq("popularity", popularity),
                    and(ne("popularity",popularity),eq("volume.name", volumeName)),
                    and(ne("popularity",popularity),ne("volume.name", volumeName),in("character_credits._id",charsIds)),
                    and(ne("popularity",popularity),ne("volume.name", volumeName),nin("character_credits._id",charsIds),in("person_credits._id", personIds))
            )).limit(10);
            sDocs.forEach(new Block<Document>() {
                @Override
                public void apply(Document document) {
                    similar.add(gson.fromJson(document.toJson(), Issue.class));
                }
            });
        }
        Arrays.sort(similar.toArray(), new Comparator<Object>() {

            @Override
            public int compare(Object o, Object t1) {
                Issue i1 = (Issue)o;
                Issue i2 = (Issue)t1;
                //
                float pop1 = i1.getPopularity();
                String vol1name = i1.getVolume() != null ? i1.getVolume().getName() : null;
                List<Integer> charsIds1 = new ArrayList<>();
                if (i1.getCharacter_credits() != null) {
                    for (int i=0; i<i1.getCharacter_credits().length; i++) {
                        charsIds1.add(i1.getCharacter_credits()[i].getId());
                    }
                }
                List<Integer> personIds1 = new ArrayList<>();
                if (i1.getPerson_credits() != null) {
                    for (int i=0; i<i1.getPerson_credits().length; i++) {
                        personIds1.add(i1.getPerson_credits()[i].getId());
                    }
                }
                //
                float pop2 = i2.getPopularity();
                String vol2name = i2.getVolume() != null ? i2.getVolume().getName() : null;
                List<Integer> charsIds2 = new ArrayList<>();
                if (i2.getCharacter_credits() != null) {
                    for (int i=0; i<i2.getCharacter_credits().length; i++) {
                        charsIds2.add(i2.getCharacter_credits()[i].getId());
                    }
                }
                List<Integer> personIds2 = new ArrayList<>();
                if (i2.getPerson_credits() != null) {
                    for (int i=0; i<i2.getPerson_credits().length; i++) {
                        personIds2.add(i2.getPerson_credits()[i].getId());
                    }
                }
                //
                return Double.compare(
                        Math.sqrt(0.5*(pop1-popularity)*(pop1-popularity)+
                                0.3*(vol1name.equalsIgnoreCase(volumeName) ? 0 : 1)*(vol1name.equalsIgnoreCase(volumeName) ? 0 : 1)+
                        0.15*(countSame(charsIds1,charsIds)*countSame(charsIds1,charsIds))+
                        0.05*(countSame(personIds1,personIds)*countSame(personIds1,personIds))),
                        Math.sqrt(0.5*(pop2-popularity)*(pop2-popularity)+
                                0.3*(vol2name.equalsIgnoreCase(volumeName) ? 0 : 1)*(vol2name.equalsIgnoreCase(volumeName) ? 0 : 1)+
                                0.15*(countSame(charsIds2,charsIds)*countSame(charsIds2,charsIds))+
                                0.05*(countSame(personIds2,personIds)*countSame(personIds2,personIds)))
                );
            }

            private int countSame(List<Integer> l1, List<Integer> l2) {
                int count = 0;
                for (int i=0; i<l1.size(); i++) {
                    for (int j=0; j<l2.size(); j++) {
                        if (l1.get(i) == l2.get(j)) {
                            count++;
                            break;
                        }
                    }
                }
                return l1.size() - count;
            }
        });


        return similar.subList(0,similar.size() >= 10 ? 10 : similar.size());
    }

    public static synchronized List<Issue> getIssueSearchResults(String query) {
        MongoDatabase db = MongoSingleton.getInstance().getDatabase();
        final Gson gson = new GsonBuilder().serializeNulls().create();
        final List<Issue> result = new ArrayList();
        for (int i=1; i<=4; i++) {
            MongoCollection<Document> dbColl = db.getCollection("issues"+i);
            FindIterable<Document> docResults = dbColl.find(text(query)).limit(8);
            docResults.forEach(new Block<Document>() {
                @Override
                public void apply(Document document) {
                    result.add(gson.fromJson(document.toJson(), Issue.class));
                }
            });
        }
        return result.subList(0, 8);
    }

    public static synchronized List<Volume> getVolumeSearchResults(String query) {
        MongoDatabase db = MongoSingleton.getInstance().getDatabase();
        MongoCollection<Document> dbColl = db.getCollection("volumes");
        final Gson gson = new GsonBuilder().serializeNulls().create();
        final List<Volume> result = new ArrayList<>();
        final FindIterable<Document> docResults = dbColl.find(text(query)).limit(8);
        docResults.forEach(new Block<Document>() {
            @Override
            public void apply(Document document) {
                result.add(gson.fromJson(document.toJson(), Volume.class));
            }
        });
        return result;
    }

    public static synchronized List<Character> getCharacterSearchResults(String query) {
        MongoDatabase db = MongoSingleton.getInstance().getDatabase();
        MongoCollection<Document> dbColl = db.getCollection("characters");
        final Gson gson = new GsonBuilder().serializeNulls().create();
        final List<Character> result = new ArrayList<>();
        final FindIterable<Document> docResults = dbColl.find(text(query)).limit(8);
        docResults.forEach(new Block<Document>() {
            @Override
            public void apply(Document document) {
                result.add(gson.fromJson(document.toJson(), Character.class));
            }
        });
        return result;
    }

    public static synchronized List<Movie> getMovieSearchResults(String query) {
        MongoDatabase db = MongoSingleton.getInstance().getDatabase();
        MongoCollection<Document> dbColl = db.getCollection("movies");
        final Gson gson = new GsonBuilder().serializeNulls().create();
        final List<Movie> result = new ArrayList<>();
        final FindIterable<Document> docResults = dbColl.find(text(query)).limit(8);
        docResults.forEach(new Block<Document>() {
            @Override
            public void apply(Document document) {
                result.add(gson.fromJson(document.toJson(), Movie.class));
            }
        });
        return result;
    }

    public static synchronized String getDropboxLink(int id) {
        MongoDatabase db = MongoSingleton.getInstance().getDatabase();
        for (int i=1; i<=4; i++) {
            MongoCollection<Document> issues = db.getCollection("issues"+i);
            Document d = issues.find(eq("_id", id)).first();
            if (d != null) {
                return d.getString("readingUrl");
            }

        }
        return null;
    }

    public static synchronized Issue getIssueById(int id) {
        MongoDatabase db = MongoSingleton.getInstance().getDatabase();
        Gson gson = new GsonBuilder().serializeNulls().create();
        for (int i=0; i<4; i++) {
            MongoCollection<Document> mc = db.getCollection("issues"+(i+1));
            Document d = mc.find(eq("_id", id)).first();
            if (d != null) {
                return gson.fromJson(d.toJson(), Issue.class);
            }
        }
        return null;
    }

}
