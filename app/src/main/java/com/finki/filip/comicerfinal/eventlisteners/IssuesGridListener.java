package com.finki.filip.comicerfinal.eventlisteners;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;

import com.finki.filip.comicerfinal.asyncs.IssueGetter;

/**
 * Created by Filip on 29.10.2016.
 */

public class IssuesGridListener implements AdapterView.OnItemClickListener {

    private Context context;

    public IssuesGridListener(Context context) {
        this.context = context;
    }
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        new IssueGetter(context).execute((int)adapterView.getItemIdAtPosition(i));

    }
}
