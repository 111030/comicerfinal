package com.finki.filip.comicerfinal.asyncs;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.finki.filip.comicerfinal.CharacterInfoScreen;
import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.gson.comicvine.Character;

/**
 * Created by Filip on 05.2.2017.
 */

public class CharacterGetter extends AsyncTask<Void, Void, Character> {

    private Context context;
    private int id;
    private String apiDetailUrl;

    public CharacterGetter(Context context, int id, String apiDetailUrl) {
        this.context = context;
        this.id = id;
        this.apiDetailUrl = apiDetailUrl;
    }

    @Override
    protected Character doInBackground(Void... voids) {
        return MongoDBHelper.getCharacter(id, apiDetailUrl);
    }

    @Override
    protected void onPostExecute(Character character) {
        if (character != null) {
            ((CharacterInfoScreen)context).updateTheUI(character);
        }
    }
}
