package com.finki.filip.comicerfinal.gson.comicvine;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.Serializable;

public class PublisherResponse extends Resource implements Serializable{

	/**
	 * 
	 */
	private String aliases;
	private String api_detail_url;
	private String deck;
	private String description;
	private int id;
	private Image image;
	private String location_city;
	private String location_state;
	private String name;
	private String site_detail_url;
	private VolumeResponse[] volumes;
	
	public PublisherResponse() {
		super();
		volumes = new VolumeResponse[]{};
	}

	public PublisherResponse(String aliases, String api_detail_url, String deck, String description, 
			int id, Image image, String location_city, String location_state,
			String name, String site_detail_url, VolumeResponse[] volumes) {
		super();
		this.aliases = aliases;
		this.api_detail_url = api_detail_url;
		this.deck = deck;
		this.description = description;
		this.id = id;
		this.location_city = location_city;
		this.location_state = location_state;
		this.name = name;
		this.site_detail_url = site_detail_url;
		this.volumes = volumes;
	}

	public String getAliases() {
		return aliases;
	}

	public void setAliases(String aliases) {
		this.aliases = aliases;
	}

	public String getApi_detail_url() {
		return api_detail_url;
	}

	public void setApi_detail_url(String api_detail_url) {
		this.api_detail_url = api_detail_url;
	}
	
	public String getDeck() {
		return deck;
	}

	public void setDeck(String deck) {
		this.deck = deck;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public void setId(int _id) {
		this.id = id;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public String getLocation_city() {
		return location_city;
	}

	public void setLocation_city(String location_city) {
		this.location_city = location_city;
	}

	public String getLocation_state() {
		return location_state;
	}

	public void setLocation_state(String location_state) {
		this.location_state = location_state;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSite_detail_url() {
		return site_detail_url;
	}

	public void setSite_detail_url(String site_detail_url) {
		this.site_detail_url = site_detail_url;
	}

	public VolumeResponse[] getVolumes() {
		return volumes;
	}

	public void setVolumes(VolumeResponse[] volumes) {
		this.volumes = volumes;
	}

	public String toString() {
		JsonObject jo = new JsonObject();
		jo.addProperty("aliases", aliases);
		jo.addProperty("api_detail_url", api_detail_url);
		jo.addProperty("deck", deck);
		jo.addProperty("description", description);
		jo.addProperty("id", id);
		if (image != null) 
			jo.addProperty("image", image.toString());
		else
			jo.addProperty("image", (String)null);
		jo.addProperty("location_city", location_city);
		jo.addProperty("location_state", location_state);
		jo.addProperty("name", name);
		jo.addProperty("site_detail_url", site_detail_url);
		JsonArray ja = new JsonArray();
		if (volumes != null)
			for (int i=0; i<volumes.length; i++) {
				ja.add(volumes[i].toString());
			}
		jo.add("volumes", ja);
		return new GsonBuilder().serializeNulls().create().toJson(jo);	
	}
	
	public boolean equals(Object o) {
		return this.id == ((PublisherResponse)o).getId();
	}

	public Publisher getPublisher() {
		Publisher p = new Publisher();
		p.setAliases(aliases);
		p.setApi_detail_url(api_detail_url);
		p.setDeck(deck);
		p.setDescription(description);
		p.setId(id);
		p.setImage(image);
		p.setLocation_city(location_city);
		p.setLocation_state(location_state);
		p.setName(name);
		p.setSite_detail_url(site_detail_url);
		Volume[] pVolumes = null;
		if (volumes != null) {
			pVolumes = new Volume[volumes.length];
			for (int i=0; i<volumes.length; i++) {
				pVolumes[i] = volumes[i].getVolume();
			}
		}
		p.setVolumes(pVolumes);
		return p;
	}
	
}
