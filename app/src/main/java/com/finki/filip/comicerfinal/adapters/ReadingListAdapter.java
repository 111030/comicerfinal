package com.finki.filip.comicerfinal.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.finki.filip.comicerfinal.R;
import com.finki.filip.comicerfinal.asyncs.IssueRatingsPopulator;
import com.finki.filip.comicerfinal.eventlisteners.ReadComicListener;
import com.finki.filip.comicerfinal.gson.comicvine.Issue;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Filip on 22.1.2017.
 */

public class ReadingListAdapter extends BaseAdapter{

    private Context mContext;
    private LayoutInflater layoutInflater;

    private List<Issue> issues;

    public ReadingListAdapter(Context context) {
        mContext = context;
        layoutInflater = LayoutInflater.from(context);
        issues = new ArrayList<Issue>();
    }

    static class ReadingListHolder {
        ImageView issueImg;
        TextView issueFullName;
        LinearLayout issueRating;
        TextView ratingCount;
        Button read;
    }

    public void addAll(List<Issue> issues) {
        this.issues.addAll(issues);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return issues.size();
    }

    @Override
    public Object getItem(int i) {
        return issues.get(i);
    }

    @Override
    public long getItemId(int i) {
        return issues.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ReadingListHolder holder;
        final int index = i;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.reading_list_item, null);
            holder = new ReadingListHolder();
            holder.issueImg = (ImageView)view.findViewById(R.id.reading_list_item_image);
            holder.issueFullName = (TextView)view.findViewById(R.id.reading_list_item_full_name);
            holder.issueRating = (LinearLayout) view.findViewById(R.id.reading_list_item_rating);
            holder.ratingCount = (TextView)view.findViewById(R.id.reading_list_item_ratings_count);
            holder.read = (Button)view.findViewById(R.id.reading_list_item_read);
            view.setTag(holder);
        }
        else {
            holder = (ReadingListHolder) view.getTag();
        }
        Glide.with(mContext).load(issues.get(i).getImage().getMedium_url()).fitCenter().
                into(holder.issueImg);
        //
        String volumeName = issues.get(i).getVolume() != null ?
                issues.get(i).getVolume().getName() : issues.get(i).getName();
        DateFormat dateFormatFrom = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String lastYear = "";
        try {
            Date d = dateFormatFrom.parse(issues.get(i).getDate_last_updated());
            DateFormat dateFormatTo = new SimpleDateFormat("yyyy-MM-dd");
            lastYear = dateFormatTo.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String issueNumber = issues.get(i).getIssue_number();
        holder.issueFullName.setText(volumeName + " (" + lastYear + ") - #" + issueNumber);
        //
        int rating = issues.get(i).getUser_review() != 0 ?
                (int)issues.get(i).getUser_review() : 0;
        holder.issueRating.setDividerPadding(16);
        holder.issueRating.removeAllViews();
        for (int j=0; j<rating; j++) {
            ImageButton ratingStar = new ImageButton(mContext);
            ratingStar.setScaleType(ImageView.ScaleType.FIT_XY);
            ratingStar.setImageResource(R.drawable.ic_star_white_24dp);
            ratingStar.setBackgroundColor(Color.TRANSPARENT);
            holder.issueRating.addView(ratingStar, j);
        }
        for (int j=rating; j<5; j++) {
            ImageButton ratingStar = new ImageButton(mContext);
            ratingStar.setScaleType(ImageView.ScaleType.FIT_XY);
            ratingStar.setImageResource(R.drawable.ic_star_border_white_24dp);
            ratingStar.setBackgroundColor(Color.TRANSPARENT);
            holder.issueRating.addView(ratingStar, j);
        }
        //
        new IssueRatingsPopulator(holder.ratingCount).execute(issues.get(i));
        //
        holder.read.setOnClickListener(new ReadComicListener(mContext, issues.get(i).getId()));

        return view;
    }

}
