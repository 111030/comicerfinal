package com.finki.filip.comicerfinal.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.finki.filip.comicerfinal.R;
import com.finki.filip.comicerfinal.gson.comicvine.Volume;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Filip on 06.2.2017.
 */

public class VolumesAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    private List<Volume> volumes;

    public VolumesAdapter(Context context) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);
        volumes = new ArrayList<>();
    }

    public void addAll(List<Volume> volumes) {
        this.volumes.addAll(volumes);
        notifyDataSetChanged();
    }

    public void add(Volume volume) {
        volumes.add(volume);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return volumes.size();
    }

    @Override
    public Object getItem(int i) {
        return volumes.get(i);
    }

    @Override
    public long getItemId(int i) {
        return volumes.get(i).getId();
    }

    static class VolumeHolder {
        ImageView img;
        TextView name;
        TextView startYear;
        TextView firstIssue;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        VolumeHolder holder;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.volume_grid_item, null);
            holder = new VolumeHolder();
            holder.img = (ImageView)view.findViewById(R.id.volume_grid_img);
            holder.name = (TextView)view.findViewById(R.id.volume_grid_name);
            holder.startYear = (TextView)view.findViewById(R.id.volume_grid_first_year);
            holder.firstIssue = (TextView)view.findViewById(R.id.volume_grid_first_issue);
            view.setTag(holder);
        }
        else {
            holder = (VolumeHolder) view.getTag();
        }
        Volume current = volumes.get(i);
        Glide.with(context).load(current.getImage().getThumb_url()).fitCenter()
                .into(holder.img);
        holder.name.setText(current.getName());
        holder.startYear.setText(current.getStart_year());
        holder.firstIssue.setText(current.getFirst_issue().getName());
        return view;
    }
}
