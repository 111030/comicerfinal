package com.finki.filip.comicerfinal;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.finki.filip.comicerfinal.asyncs.CheckBeforeRegister;
import com.finki.filip.comicerfinal.eventlisteners.ToggleViewListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Filip on 24.12.2016.
 */

public class RegisterScreen extends Activity {

    public EditText firstName;
    public EditText surname;
    public EditText email;
    public EditText username;
    public EditText password;
    public EditText dateOfBirth;
    public Spinner gender;
    public Spinner country;
    public EditText postalCode;
    public EditText phoneNumber;
    public EditText address;
    public EditText creditCard;

    //
    TextView firstNameError;
    TextView surnameError;
    TextView emailError;
    TextView usernameErrorEmpty;
    TextView usernameErrorAlreadyExists;
    TextView passwordError;
    //
    ImageView closeButton;
    Button saveButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_modal);

        //
        firstName = (EditText)findViewById(R.id.dialog_signup_first_name);
        surname = (EditText)findViewById(R.id.dialog_signup_surname);
        email = (EditText)findViewById(R.id.dialog_signup_email);
        username = (EditText)findViewById(R.id.dialog_signup_username);
        password = (EditText)findViewById(R.id.dialog_signup_password);
        dateOfBirth = (EditText)findViewById(R.id.dialog_signup_dob);
        gender = (Spinner)findViewById(R.id.dialog_signup_gender);
        country = (Spinner)findViewById(R.id.dialog_signup_country);
        postalCode = (EditText)findViewById(R.id.dialog_signup_postal_code);
        phoneNumber = (EditText)findViewById(R.id.dialog_signup_phone_number);
        address = (EditText)findViewById(R.id.dialog_signup_address);
        creditCard = (EditText)findViewById(R.id.dialog_signup_credit_card);
        //
        firstNameError = (TextView)findViewById(R.id.dialog_signup_first_name_error);
        surnameError = (TextView)findViewById(R.id.dialog_signup_surname_error);
        emailError = (TextView)findViewById(R.id.dialog_signup_email_error);
        usernameErrorEmpty = (TextView)findViewById(R.id.dialog_signup_username_empty_error);
        usernameErrorAlreadyExists = (TextView)findViewById(R.id.dialog_signup_username_exists_error);
        passwordError = (TextView)findViewById(R.id.dialog_signup_password_error);
        //
        firstName.addTextChangedListener(new ToggleViewListener(firstNameError));
        surname.addTextChangedListener(new ToggleViewListener(surnameError));
        email.addTextChangedListener(new ToggleViewListener(emailError));
        username.addTextChangedListener(new ToggleViewListener(usernameErrorEmpty));
        password.addTextChangedListener(new ToggleViewListener(passwordError));
        initializeGenderSpinner();
        initializeCountriesSpinner();


        Button showAdditional = (Button)findViewById(R.id.dialog_signup_additional);
        final LinearLayout ll = (LinearLayout)findViewById(R.id.dialog_signup_additional_fields);
        //
        showAdditional.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ll.getVisibility() == View.GONE) {
                    ll.setVisibility(View.VISIBLE);
                }
                else {
                    ll.setVisibility(View.GONE);
                }
            }
        });


        //
        closeButton = (ImageView)findViewById(R.id.dialog_close);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        //
        saveButton = (Button)findViewById(R.id.dialog_save);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View invalid = getNonValidArea();
                if (invalid != null)
                    invalid.requestFocus();
                else {
                    new CheckBeforeRegister(RegisterScreen.this, usernameErrorAlreadyExists)
                            .execute(username.getText().toString(), email.getText().toString());
                }

            }
        });

    }

    private void initializeGenderSpinner() {
        gender.setAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item,
                new String[]{"Male", "Female"}));
    }

    private void initializeCountriesSpinner() {
        String locales[] = Locale.getISOCountries();
        List<String> countries = new ArrayList<String>();
        for (String l : locales) {
            countries.add(new Locale(Locale.US.getLanguage(), l).getDisplayCountry(Locale.US));
        }
        country.setAdapter(new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item,
                countries));
    }

    private View getNonValidArea() {
        if (firstNameError.getVisibility() == View.VISIBLE)
            return firstName;
        if (surnameError.getVisibility() == View.VISIBLE)
            return surname;
        if (emailError.getVisibility() == View.VISIBLE)
            return email;
        if (usernameErrorEmpty.getVisibility() == View.VISIBLE)
            return username;
        if (passwordError.getVisibility() == View.VISIBLE)
            return password;
        return null;
    }
}
