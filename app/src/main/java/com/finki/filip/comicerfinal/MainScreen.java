package com.finki.filip.comicerfinal;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.support.annotation.Dimension;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;

import com.finki.filip.comicerfinal.asyncs.NavDrawerUser;
import com.finki.filip.comicerfinal.eventlisteners.ComicsTabListener;
import com.finki.filip.comicerfinal.fragments.NavDrawerFragment;
import com.finki.filip.comicerfinal.fragments.TempEmptyDrawer;
import com.finki.filip.comicerfinal.holders.MainScreenIssuesAdapters;
import com.finki.filip.comicerfinal.singletons.MongoSingleton;
import com.finki.filip.comicerfinal.singletons.UserSingleton;

import java.util.HashMap;
import java.util.Map;

public class MainScreen extends DropboxActivity {

    private MongoSingleton mongo;
    //
    private Button marvelTab;
    private Button dcTab;
    private Button othersTab;
    private ImageButton toggleDrawerButton;
    private TextView shoppingCartCounter;
    private TextView readingListCounter;

    //
    public static final int MARVEL_COMICS = 0;
    public static final int DC_COMICS = 1;
    public static final int OTHER_COMICS = 2;
    //
    private MainScreenIssuesAdapters[] adapters;
    //
    private Map<Integer, Integer> tabsScreens;
    private ImageButton shoppingCart;
    private ImageButton readingList;
    //
    private boolean drawerShow = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);
        //
        mongo = MongoSingleton.getInstance();
        //
        adapters = new MainScreenIssuesAdapters[3];
        //
        toggleDrawerButton = (ImageButton)findViewById(R.id.appbar_drawer_button);
        toggleDrawerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleNavigationDrawer();
            }
        });
        //
        marvelTab = (Button)findViewById(R.id.main_screen_tab_marvel);
        marvelTab.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.tab_focuser);
        marvelTab.setTextSize(Dimension.SP, marvelTab.getTextSize()*1.2f);
        dcTab = (Button)findViewById(R.id.main_screen_tab_dc);
        othersTab = (Button)findViewById(R.id.main_screen_tab_other);
        shoppingCartCounter = (TextView)findViewById(R.id.appbar_shopping_cart_counter);
        readingListCounter = (TextView)findViewById(R.id.appbar_reading_list_counter);
        //setCurrentSignedUser();
        //
        tabsScreens = new HashMap<>();
        tabsScreens.put(R.id.main_screen_tab_marvel, MARVEL_COMICS);
        tabsScreens.put(R.id.main_screen_tab_dc, DC_COMICS);
        tabsScreens.put(R.id.main_screen_tab_other, OTHER_COMICS);
        LinearLayout ll = (LinearLayout)findViewById(R.id.main_screen_tabs);
        ComicsTabListener ctl = new ComicsTabListener(this, ll, tabsScreens, getFragmentManager(), adapters);
        marvelTab.setOnClickListener(ctl);
        dcTab.setOnClickListener(ctl);
        othersTab.setOnClickListener(ctl);
        shoppingCart = (ImageButton)findViewById(R.id.appbar_shopping_cart);
        shoppingCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainScreen.this, ShoppingCartScreen.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                i.putExtra("User", UserSingleton.getInstance().getUser() != null ?
                        UserSingleton.getInstance().getUser().getUsername() : "anonymous");
                startActivity(i);
            }
        });
        readingList = (ImageButton)findViewById(R.id.appbar_reading_list);
        readingList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainScreen.this, ReadingListScreen.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                i.putExtra("User", UserSingleton.getInstance().getUser() != null ?
                        UserSingleton.getInstance().getUser().getUsername() : "anonymous");
                startActivity(i);
            }
        });
        //
        SearchManager searchManager = (SearchManager)getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView)findViewById(R.id.appbar_search_view);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                Intent intent1 = new Intent(MainScreen.this, SearchResultsActivity.class);
                intent1.putExtra("query", s);
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent1);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
        //
        marvelTab.callOnClick();
        getFragmentManager().beginTransaction()
                .add(R.id.nav_drawer_fragment, TempEmptyDrawer.newInstance())
                .commit();
        //
    }

    @Override
    protected void onResume() {
        super.onResume();
        setCurrentSignedUser();
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
            drawerShow = false;
        }
    }

    @Override
    protected void loadData() {

    }

    @Override
    protected void onDestroy() {
        MongoSingleton.getInstance().getClient().close();
        super.onDestroy();
    }

    private void setCurrentSignedUser() {
            WifiManager wm = (WifiManager)getApplicationContext().getSystemService(WIFI_SERVICE);
            String macAddress = wm.getConnectionInfo().getMacAddress();
            NavDrawerUser getCurrentUser = new NavDrawerUser(shoppingCartCounter, readingListCounter);
            getCurrentUser.execute(macAddress);
    }

    private void toggleNavigationDrawer() {
        if (drawerShow) {
            getFragmentManager().popBackStack();
            drawerShow = false;
            return;
        }
        drawerShow = true;
        getFragmentManager().beginTransaction()
                .setCustomAnimations(R.animator.drawer_to_right, R.animator.drawer_to_left,
                        R.animator.drawer_to_right, R.animator.drawer_to_left)
                .replace(R.id.nav_drawer_fragment, NavDrawerFragment.newInstance())
                .addToBackStack(null)
                .commit();

    }


    public MainScreenIssuesAdapters[] getAdapters() {
        return adapters;
    }

}
