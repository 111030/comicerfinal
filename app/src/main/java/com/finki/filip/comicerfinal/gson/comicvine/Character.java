package com.finki.filip.comicerfinal.gson.comicvine;

import java.io.Serializable;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class Character extends Resource implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8318588751978178533L;
	private String aliases;
	private String api_detail_url;
	private String birth;
	private int count_of_issue_appearances;
	private Character[] character_enemies;
	private Character[] character_friends;
	private Person[] creators;
	private String deck;
	private String description;
	private Issue first_appeared_in_issue;
	private int _id;
	private Image image;
	private Issue[] issues_died_in;
	private Movie[] movies;
	private String name;
	private Power[] powers;
	private Publisher publisher;
	private String real_name;
	private String site_detail_url;
	private Team[] teams;
	private Volume[] volume_credits;
	
	public Character() {
		super();
		character_enemies = new Character[]{};
		character_friends = new Character[]{};
		creators = new Person[]{};
		issues_died_in = new Issue[]{};
		movies = new Movie[]{};
		powers = new Power[]{};
		teams = new Team[]{};
		volume_credits = new Volume[]{};
	}

	public Character(String aliases, String api_detail_url, String birth, int count_of_issue_appearances, Character[] character_enemies,
			Character[] character_friends, Person[] creators, String deck, String description,
			Issue first_appeared_in_issue, int _id, Image image, Issue[] issues_died_in, Movie[] movies, String name,
			Power[] powers, Publisher publisher, String real_name, String site_detail_url, Team[] teams, Volume[] volume_credits) {
		super();
		this.aliases = aliases;
		this.api_detail_url = api_detail_url;
		this.birth = birth;
		this.count_of_issue_appearances = count_of_issue_appearances;
		this.character_enemies = character_enemies;
		this.character_friends = character_friends;
		this.creators = creators;
		this.deck = deck;
		this.description = description;
		this.first_appeared_in_issue = first_appeared_in_issue;
		this._id = _id;
		this.image = image;
		this.issues_died_in = issues_died_in;
		this.movies = movies;
		this.name = name;
		this.powers = powers;
		this.publisher = publisher;
		this.real_name = real_name;
		this.site_detail_url = site_detail_url;
		this.teams = teams;
		this.volume_credits = volume_credits;
	}

	public String getAliases() {
		return aliases;
	}

	public void setAliases(String aliases) {
		this.aliases = aliases;
	}

	public String getApi_detail_url() {
		return api_detail_url;
	}

	public void setApi_detail_url(String api_detail_url) {
		this.api_detail_url = api_detail_url;
	}

	public String getBirth() {
		return birth;
	}

	public void setBirth(String birth) {
		this.birth = birth;
	}

	public int getCount_of_issue_appearances() {
		return count_of_issue_appearances;
	}

	public void setCount_of_issue_appearances(int count_of_issue_appearances) {
		this.count_of_issue_appearances = count_of_issue_appearances;
	}

	public Character[] getCharacter_enemies() {
		return character_enemies;
	}

	public void setCharacter_enemies(Character[] character_enemies) {
		this.character_enemies = character_enemies;
	}

	public Character[] getCharacter_friends() {
		return character_friends;
	}

	public void setCharacter_friends(Character[] character_friends) {
		this.character_friends = character_friends;
	}

	public Person[] getCreators() {
		return creators;
	}

	public void setCreators(Person[] creators) {
		this.creators = creators;
	}
	
	

	public String getDeck() {
		return deck;
	}

	public void setDeck(String deck) {
		this.deck = deck;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Issue getFirst_appeared_in_issue() {
		return first_appeared_in_issue;
	}

	public void setFirst_appeared_in_issue(Issue first_appeared_in_issue) {
		this.first_appeared_in_issue = first_appeared_in_issue;
	}

	public int getId() {
		return _id;
	}

	public void setId(int _id) {
		this._id = _id;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}
	
	
	
	public Issue[] getIssues_died_in() {
		return issues_died_in;
	}

	public void setIssues_died_in(Issue[] issues_died_in) {
		this.issues_died_in = issues_died_in;
	}

	public Movie[] getMovies() {
		return movies;
	}

	public void setMovies(Movie[] movies) {
		this.movies = movies;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Power[] getPowers() {
		return powers;
	}

	public void setPowers(Power[] powers) {
		this.powers = powers;
	}

	public Publisher getPublisher() {
		return publisher;
	}

	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}

	public String getReal_name() {
		return real_name;
	}

	public void setReal_name(String real_name) {
		this.real_name = real_name;
	}
	
	
	
	public String getSite_detail_url() {
		return site_detail_url;
	}

	public void setSite_detail_url(String site_detail_url) {
		this.site_detail_url = site_detail_url;
	}

	public Team[] getTeams() {
		return teams;
	}

	public void setTeams(Team[] teams) {
		this.teams = teams;
	}

	public Volume[] getVolume_credits() {
		return volume_credits;
	}

	public void setVolume_credits(Volume[] volume_credits) {
		this.volume_credits = volume_credits;
	}

	public String toString() {
		Gson gson = new GsonBuilder().serializeNulls().create();
		JsonObject jo = new JsonObject();
		jo.addProperty("aliases", aliases);
		jo.addProperty("api_detail_url", api_detail_url);
		jo.addProperty("birth", birth);
		jo.addProperty("count_of_issue_appearances", count_of_issue_appearances);
		JsonArray ja = new JsonArray();
		if (character_enemies != null)
			for (int i=0; i<character_enemies.length; i++) {
				ja.add(character_enemies[i].toString());
			}
		jo.add("character_enemies", ja);
		ja = new JsonArray();
		if (character_friends != null)
			for (int i=0; i<character_friends.length; i++) {
				ja.add(character_friends[i].toString());
			}
		jo.add("character_friends", ja);
		ja = new JsonArray();
		if (creators != null)
			for (int i=0; i<creators.length; i++) {
				ja.add(creators[i].toString());
			}
		jo.add("creators", ja);
		jo.addProperty("deck", deck);
		jo.addProperty("description", description);
		if (first_appeared_in_issue != null)
			jo.addProperty("first_appeared_in_issue", first_appeared_in_issue.toString());
		else
			jo.addProperty("first_appeared_in_issue", (String)null);
		jo.addProperty("_id", _id);
		if (image != null)
			jo.addProperty("image", image.toString());
		else
			jo.addProperty("image", (String)null);
		ja = new JsonArray();
		if (issues_died_in != null)
			for (int i=0; i<issues_died_in.length; i++) {
				ja.add(issues_died_in[i].toString());
			}
		jo.add("issues_died_in", ja);
		ja = new JsonArray();
		if (movies != null)
			for (int i=0; i<movies.length; i++) {
				ja.add(movies[i].toString());
			}
		jo.add("movies", ja);
		jo.addProperty("name", name);
		ja = new JsonArray();
		if (powers != null) {
			for (int i=0; i<powers.length; i++) {
				ja.add(gson.toJson(powers[i], Power.class));
			}
		}
		jo.add("powers", ja);
		if (publisher != null)
			jo.addProperty("publisher", publisher.toString());
		else
			jo.addProperty("publisher", (String)null);
		jo.addProperty("real_name", real_name);
		jo.addProperty("site_detail_url", site_detail_url);
		ja = new JsonArray();
		if (teams != null)
			for (int i=0; i<teams.length; i++) {
				ja.add(teams[i].toString());
			}
		jo.add("teams", ja);
		ja = new JsonArray();
		if (volume_credits != null)
			for (int i=0; i<volume_credits.length; i++) {
				ja.add(volume_credits[i].toString());
			}
		jo.add("volume_credits", ja);
		return new GsonBuilder().serializeNulls().create().toJson(jo);
	}
	
	public boolean equals(Object o) {
		return this._id == ((Character)o).getId();
	}

}
