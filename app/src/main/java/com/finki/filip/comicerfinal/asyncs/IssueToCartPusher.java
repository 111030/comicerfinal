package com.finki.filip.comicerfinal.asyncs;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.gson.comicvine.Issue;
import com.finki.filip.comicerfinal.gson.comicvine.Resource;
import com.finki.filip.comicerfinal.gson.other.User;
import com.finki.filip.comicerfinal.singletons.MongoSingleton;
import com.mongodb.client.MongoDatabase;

/**
 * Created by Filip on 19.1.2017.
 */

public class IssueToCartPusher extends AsyncTask<Resource, Void, Boolean>{

    private Context context;
    private TextView counter;

    public IssueToCartPusher(Context context, TextView counter) {
        this.context = context;
        this.counter = counter;
    }

    @Override
    protected Boolean doInBackground(Resource... resources) {
        User user = (User)resources[0];
        Issue issue = (Issue)resources[1];
        MongoDatabase db = MongoSingleton.getInstance().getDatabase();
        return !MongoDBHelper.issueExistsInReadingList(issue, db)
        && MongoDBHelper.insertIntoShoppingCart(user, issue, db);
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        if (aBoolean) {
            Toast.makeText(context, "Issue succesfully added to shopping cart", Toast.LENGTH_LONG).show();
            int count = Integer.parseInt(counter.getText().toString());
            counter.setText(String.valueOf(++count));
            counter.setVisibility(View.VISIBLE);
        }
        else {
            Toast.makeText(context, "Issue already exists", Toast.LENGTH_LONG).show();
        }
    }
}
