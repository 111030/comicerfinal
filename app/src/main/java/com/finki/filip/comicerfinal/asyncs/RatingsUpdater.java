package com.finki.filip.comicerfinal.asyncs;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.finki.filip.comicerfinal.R;
import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.gson.comicvine.Issue;

import java.util.List;

/**
 * Created by Filip on 29.1.2017.
 */

public class RatingsUpdater extends AsyncTask<Integer, Void, Integer> {

    private Context context;
    private List<ImageView> gradeStars;
    private TextView gradesCount;
    private ImageView[] ratingStars;
    private Issue issue;

    public RatingsUpdater(Context context, List<ImageView> gradeStars, TextView gradesCount,
                          ImageView[] ratingStars, Issue issue) {
        this.context = context;
        this.ratingStars = ratingStars;
        this.gradesCount = gradesCount;
        this.gradeStars = gradeStars;
        this.issue = issue;
    }

    @Override
    protected Integer doInBackground(Integer... integers) {
        return MongoDBHelper.updateRatingsForIssue(integers[0],issue);
    }

    @Override
    protected void onPostExecute(Integer integer) {
        for (int i=0; i<integer; i++) {
            ratingStars[i].setImageResource(R.drawable.ic_star_white_36dp);
        }
        new InfoScreenPopulateUserGrade(context, gradeStars, gradesCount).execute(issue);
    }
}
