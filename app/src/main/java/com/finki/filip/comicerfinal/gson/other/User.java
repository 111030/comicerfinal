package com.finki.filip.comicerfinal.gson.other;

import com.finki.filip.comicerfinal.gson.comicvine.Issue;
import com.finki.filip.comicerfinal.gson.comicvine.Resource;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Filip on 19.12.2016.
 */

public class User extends Resource implements Serializable{

    private String username;
    private String password;
    private String email;
    private String name;
    private String surname;
    private String born;
    private String gender;
    private Country country;
    private String code;
    private String phoneNumber;
    private String address;
    private String credditCard;
    private List<Issue> shopping_cart;
    private List<Issue> favorites;
    private List<Issue> reading_list;
    private float amount_credit_card;

    public User() {
        super();
        shopping_cart = new ArrayList<Issue>();
        favorites = new ArrayList<Issue>();
        reading_list = new ArrayList<Issue>();
    }

    public User(String username, String password, String email, String name, String surname,
                String born, String gender, Country country, String code, String phoneNumber,
                String address, String credditCard,
                List<Issue> shopping_cart, List<Issue> favorites, List<Issue> reading_list,
                float amount_credit_card) {
        super();
        this.username = username;
        this.password = password;
        this.email = email;
        this.name = name;
        this.surname = surname;
        this.born = born;
        this.gender = gender;
        this.country = country;
        this.code = code;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.credditCard = credditCard;
        this.shopping_cart = shopping_cart;
        this.favorites = favorites;
        this.reading_list = reading_list;
        this.amount_credit_card = amount_credit_card;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getBorn() {
        return born;
    }

    public void setBorn(String born) {
        this.born = born;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCredditCard() {
        return credditCard;
    }

    public void setCredditCard(String credditCard) {
        this.credditCard = credditCard;
    }

    public List<Issue> getShopping_cart() {
        return shopping_cart;
    }

    public void setShopping_cart(List<Issue> shopping_cart) {
        this.shopping_cart = shopping_cart;
    }

    public List<Issue> getFavorites() {
        return favorites;
    }

    public void setFavorites(List<Issue> favorites) {
        this.favorites = favorites;
    }

    public List<Issue> getReading_list() {
        return reading_list;
    }

    public void setReading_list(List<Issue> reading_list) {
        this.reading_list = reading_list;
    }

    public float getAmount_credit_card() {
        return amount_credit_card;
    }

    public void setAmount_credit_card(float amount_credit_card) {
        this.amount_credit_card = amount_credit_card;
    }

    public String toString() {
        JsonObject jo = new JsonObject();
        jo.addProperty("username", username);
        jo.addProperty("password", password);
        jo.addProperty("email", email);
        jo.addProperty("name", name);
        jo.addProperty("surname", surname);
        jo.addProperty("born", born);
        jo.addProperty("gender", gender);
        if (country != null)
            jo.addProperty("country", country.toString());
        else
            jo.addProperty("country", (String)null);
        jo.addProperty("code", code);
        jo.addProperty("phoneNumber", phoneNumber);
        jo.addProperty("address", address);
        jo.addProperty("credditCard", credditCard);
        JsonArray ja = new JsonArray();
        if (shopping_cart != null) {
            for (int i=0; i<shopping_cart.size(); i++) {
                ja.add(shopping_cart.get(i).toString());
            }
        }
        jo.add("shopping_cart", ja);
        ja = new JsonArray();
        if (favorites != null) {
            for (int i=0; i<favorites.size(); i++) {
                ja.add(favorites.get(i).toString());
            }
        }
        jo.add("favorites", ja);
        ja = new JsonArray();
        if (reading_list != null) {
            for (int i=0; i<reading_list.size(); i++) {
                ja.add(reading_list.get(i).toString());
            }
        }
        jo.add("reading_list", ja);
        jo.addProperty("amount_credit_card", amount_credit_card);
        return new GsonBuilder().serializeNulls().create().toJson(jo);
    }

    public boolean equals(Object o) {
        User oUser = (User)o;
        return oUser != null && this.getUsername() == oUser.getUsername();
    }
}
