package com.finki.filip.comicerfinal.fragments.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.TextView;

import com.finki.filip.comicerfinal.R;
import com.finki.filip.comicerfinal.ShoppingCartScreen;
import com.finki.filip.comicerfinal.asyncs.IssueToReadingListPusher;
import com.finki.filip.comicerfinal.asyncs.ShoppingCartUpdater;
import com.finki.filip.comicerfinal.gson.comicvine.Issue;
import com.finki.filip.comicerfinal.gson.marvel.ComicPrice;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.List;

/**
 * Created by Filip on 23.1.2017.
 */

public class BuyIssuePromptDialog extends DialogFragment implements DialogInterface.OnClickListener {

    public static BuyIssuePromptDialog newInstance(float total, int itemIndex, List<Issue> issues) {
        BuyIssuePromptDialog bpd = new BuyIssuePromptDialog();
        Bundle b = new Bundle();
        b.putFloat("amount_credit_card", total);
        b.putInt("itemIndex", itemIndex);
        b.putString("issues", new GsonBuilder().serializeNulls().create().toJson(
                issues, new TypeToken<List<Issue>>(){}.getType()));
        float totalSum = 0;
        if (itemIndex == -1) {
            for (Issue issue : issues) {
                List<ComicPrice> prices = issue.getPrices();
                for (int i=0; i<prices.size(); i++) {
                    if (prices.get(i).getType().equalsIgnoreCase("digitalPurchasePrice")) {
                        totalSum += prices.get(i).getPrice();
                        break;
                    }
                    if (i == prices.size() - 1) {
                        totalSum += prices.get(i).getPrice();
                    }
                }
            }
        }
        else {
            List<ComicPrice> prices = issues.get(itemIndex).getPrices();
            for (int i=0; i<prices.size(); i++) {
                if (prices.get(i).getType().equalsIgnoreCase("digitalPurchasePrice")) {
                    totalSum = prices.get(i).getPrice();
                    break;
                }
                if (i == prices.size() - 1) {
                    totalSum = prices.get(i).getPrice();
                }
            }
        }
        b.putFloat("totalSum", totalSum);
        bpd.setArguments(b);
        return bpd;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
        b.setTitle("Buy comics issue");
        b.setMessage(bundle.getFloat("amount_credit_card") < bundle.getFloat("totalSum")
        ? "You have insufficient funds. Check your credit card"
        : "Funds: " + bundle.getFloat("amount_credit_card")+"." + "\n" + "Do you want to proceed?");
        if (bundle.getFloat("amount_credit_card") < bundle.getFloat("totalSum")) {
            b.setNeutralButton("OK", this);
        }
        else {
            b.setPositiveButton("Yes", this);
            b.setNegativeButton("No", this);
        }
        return b.create();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        if (i == AlertDialog.BUTTON_POSITIVE) {
            int itemIndex = getArguments().getInt("itemIndex");
            List<Issue> issues = new GsonBuilder().serializeNulls().create().fromJson(
                    getArguments().getString("issues"), new TypeToken<List<Issue>>(){}.getType()
            );
            ShoppingCartScreen screen = (ShoppingCartScreen)getActivity();
            if (itemIndex >= 0) {
                new IssueToReadingListPusher(screen, (TextView)screen.findViewById(
                        R.id.appbar_reading_list_counter)).execute(issues.get(itemIndex));
                new ShoppingCartUpdater(screen.getAdapter(), screen,
                        itemIndex, screen.getAdapter().getIssues()).execute();
            }
            else {
                Object[] objects = issues.toArray();
                Issue[] arrayIssus = new Issue[objects.length];
                for (int j=0; j<objects.length; j++) {
                    arrayIssus[j] = (Issue)objects[j];
                }
                new IssueToReadingListPusher(screen, (TextView)screen.findViewById(
                        R.id.appbar_reading_list_counter)).execute(arrayIssus);
                new ShoppingCartUpdater(screen.getAdapter(), screen,
                        itemIndex, screen.getAdapter().getIssues()).execute();
            }

        }

    }
}
