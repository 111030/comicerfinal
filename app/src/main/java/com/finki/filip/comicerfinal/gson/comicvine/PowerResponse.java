package com.finki.filip.comicerfinal.gson.comicvine;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.io.Serializable;

/**
 * Created by Filip on 05.2.2017.
 */

public class PowerResponse extends Resource implements Serializable {

    private String api_detail_url;
    private String description;
    private int id;
    private String name;

    public PowerResponse() {}

    public PowerResponse(String api_detail_url, String description, int id, String name) {
        this.api_detail_url = api_detail_url;
        this.description = description;
        this.id = id;
        this.name = name;
    }

    public String getApi_detail_url() {
        return api_detail_url;
    }

    public void setApi_detail_url(String api_detail_url) {
        this.api_detail_url = api_detail_url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int get_id() {
        return id;
    }

    public void set_id(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        Gson gson = new GsonBuilder().serializeNulls().create();
        JsonObject jo = new JsonObject();
        jo.addProperty("api_detail_url", api_detail_url);
        jo.addProperty("description", description);
        jo.addProperty("id", id);
        jo.addProperty("name", name);
        return gson.toJson(jo);
    }

    public Power getPower() {
        Power p = new Power();
        p.setApi_detail_url(api_detail_url);
        p.setDescription(description);
        p.set_id(id);
        p.setName(name);
        return p;
    }
}
