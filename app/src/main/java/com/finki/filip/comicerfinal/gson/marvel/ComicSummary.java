package com.finki.filip.comicerfinal.gson.marvel;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class ComicSummary {
	
	private String resourceURI;
	private String name;
	
	public ComicSummary() {
		super();
	}
	
	

	public ComicSummary(String resourceURI, String name) {
		super();
		this.resourceURI = resourceURI;
		this.name = name;
	}



	public String getResourceURI() {
		return resourceURI;
	}

	public void setResourceURI(String resourceURI) {
		this.resourceURI = resourceURI;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



	@Override
	public String toString() {
		JsonObject jo = new JsonObject();
		jo.addProperty("resourceURI", resourceURI);
		jo.addProperty("name", name);
		return new GsonBuilder().serializeNulls().create().toJson(jo);
	}
	
	

}
