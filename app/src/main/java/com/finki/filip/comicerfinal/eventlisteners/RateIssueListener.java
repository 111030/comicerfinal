package com.finki.filip.comicerfinal.eventlisteners;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.finki.filip.comicerfinal.asyncs.RatingsUpdater;
import com.finki.filip.comicerfinal.gson.comicvine.Issue;

import java.util.List;

/**
 * Created by Filip on 28.1.2017.
 */

public class RateIssueListener implements View.OnClickListener {

    private Context context;
    private List<ImageView> gradeStars;
    private TextView gradesCount;
    private ImageView[] ratingStars;
    private int gradeStarIndex;
    private Issue issue;

    public RateIssueListener(int gradeStarIndex, Context context, List<ImageView> gradeStars,
                             TextView gradesCount, ImageView[] ratingStars, Issue issue) {
        this.gradeStarIndex = gradeStarIndex;
        this.context = context;
        this.gradeStars = gradeStars;
        this.gradesCount = gradesCount;
        this.ratingStars = ratingStars;
        this.issue = issue;
    }

    @Override
    public void onClick(View view) {
        new RatingsUpdater(context,gradeStars,gradesCount,ratingStars,issue)
                .execute(gradeStarIndex);
    }
}
