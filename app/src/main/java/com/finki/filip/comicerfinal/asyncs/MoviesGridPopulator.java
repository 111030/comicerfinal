package com.finki.filip.comicerfinal.asyncs;

import android.os.AsyncTask;

import com.finki.filip.comicerfinal.adapters.MoviesAdapter;
import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.gson.comicvine.Movie;

import java.util.List;

/**
 * Created by Filip on 07.2.2017.
 */

public class MoviesGridPopulator extends AsyncTask<Void,Void,List<Movie>> {

    private MoviesAdapter adapter;

    public MoviesGridPopulator(MoviesAdapter adapter) {
        this.adapter = adapter;
    }

    @Override
    protected List<Movie> doInBackground(Void... voids) {
        return MongoDBHelper.getMovies();
    }

    @Override
    protected void onPostExecute(List<Movie> movies) {
        adapter.addAll(movies);
    }
}
