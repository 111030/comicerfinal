package com.finki.filip.comicerfinal.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import com.finki.filip.comicerfinal.MainScreen;
import com.finki.filip.comicerfinal.R;
import com.finki.filip.comicerfinal.eventlisteners.IssuesGridListener;
import com.finki.filip.comicerfinal.holders.MainScreenIssuesAdapters;

/**
 * Created by Filip on 11.1.2017.
 */

public class MainScreenPublisherComics extends Fragment {

    private int mIndex = 0;

    public static MainScreenPublisherComics newInstance(int index) {
        MainScreenPublisherComics mspc = new MainScreenPublisherComics();
        Bundle args = new Bundle();
        args.putInt("index", index);
        mspc.setArguments(args);
        return mspc;
    }

    public static MainScreenPublisherComics newInstance(Bundle bundle) {
        int index = bundle.getInt("index", 0);
        return newInstance(index);
    }

    public void onCreate(Bundle myBundle) {
        super.onCreate(myBundle);
        mIndex = getArguments().getInt("index", 0);
    }

    public int getShownIndex() {
        return mIndex;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.main_screen_comic_grids, container, false);
        MainScreen callingActivity = (MainScreen)getActivity();
        GridView newIssuesGrid = (GridView)v.findViewById(R.id.comic_grid_recent);
        GridView freeIssuesGrid = (GridView)v.findViewById(R.id.comic_grid_free);
        GridView popularIssuesGrid = (GridView)v.findViewById(R.id.comic_grid_popular);
        GridView bestRatedIssuesGrid = (GridView)v.findViewById(R.id.comic_grid_best_rated);
        TextView newIssuesText = (TextView)v.findViewById(R.id.comic_grid_recent_text);
        TextView freeIssuesText = (TextView)v.findViewById(R.id.comic_grid_free_text);
        TextView popularIssuesText = (TextView)v.findViewById(R.id.comic_grid_popular_text);
        TextView bestRatedText = (TextView)v.findViewById(R.id.comic_grid_best_rated_text);
        MainScreenIssuesAdapters thisAdapter = callingActivity.getAdapters()[mIndex];
        if (thisAdapter != null) {
            newIssuesGrid.setAdapter(thisAdapter.getNewIssuesAdapter());
            freeIssuesGrid.setAdapter(thisAdapter.getFreeIssuesAdapter());
            popularIssuesGrid.setAdapter(thisAdapter.getPopularIssuesAdapter());
            bestRatedIssuesGrid.setAdapter(thisAdapter.getBestRatedIssuesAdapter());
            IssuesGridListener igl = new IssuesGridListener(callingActivity);
            newIssuesGrid.setOnItemClickListener(igl);
            freeIssuesGrid.setOnItemClickListener(igl);
            popularIssuesGrid.setOnItemClickListener(igl);
            bestRatedIssuesGrid.setOnItemClickListener(igl);
            newIssuesText.setVisibility(View.VISIBLE);
            freeIssuesText.setVisibility(View.VISIBLE);
            popularIssuesText.setVisibility(View.VISIBLE);
            bestRatedText.setVisibility(View.VISIBLE);

        }

        return v;
    }
}
