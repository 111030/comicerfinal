package com.finki.filip.comicerfinal.adapters;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.finki.filip.comicerfinal.R;
import com.finki.filip.comicerfinal.eventlisteners.FavoritesOptionClickListener;
import com.finki.filip.comicerfinal.eventlisteners.FavoritesOptionsListener;
import com.finki.filip.comicerfinal.gson.comicvine.Issue;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Filip on 21.1.2017.
 */

public class FavoritesAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater layoutInflater;

    private List<Issue> issues;
    private TextView shoppingCartCounter;

    public FavoritesAdapter(Context context, TextView shoppingCartCounter) {
        mContext = context;
        layoutInflater = LayoutInflater.from(context);
        issues = new ArrayList<Issue>();
        this.shoppingCartCounter = shoppingCartCounter;
    }

    static class FavoritesHolder {
        ImageView issueImg;
        TextView issueFullName;
        ImageButton options;
    }

    public void addAll(List<Issue> issues) {
        this.issues.addAll(issues);
        notifyDataSetChanged();
    }

    public Issue removeIssue(int i) {
        Issue issue = issues.remove(i);
        notifyDataSetChanged();
        return issue;
    }

    public List<Issue> getIssues() {
        return issues;
    }

    @Override
    public int getCount() {
        return issues.size();
    }

    @Override
    public Object getItem(int i) {
        return issues.get(i);
    }

    @Override
    public long getItemId(int i) {
        return issues.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        FavoritesHolder holder;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.favorites_item, null);
            holder = new FavoritesHolder();
            holder.issueImg = (ImageView)view.findViewById(R.id.favorites_item_image);
            holder.issueFullName = (TextView)view.findViewById(R.id.favorites_item_title);
            holder.options = (ImageButton)view.findViewById(R.id.favorites_item_menu);
            view.setTag(holder);
        }
        else {
            holder = (FavoritesHolder) view.getTag();
        }
        Glide.with(mContext).load(issues.get(i).getImage().getThumb_url()).fitCenter().
                into(holder.issueImg);
        //
        String volumeName = issues.get(i).getVolume() != null ?
                issues.get(i).getVolume().getName() : issues.get(i).getName();
        DateFormat dateFormatFrom = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String lastYear = "";
        try {
            Date d = dateFormatFrom.parse(issues.get(i).getDate_last_updated());
            DateFormat dateFormatTo = new SimpleDateFormat("yyyy-MM-dd");
            lastYear = dateFormatTo.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String issueNumber = issues.get(i).getIssue_number();
        holder.issueFullName.setText(volumeName + " (" + lastYear + ") - #" + issueNumber);
        //
        PopupMenu popup = new PopupMenu(mContext, holder.options, Gravity.END);
        popup.getMenuInflater().inflate(R.menu.favorite_item_options, popup.getMenu());
        popup.dismiss();
        holder.options.setOnClickListener(new FavoritesOptionsListener(popup));
        popup.setOnMenuItemClickListener(new FavoritesOptionClickListener(mContext, this, i,
                shoppingCartCounter));
        //

        return view;
    }

}
