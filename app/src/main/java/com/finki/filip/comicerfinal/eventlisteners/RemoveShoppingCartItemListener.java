package com.finki.filip.comicerfinal.eventlisteners;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.finki.filip.comicerfinal.adapters.ShoppingCartAdapter;
import com.finki.filip.comicerfinal.asyncs.ShoppingCartUpdater;
import com.finki.filip.comicerfinal.gson.comicvine.Issue;

import java.util.List;

/**
 * Created by Filip on 21.1.2017.
 */

public class RemoveShoppingCartItemListener implements View.OnClickListener {

    private ShoppingCartAdapter adapter;
    private Context context;
    private int removeIndex;
    private List<Issue> issues;

    public RemoveShoppingCartItemListener(
            ShoppingCartAdapter adapter, Context context, int removeIndex, List<Issue> issues) {
        this.adapter = adapter;
        this.context = context;
        this.removeIndex = removeIndex;
        this.issues = issues;

    }

    @Override
    public void onClick(View view) {
        new ShoppingCartUpdater(adapter, context, removeIndex, issues).execute();
    }
}
