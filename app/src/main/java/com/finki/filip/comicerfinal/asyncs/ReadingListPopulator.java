package com.finki.filip.comicerfinal.asyncs;

import android.os.AsyncTask;

import com.finki.filip.comicerfinal.adapters.ReadingListAdapter;
import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.gson.comicvine.Issue;
import com.finki.filip.comicerfinal.gson.other.User;
import com.finki.filip.comicerfinal.singletons.MongoSingleton;

import java.util.List;

/**
 * Created by Filip on 22.1.2017.
 */

public class ReadingListPopulator extends AsyncTask<User, Void, List<Issue>> {

    private ReadingListAdapter readingListAdapter;

    public ReadingListPopulator(ReadingListAdapter readingListAdapter) {
        this.readingListAdapter = readingListAdapter;
    }

    @Override
    protected List<Issue> doInBackground(User... users) {
        return MongoDBHelper.getReadingListItemsList(users[0], MongoSingleton.getInstance().getDatabase());
    }

    @Override
    protected void onPostExecute(List<Issue> issues) {
        if (issues != null) {
            readingListAdapter.addAll(issues);
        }
    }
}