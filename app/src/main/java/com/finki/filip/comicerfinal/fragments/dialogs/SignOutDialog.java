package com.finki.filip.comicerfinal.fragments.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import com.finki.filip.comicerfinal.asyncs.SignOut;
import com.finki.filip.comicerfinal.gson.other.User;

/**
 * Created by Filip on 18.2.2017.
 */

public class SignOutDialog extends DialogFragment implements DialogInterface.OnClickListener {

    public SignOutDialog newInstance(User user) {
        SignOutDialog sod = new SignOutDialog();
        Bundle b = new Bundle();
        b.putSerializable("user", user);
        sod.setArguments(b);
        return sod;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Sign out");
        builder.setMessage("Are you sure you want to leave?");
        builder.setPositiveButton("No", this);
        builder.setNegativeButton("Yes", this);
        return builder.create();

    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        if (i == AlertDialog.BUTTON_NEGATIVE) {
            new SignOut(getActivity(), (User)getArguments().getSerializable("user")).execute();
        }
    }
}
