package com.finki.filip.comicerfinal.asyncs;

import android.os.AsyncTask;
import android.view.View;
import android.widget.TextView;

import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.gson.other.User;
import com.finki.filip.comicerfinal.singletons.MongoSingleton;

/**
 * Created by Filip on 23.1.2017.
 */

public class ReadingListSubscriber extends AsyncTask<User, Void, Integer> {

    private TextView readingListCounter;

    public ReadingListSubscriber(TextView readingListCounter) {
        this.readingListCounter = readingListCounter;
    }


    @Override
    protected Integer doInBackground(User... users) {
        return MongoDBHelper.getReadingListItems(
                users[0], MongoSingleton.getInstance().getDatabase());

    }

    @Override
    protected void onPostExecute(Integer integer) {
        readingListCounter.setText(String.valueOf(integer));
        if (integer > 0)
            readingListCounter.setVisibility(View.VISIBLE);
        else
            readingListCounter.setVisibility(View.INVISIBLE);
    }
}