package com.finki.filip.comicerfinal.gson.comicvine;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.Serializable;

public class PersonResponse extends Resource implements Serializable{

	/**
	 * 
	 */
	private String aliases;
	private String api_detail_url;
	private String country;
	private CharacterResponse[] created_characters;
	private String deck;
	private String description;
	private int id;
	private String name;
	private VolumeResponse[] volume_credits;
	
	public PersonResponse() {
		super();
		created_characters = new CharacterResponse[]{};
		volume_credits = new VolumeResponse[]{};
	}

	public PersonResponse(String aliases, String api_detail_url, String country, CharacterResponse[] created_characters, 
			String deck, String description, int id, String name, VolumeResponse[] volume_credits) {
		super();
		this.aliases = aliases;
		this.api_detail_url = api_detail_url;
		this.country = country;
		this.created_characters = created_characters;
		this.deck = deck;
		this.description = description;
		this.id = id;
		this.name = name;
		this.volume_credits = volume_credits;
	}

	public String getAliases() {
		return aliases;
	}

	public void setAliases(String aliases) {
		this.aliases = aliases;
	}

	public String getApi_detail_url() {
		return api_detail_url;
	}

	public void setApi_detail_url(String api_detail_url) {
		this.api_detail_url = api_detail_url;
	}
	
	

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public CharacterResponse[] getCreated_characters() {
		return created_characters;
	}

	public void setCreated_characters(CharacterResponse[] created_characters) {
		this.created_characters = created_characters;
	}

	public String getDeck() {
		return deck;
	}

	public void setDeck(String deck) {
		this.deck = deck;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	public VolumeResponse[] getVolume_credits() {
		return volume_credits;
	}

	public void setVolume_credits(VolumeResponse[] volume_credits) {
		this.volume_credits = volume_credits;
	}

	public String toString() {
		JsonObject jo = new JsonObject();
		jo.addProperty("aliases", aliases);
		jo.addProperty("api_detail_url", api_detail_url);
		jo.addProperty("country", country);
		JsonArray ja = new JsonArray();
		if (created_characters != null)
			for (int i=0; i<created_characters.length; i++) {
				ja.add(created_characters[i].toString());
			}
		jo.add("created_characters", ja);
		jo.addProperty("deck", deck);
		jo.addProperty("description", description);
		jo.addProperty("id", id);
		jo.addProperty("name", name);
		ja = new JsonArray();
		if (volume_credits != null)
			for (int i=0; i<volume_credits.length; i++) {
				ja.add(volume_credits[i].toString());
			}
		jo.add("volume_credits", ja);
		return new GsonBuilder().serializeNulls().create().toJson(jo);
	}
	
	public boolean equals(Object o) {
		return this.id == ((PersonResponse)o).getId();
	}

	public Person getPerson() {
		Person p = new Person();
		p.setAliases(aliases);
		p.setApi_detail_url(api_detail_url);
		p.setCountry(country);
		Character[] characters = null;
		if (created_characters != null) {
			characters = new Character[created_characters.length];
			for (int i=0; i<created_characters.length; i++) {
				characters[i] = created_characters[i].getCharacter();
			}
		}
		p.setCreated_characters(characters);
		p.setDeck(deck);
		p.setDescription(description);
		p.setId(id);
		p.setName(name);
		Volume[] volumes = null;
		if (volume_credits != null) {
			volumes = new Volume[volume_credits.length];
			for (int i=0; i<volume_credits.length; i++) {
				volumes[i] = volume_credits[i].getVolume();
			}
		}
		p.setVolume_credits(volumes);
		return p;
	}
}
