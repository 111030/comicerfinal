package com.finki.filip.comicerfinal.asyncs;

import android.os.AsyncTask;
import android.widget.TextView;

import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.gson.comicvine.Issue;
import com.finki.filip.comicerfinal.singletons.MongoSingleton;

/**
 * Created by Filip on 19.1.2017.
 */

public class IssueRatingsPopulator extends AsyncTask<Issue, Void, Integer> {

    private TextView ratingsCount;

    public IssueRatingsPopulator(TextView ratingsCount) {
        this.ratingsCount = ratingsCount;
    }

    @Override
    protected Integer doInBackground(Issue... issues) {
        return MongoDBHelper.getRatingsCountForIssue(issues[0], MongoSingleton.getInstance().getDatabase());
    }

    @Override
    protected void onPostExecute(Integer integer) {
        ratingsCount.setText("(" + String.valueOf(integer) + ")");
    }
}
