package com.finki.filip.comicerfinal.gson.comicvine;

import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class ResponseMultiple<T extends Resource> extends Response {
	
	private ArrayList<T> results;
	
	public ResponseMultiple()
	{
		super();
		results = new ArrayList<T>();
	}
	
	public ResponseMultiple(int status_code, String error, long number_of_total_results, int number_of_page_results,
            int limit, int offset, ArrayList<T> results)
	{
		//super(status_code, error, number_of_total_results, number_of_page_results, limit, offset);
		this.results = results;
	}

	public ArrayList<T> getResults() {
		return results;
	}

	public void setResults(ArrayList<T> results) {
		this.results = results;
	}
	
	public String toString()
	{
		JsonObject jo = new JsonObject();
		JsonArray ja = new JsonArray();
		for (int i=0; i<results.size(); i++) 
		{
			ja.add(results.get(i).toString());
		}
		jo.add("results", ja);
		return super.toString() + new Gson().toJson(jo);
		
	}

}
