package com.finki.filip.comicerfinal.asyncs;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;

import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.gson.other.User;

/**
 * Created by Filip on 23.1.2017.
 */

public class GetCurrentUser extends AsyncTask<Void, Void, User> {

    private Context context;

    public GetCurrentUser(Context context) {
        this.context = context;
    }

    @Override
    protected User doInBackground(Void... voids) {
        return MongoDBHelper.getSignedIn(((WifiManager)context.getSystemService(
                Context.WIFI_SERVICE)).getConnectionInfo().getMacAddress());
    }
}
