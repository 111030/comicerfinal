package com.finki.filip.comicerfinal.asyncs;

import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.finki.filip.comicerfinal.R;
import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.gson.comicvine.Issue;

import java.util.List;

/**
 * Created by Filip on 06.2.2017.
 */

public class GetTopIssuesForVolume extends AsyncTask<Integer, Void, List<Issue>> {

    private Context context;
    private LayoutInflater layoutInflater;
    private LinearLayout issuesll;

    public GetTopIssuesForVolume(Context context, LinearLayout issuesll) {
        this.context = context;
        this.issuesll = issuesll;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    protected List<Issue> doInBackground(Integer... integers) {
        return MongoDBHelper.getTopVolumeIssues(integers[0]);
    }

    @Override
    protected void onPostExecute(List<Issue> issues) {
        if (issues != null) {
                for (int i=0; i<issues.size(); i++) {
                    if (issues.get(i).getImage() == null || issues.get(i).getImage().getThumb_url() == null)
                        continue;
                    View issue = layoutInflater.inflate(R.layout.holder_horizontal, null);
                    ImageView imgHolder = (ImageView)issue.findViewById(R.id.horizontal_holder_image);
                    TextView textHolder = (TextView)issue.findViewById(R.id.horizontal_holder_text);
                    Glide.with(context).load(issues.get(i).getImage().getThumb_url())
                            .into(imgHolder);
                    textHolder.setText(issues.get(i).getName());
                    issuesll.addView(issue);
                }
        }
    }
}
