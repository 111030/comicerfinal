package com.finki.filip.comicerfinal.asyncs;

import android.os.AsyncTask;
import android.widget.TextView;

import com.finki.filip.comicerfinal.factories.MongoDBHelper;
import com.finki.filip.comicerfinal.gson.comicvine.Issue;

/**
 * Created by Filip on 29.1.2017.
 */

public class GetRatingsCount extends AsyncTask<Issue, Void, Integer> {

    private TextView ratingsCount;

    public GetRatingsCount(TextView ratingsCount) {
        this.ratingsCount = ratingsCount;
    }


    @Override
    protected Integer doInBackground(Issue... issues) {
        return MongoDBHelper.getRatingsCountForIssue(issues[0]);
    }

    @Override
    protected void onPostExecute(Integer integer) {
        if (integer > -1) {
            ratingsCount.setText("("+integer+")");
        }
    }
}
