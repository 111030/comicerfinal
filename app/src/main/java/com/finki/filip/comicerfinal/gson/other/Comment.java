package com.finki.filip.comicerfinal.gson.other;

import com.finki.filip.comicerfinal.gson.comicvine.Resource;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Filip on 29.1.2017.
 */

public class Comment extends Resource implements Serializable {

    private Object _id;
    private User user;
    private String message;
    private List<User> likes;
    private String dateSent;

    public Comment() {
        likes = new ArrayList<>();
    }

    public Comment(Object _id, User user, String message, List<User> likes, String dateSent) {
        this._id = _id;
        this.user = user;
        this.message = message;
        this.likes = likes;
        this.dateSent = dateSent;
    }

    public Object get_id() {
        return _id;
    }

    public void set_id(Object _id) {
        this._id = _id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<User> getLikes() {
        return likes;
    }

    public void setLikes(List<User> likes) {
        this.likes = likes;
    }

    public String getDateSent() {
        return dateSent;
    }

    public void setDateSent(String dateSent) {
        this.dateSent = dateSent;
    }

    public String toString() {
        Gson gson = new GsonBuilder().serializeNulls().create();
        JsonObject jo = new JsonObject();
        jo.addProperty("_id", _id != null ? gson.toJson(_id) : null);
        jo.addProperty("user", user != null ? gson.toJson(user, User.class) : null);
        jo.addProperty("message", message);
        JsonArray ja = new JsonArray();
        if (likes != null) {
            for (int i=0; i<likes.size(); i++) {
                ja.add(gson.toJson(likes.get(i), User.class));
            }
        }
        jo.add("likes", ja);
        jo.addProperty("dateSent", dateSent);
        return gson.toJson(jo);
    }
}
