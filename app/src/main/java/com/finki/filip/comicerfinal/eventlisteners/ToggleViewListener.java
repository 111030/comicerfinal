package com.finki.filip.comicerfinal.eventlisteners;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

/**
 * Created by Filip on 24.12.2016.
 */

public class ToggleViewListener implements TextWatcher {

    private View toBeToggled;

    public ToggleViewListener(View toBeToggled) {
        this.toBeToggled = toBeToggled;
    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (charSequence == null || charSequence.length() == 0)
            toBeToggled.setVisibility(View.VISIBLE);
        else
            toBeToggled.setVisibility(View.INVISIBLE);
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }
}
